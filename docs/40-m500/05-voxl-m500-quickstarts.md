---
layout: default
title: M500 Quickstart
parent: M500
nav_order: 5
has_children: false
permalink: /voxl-m500-quickstart/
youtubeId: gTuSaHLCz8w
youtubeId1: EstL6hBuxE4
youtubeId2: V5kxXvu1pLg
youtubeId3: A3ixWg2EOrw
youtubeId4: eYr8nyFid0E
youtubeId5: wrqZFTiHyTU
---

## M500 Quickstart
How to get started with M500
<br>
{% include youtubePlayer.html id=page.youtubeId %}


## Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation) 
- LiPo 4S battery with XT60 connector
  - Buy [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 

- Host computer with:
  - QGroundControl 3.5.6+
  - Wi-Fi
  - a terminal program

### What's in the Box

Included in the box are:

- VOXL m500 with Spektrum Reciever (note: the system supports FrSky receivers like X8R, see [below](#rc-radio-setup))
- 10" props
- USB-to-JST Cable (not used in this guide)
- Spare landing gear mounting screws

![m500-out-of-box](/images/userguides/m500/m500-out-of-box.jpg)


---
# M500 Demos

## M500 LTE Control Demo
How to control M500 over LTE
<br>
{% include youtubePlayer.html id=page.youtubeId3 %}

---
## M500 GPS to Vision Transition for GPS-denied Navigation
How to transition to vision-based, GPS-denied Navigation
{% include youtubePlayer.html id=page.youtubeId1 %}

---
## M500 Hover While Being Pushed Around with no GPS
How to hover in place in a GPS-denied environment using only Visual Inertial Odometry.
{% include youtubePlayer.html id=page.youtubeId2 %}

---
## M500 Camera Gimbal Demo
Demo of M500's optional Camera Gimbal
{% include youtubePlayer.html id=page.youtubeId4 %}

---
## M500 MAVROS Demo

{% include youtubePlayer.html id=page.youtubeId5 %}
