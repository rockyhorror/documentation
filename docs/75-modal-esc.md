---
layout: default
title: Modal ESC
nav_order: 75
has_children: true
permalink: /modal-escs/
---

# ModalAI Electronic Speed Controller (ESC)
{: .no_toc }

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/13/escs)

ModalAI's Electronic Speed Controllers (ESCs) are high-performing, closed-loop speed controllers that use a digital interface (UART, i2c in future).

## Brief Overview
Brushless Electronic Speed Controllers (**ESCs**) are devices that consist of hardware
and software for controlling three-phase brushless DC (**BLDC**) motors. ESCs communicate
with the flight controller, which instructs the ESCs how fast the motor should spin.

ModalAI's ESCs implement the following advanced features:
* Full integration with ModalAI VOXL and Flight Core PCBs
* Bi-directional UART communication with checksum; status and fault monitoring
* Real-time status and health reporting at high update rate (100Hz+ each)
* Closed-loop RPM control for best flight performance
* LED control from flight controller via UART

## Feature Comparison between ModalAI ESC V1 and V2

<table>
<tr>
<td><center><b>ModalAI 4-in-1 V1 ESC</b></center></td>
<td><center><b>ModalAI 4-in-1 V2 RevA ESC</b></center></td>
</tr>
<tr>
  <td><center><img src="/images/datasheet/escs/m0004/m0004-bottom-zoom.jpg" width="300"></center></td>
  <td><center><img src="/images/datasheet/escs/m0027/m0027a-top.jpg" width="300"></center></td>
</tr>
</table>

| Feature	| ModalAI 4-in-1 V1 ESC | ModalAI 4-in-1 V2 RevA ESC |
|--- |:---:|:---:|
| Input Voltage	| 6.5V-12.6V (2-3S Lipo) | 5.5V-16.8V (2-4S Lipo) |
| Aux Power Output | 5.15V 6A | 2x 4.5V (adjustable) 600mA |
| Max Continuous Current Per Motor	| 5A (thermally limited) | 10A+ (thermally limited) |
| MCU |	Atmega328P |	STM32F051K86 |
| Mosfet Driver |	custom (BJT-based) |	MP6530 |
| Mosfets	| AO4616 (N + P) | AON7528 (N) |
| Total Current Sensing | 1mOhm + LTC2946 | N/A |
| Individual Current Sensing | N/A | 4x 2mOhm + INA186 |
| Reverse Polarity Protection | Yes |	No |
| ESD signal protection	| No |Yes  |
| Temperature Sensing	| No | Yes |
| On-board Status LEDs | Yes | Yes |
| External LEDs| RGB via BJT | Neopixel LEDs |
| Secure Bootloader | Yes (AES128) | Yes (AES256) |
| PWM Switching Frequency |	20Khz (fixed)	| 48, 24 Khz |
| Maximum RPM (6 pole pairs) | 17K | 50K+ |
| PWM control input	| No | Yes |
| Active Freewheeling |	No | Yes |
| Tone Generation | Yes | Yes |
| Closed-loop RPM Control |	Yes |	Yes (10Khz) |
| Number of UART ports | 1 (250 Kbit) | 2 (2Mbit+) |		
| Qualcomm ESC protocol support |	Yes |	Yes |
| Weight without wires (g) | 13.7 | 9.5 |
| Board Dimensions (mm) | 58.0 x 40.0	| 40.5 x 40.5 |
| Mounting Hole Size, Pattern (mm)|3.15, 35.0 x 46.5 | 3.05, 31.0 x 33.0 |
