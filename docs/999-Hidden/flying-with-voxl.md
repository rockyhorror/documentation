---
layout: default
title: Flying with VOXL
nav_exclude: true
search_exclude: true
has_children: false
permalink: /flying-with-voxl/
---

# Flying with VOXL
{: .no_toc }

This page is deprecated, please look to [VOXL Vision PX4](/voxl-vision-px4/) for guides on how to fly your voxl.

---