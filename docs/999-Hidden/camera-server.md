---
layout: default
title: Old Camera Server
nav_exclude: true
search_exclude: true
permalink: /camera-server/
---

# Old Camera Server
{: .no_toc }

This page is deprecated, please refer to [voxl-camera-server](/voxl-camera-server/) for camera server documentation.