---
layout: default
title: Qualcomm Flight RB5 Datasheet
parent: Qualcomm Flight RB5
nav_order: 10
has_children: true
permalink: /Qualcomm-Flight-RB5-datasheet/
---

# Qualcomm Flight RB5 Reference Drone Datasheet
[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---
![RB5](/images/quickstart/RB5/rb5frontpage.png)

## High-Level Specs

| Specification | [Qualcomm Flight RB5 Reference Drone](https://www.modalai.com/collections/development-drones/products/voxl-m500?variant=31790290599987) |
| --- | --- |
| Take-off Weight | 1235g |
| --- | --- |
| Size | 480mm |
| --- | --- |
| Flight Time | > 20 minutes |
| --- | --- |
| Payload Capacity | 1kg |
| --- | --- |
| Motors | Holybro 2216-880kv Motors |
| --- | --- |
| Propellers | 10 inch |
| --- | --- |
| Frame | Holybro S500 V2 Frame Kit |
| --- | --- |
| ESCs | ModalAI VOXL 4in1 ESC V2 |
| --- | --- |
| CPU | Qualcomm QRB5165 |
| --- | --- |
| Sensors | 2 TDK CH-201 ultrasonic rangefinder sensors <br> TDK ICM-42688-P IMU <br> TDK ICP-10100 Barometer <br> GPS/Magnetometer (Holybro) | 
| --- | --- |
| Image Sensors | Tracking Sensor - Global Shutter VGA <br> High-resolution 4k30 <br> Front and Rear Stereo - Global Shutter VGA <br> 6x 4-lane MIPI-CSI2 (Up to 7 cameras) |
| --- | --- |
| Connections | USB 3.1 Hub <br> USB-C OTG |
| --- | --- |
| Additional I/O | 2 GPIO <br> I2C, 4 SPI |
| --- | --- |
| Communications Options | 5G Add-On (with [Quectel RM502Q-AE](https://www.quectel.com/product/5g-rm502q-ae/)) <br> WiFi  <br> [4G LTE](https://modalai.com/lte-addon) |
| --- | --- |
| Remote Control | Spektrum DSMX Receiver |
| --- | --- |
| Power Module | ModalAI Power Module v3 - 5V/6A |
| --- | --- |
| Battery | 4S with XT60 Connector (6S with expanded battery standoffs) |
| --- | --- |
| Expansions | ModalAI Legacy B2B connector for backwards compatibility with 4G add-on, Microhard add-on, USB debug board. <br> High speed B2B connector for PCIe2, I2C, SPI, UART, GPIO <br> TDK Chirp Sensor breakout PCBAs for object avoidance capabilities |
| --- | --- |
| Optional Expansion Add-Ons | 5G modem add-on with USB 3.1 Hub <br> SD Card add-on <br> Gigabit Ethernet add-on with USB 3.1 Hub |
| --- | --- |
| Open Software Packages | Ubuntu 18.04 <br> Linux Kernel 4.19 <br> OpenCV <br> TensorFlow Lite <br> ROS 1 / ROS 2 br> RB5 Flight SDK hosted by ModalAI |
| --- | --- |
| Developer Connectivity |QGroundControl <br> RViz <br> ADB (Android Debug Bridge) |


## System Overview

## High-level Overview

[View in fullsize](/images/datasheet/RB5/RB5-System-Diagram.png){:target="_blank"}
![RB5.png](/images/datasheet/RB5/RB5-System-Diagram.png)

## Low-level Block Diagram

[View in fullsize](/images/datasheet/RB5/rb5-flight-block-diagram.png){:target="_blank"}
![RB5.png](/images/datasheet/RB5/rb5-flight-block-diagram.png)

## Qualcomm Flight RB5 Labeled

[View in fullsize](/images/datasheet/RB5/rb5-labeled-5.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-5.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-6.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-6.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-3.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-3.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-4.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-4.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-2.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-2.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-1.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-1.png)


## PX4 Tuning Parameters

Our engineers have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Motor Configuration

The propellers have arrows on them as to which way they should turn. Motor configuration in the image below.

![voxl-m500-motor-configuration.png](/images/datasheet/m500/voxl-m500-motor-configuration.png)

## Connectivity for Remote Operation

| Connectivity Option | Use Case | Details |
| --- | --- | --- |
| Spektrum R/C | Manual remote control of the vehicle | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/) |
| 5G LTE | Long Range, BVLOS operation | Coming Soon  |

## Sub-component Dimensions

| PCB | H x W x D | Weight | Drawings |
| --- | --- | --- | --- | 
| QC Flight RB5 Mainboard (M0052) | 50 x 84 x 10.9mm | 24.8g | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0052-mechanical-drawing%20v4.pdf) |
| ModalAI 5G Modem Carrier Board (M0067) | 67.5 x 36 x 10mm | 12.8g | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0067-mechanical-drawing%20v1.pdf) |
| Quectel RM502Q-AE | 52× 30 × 2.3mm | 8.7g | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Quectel_RM500Q-AERM502Q-AE_Hardware_Design_V1.0.pdf) |
| Thundercomm RB5 SOM | 45 x 56 x 9mm | 17.7g | [Mechanical Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/Thundercomm-SOM-5165-drawing%20v2.pdf) |
