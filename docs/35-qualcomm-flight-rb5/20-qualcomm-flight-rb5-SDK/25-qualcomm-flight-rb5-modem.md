---
layout: default
title: RB5 Modem
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 25
has_children: false
permalink: /Qualcomm-Flight-RB5-modem/
---

# Qualcomm Flight RB5 Modem
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-modem

`rb5-modem` is a tool for enabling cellular connection on RB5 Flight via Quectel or Telit 5G modem. 

For more info on connecting to a ground station over the 5G network, checkout the following page:

- [5G Modem User Guide](https://docs.modalai.com/5G-Modem-user-guide/)

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).


### How to Use

#### Configure to Run on Bootup

The `rb5-modem` package installs with a configuration script.  You can run the following after you adb onto the target.

- adb onto target

```
adb shell
sh-4.4# 
```

```
rb5-modem-configure
```

You will be prompted to select your modem hardware and APN (Access Point Name) for your chosen SIM card. 

This will enable the rb5-modem service in the background and it will be started at boot.

The configuration file located at `/etc/modalai/rb5-modem.conf` can be modified directly to change settings as well.

- Enable `rb5-modem` to connect to network on bootup and start 

```
sh-4.4# rb5-modem-configure factory_enable
```

- Disable `rb5-modem` from connecting to network on bootup and kill network by running

```
sh-4.4# rb5-modem-configure factory_disable
```

#### View Service Status

```
sh-4.4# systemctl status rb5-modem

● rb5-modem.service - "rb5-modem"
   Loaded: loaded (/usr/bin/rb5-modem; indirect; vendor preset: enabled)
   Active: active (running) since Mon 2021-07-26 16:28:00 UTC; 15s ago

Jul 26 16:28:00 qrb5165-rb5 systemd[1]: Started "rb5-modem".
Jul 26 16:28:00 qrb5165-rb5 rb5-streamer[9522]: Initializing network connection...

```

### Connecting to QGroundControl

Checkout this page for info on [Connecting to QGC over 5G](https://docs.modalai.com/Qualcomm-Flight-RB5-user-guide-connect-gcs/#connecting-to-qgc-over-5g) using a VPN
