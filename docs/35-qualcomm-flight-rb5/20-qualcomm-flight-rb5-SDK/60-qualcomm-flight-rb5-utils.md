---
layout: default
title: RB5 Utils
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 60
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-utils/
---

# Qualcomm Flight RB5 SDK Utils
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-utils

The `rb5-utils` package brings a variety of miscellaneous utilities that help with setting up or inspect your rb5 environment

### Utilities

| Name  | Summary |
| --- | --- |
| rb5-bind   | Set Sektrum receiver into bind mode for radio binding | 
| rb5-flight-versions   | Prints out versions of different packages on the rb5 |
| rb5-net-check   | Check network interface |

### rb5-bind
The rb5-bind tool is used to bind a Spektrum receiver to a radio. It sets the receiver into bind mode allow you to bind to the radio

#### Architecture
The following is the architecture depicting how rb5-bind works

![rb5-bind architecture](/images/userguides/rb5-sdk/rb5-bind-architecture.png)

rb5-bind works by pulsating a GPIO pin based on the binding protocol selected. It begins by enabling the power register via GPIO pin 146. Then it pulsates GPIO pin 46 based on the selected protocol. 

#### How to use

In order to use rb5-bind, begin by running rb5-bind and showing the help menu
```bash
rb5-bind -h
```

The following menu should appear
```
Tool used for binding Spektrum receiver. In order to use,
select ONLY 1 bind type
-d, --debug         enable debug messages
-h, --help          print this help messsage
-t, --DSM2_22       3 pulses (DSM2 1024/22ms)
-f, --DSM2_11       5 pulses (DSM2 2048/11ms)
-s, --DSMX_22       7 pulses (DSMX 22ms)
-n, --DSMX_11       9 pulses (DSMX 11ms)
```

This menu will show you the different bind options available. You should select the protocol based on your radio.

In order to run rb5-bind select **ONLY ONE** binding mode. In this example we'll select the DSMX protocol with 22ms latency:
```bash
rb5-bind --DSMX_22
```

After a couple seconds your receiver should have an orange LED that will start flashing, if this is the case, then your receiver is successfully in binding mode. You can now use your radio to bind to it.

### rb5-flight-versions
The `rb5-flight-versions` tool is used to check version of packages on your rb5

#### How to Use
In order to use just run `rb5-flight-versions` on your rb5:
```bash
rb5-flight-versions
```

This will show the following output:
```
SDK version:
rb5-flight-sdk: 1.0.5

PX4 versions:
modalai-slpi: 1.0-4
px4-rb5-fligh: 1.2-2
px4-support: 1.0-4
libfc-sensor: 1.0-2

System Image:
1.0.3-M0052-9.1-perf

Serial Number:
684089971
```

This will show you the versions of the SDK, PX4, and system image installed on your rb5. It will also print out your rb5's serial number.

### rb5-net-check
This tool is used for checking a network interface

#### How to Use
In order to use, run `rb5-net-check` and specify a network interface like eth0 for ethernet or wlan0 for wifi:
```bash
rb5-net-check wlan0
```

an IP should print as the output:
```
192.168.1.127
```

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).
