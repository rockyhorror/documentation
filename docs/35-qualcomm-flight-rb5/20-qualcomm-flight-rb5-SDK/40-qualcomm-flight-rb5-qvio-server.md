---
layout: default
title: RB5 QVIO Server
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 40
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-qvio-server/
---

# Qualcomm Flight RB5 SDK QVIO Server
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-qvio-server

The `rb5-qvio-server` allows you to publish VIO data.

### QVIO Information

Coming soon

### Architecture

Coming soon

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).


### How to Use

#### Configure to Run on Bootup

Coming soon


#### View Service Status

Coming soon.