---
layout: default
title: RB5 PX4
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 10
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-px4/
---

# Qualcomm Flight RB5 PX4
{: .no_toc }


## RB5 PX4

On the Qualcomm Flight RB5, PX4 runs on a DSP.  It's deployed in the Qualcomm Flight RB5 SDK.  More information coming soon.