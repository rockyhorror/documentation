---
layout: default
title: RB5 SDK Installation
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 5
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-installation/
---

# Qualcomm Flight RB5 SDK
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## What is the Qualcomm Flight RB5 SDK?

The SDK provides the following:

- ModalAI packages to support computer vision, flight control and communications
- Configuration tools to setup the `Qualcomm Flight RB5`, used by us internally
- Supporting tools
- Upgrade support

## Do I need to Install?

If you have a `Qualcomm Flight RB5`, it comes pre-installed so there's nothing to do.

Occasionally we will ship updates, and you can follow the update procedure below.

## Where To Download

Coming soon to [developer.modalai.com](developer.modalai.com)

## How to Install

Assume the download name was `rb5-flight-sdk-0.0.0.tar.gz`, then:

On host computer, unzip and push:

```
tar -xzf rb5-flight-sdk-0.0.0.tar.gz
adb push rb5-flight-sdk-0.0.0 /home/
```

Then, on target, install:

```bash
adb shell
cd /home/rb5-flight-sdk-0.0.0
./install-sdk.sh
```

Now, to enable the default configuration for the vehicle:

  - **This is the Only Supported Option Currently**

```bash
./configure-sdk.sh factory_enable
```

## How to Upgrade

This process is the same as above, but instead of running `install-sdk.sh`, run this final command instead:

```bash
./upgrade-sdk.sh
```

A reboot will be required after running this command.

Here's is an example of upgrading from an SDK v0.0.2 test build to an SDK v0.0.4 test build for reference.  In this example, `rb5-modem` was updated.

After downloading an update from link above, begin by unzipping:

```
cd ~/Downloads
❯ tar -xzvf rb5-flight-sdk-0.0.4.tar.gz
x rb5-flight-sdk-0.0.4/
x rb5-flight-sdk-0.0.4/install-sdk.sh
x rb5-flight-sdk-0.0.4/packages/
x rb5-flight-sdk-0.0.4/update-sdk.sh
x rb5-flight-sdk-0.0.4/configure-sdk.sh
x rb5-flight-sdk-0.0.4/test.sh
x rb5-flight-sdk-0.0.4/packages/rb5-voa-server-0.0.1.deb
x rb5-flight-sdk-0.0.4/packages/rb5-modem-0.0.4.deb
x rb5-flight-sdk-0.0.4/packages/rb5-chirp-server-0.0.1.deb
x rb5-flight-sdk-0.0.4/packages/rb5-mv-0.0.1.deb
x rb5-flight-sdk-0.0.4/packages/rb5-qvio-server-0.0.1.deb
x rb5-flight-sdk-0.0.4/packages/rb5-streamer-0.0.1.deb
x rb5-flight-sdk-0.0.4/packages/rb5-camera-server-0.0.2.deb

❯ adb push rb5-flight-sdk-0.0.4 /home/
rb5-flight-sdk-0.0.4/: 11 files pushed, 0 skipped. 7.9 MB/s (17421912 bytes in 2.097s)

❯ adb shell
sh-4.4# cd /home/

sh-4.4# ls
rb5-flight-sdk-0.0.2  rb5-flight-sdk-0.0.4

sh-4.4# cd rb5-flight-sdk-0.0.4/
sh-4.4# ./update-sdk.sh

[INFO] Upgrading
dpkg-scanpackages: info: Wrote 7 entries to output Packages file.
Get:1 file:/home/rb5-flight-sdk-0.0.4 packages/ InRelease
Ign:1 file:/home/rb5-flight-sdk-0.0.4 packages/ InRelease
Get:2 file:/home/rb5-flight-sdk-0.0.4 packages/ Release
Ign:2 file:/home/rb5-flight-sdk-0.0.4 packages/ Release
Get:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Ign:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Ign:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Ign:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:3 file:/home/rb5-flight-sdk-0.0.4 packages/ Packages [1112 B]
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Get:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Ign:4 file:/home/rb5-flight-sdk-0.0.4 packages/ Translation-en
Reading package lists... Done
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-mv is already the newest version (0.0.1).
Calculating upgrade... Done
The following packages will be upgraded:
  rb5-modem
1 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Need to get 0 B/3932 B of archives.
After this operation, 0 B of additional disk space will be used.
Get:1 file:/home/rb5-flight-sdk-0.0.4 packages/ rb5-modem 0.0.4 [3932 B]
debconf: delaying package configuration, since apt-utils is not installed
(Reading database ... 78196 files and directories currently installed.)
Preparing to unpack .../packages/rb5-modem-0.0.4.deb ...
Unpacking rb5-modem (0.0.4) over (0.0.3) ...
Setting up rb5-modem (0.0.4) ...

Done installing rb5-modem
To configure and enable this as a service on boot
run: rb5-modem-configure

Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-camera-server is already the newest version (0.0.2).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-voa-server is already the newest version (0.0.1).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-qvio-server is already the newest version (0.0.1).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-chirp-server is already the newest version (0.0.1).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-modem is already the newest version (0.0.4).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Reading package lists... Done
Building dependency tree
Reading state information... Done
rb5-streamer is already the newest version (0.0.1).
Calculating upgrade... Done
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.

```

Upgrading disables the services, so re-enable using the following:

```bash
./configure-sdk.sh factory_enable
```

## Release Notes

Coming soon.
