---
layout: default
title: RB5 Streamer
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 50
has_children: false
permalink: /Qualcomm-Flight-RB5-streamer/
---

# Qualcomm Flight RB5 Streamer
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-streamer

The `rb5-streamer` enables RTSP video streaming from the Hi-res camera to a ground station.

### Camera Information

| ID  | Summary             | Usage          | Sensor         | Pipeline |
| --- | ---                 | ---            | ---            | --- |
| 3   | Front Hi-res        | FPV (front)    | IMX214         | 4208x3120 (4k), Bayer RGGB 10bit, 30 FPS |


### Architecture

In summary, this is what's happening:

```bash
Image Sensor 
  --> Kernel 
    --> CameraX/GST
      --> rb5-streamer  
        --> RTSP stream sending frames
           --> Video stream viewer e.g. (VLC)
```

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).


### How to Use

#### Configure to Run on Bootup

The `rb5-streamer` package installs with a configuration script.  You can run the following after you adb onto the target.

- adb onto target

```
adb shell
sh-4.4# 
```

- Enable the Hi-res camera to start streaming on bootup and start streaming by running

```
sh-4.4# rb5-streamer-configure factory_enable
```

- Disable the Hi-res camera from streaming on bootup and stop streaming by running

```
sh-4.4# rb5-streamer-configure factory_disable
```

#### View Service Status

```
sh-4.4# systemctl status rb5-streamer

● rb5-streamer.service - "rb5-streamer"
   Loaded: loaded (/usr/bin/rb5-streamer; indirect; vendor preset: enabled)
   Active: active (running) since Mon 2021-07-26 16:28:00 UTC; 15s ago

Jul 26 16:28:00 qrb5165-rb5 systemd[1]: Started "rb5-streamer".
Jul 26 16:28:00 qrb5165-rb5 rb5-streamer[9522]: gbm_create_device(156)
Jul 26 16:28:00 qrb5165-rb5 rb5-streamer[9522]: stream ready at rtsp://127.0.0.1:8554/test
```

#### View output stream

The output video stream can be viewed using a number of clients e.g. (VLC, QGroundControl, FFPlay).

Simply point your client to the following url: `rtsp://HOST_IP:8554/camera3`

where `HOST_IP` is the IP address of your `Qualcomm Flight RB5`.
