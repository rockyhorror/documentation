---
layout: default
title: RB5 VOA Server
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 45
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-voa-server/
---

# Qualcomm Flight RB5 SDK VOA Server
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-voa-server

The `rb5-voa-server` allows you to publish VOA data.

### VOA Information

Coming soon

### Architecture

Coming soon

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).

#### Stereo Camera Calibration

Needed

### How to Use

#### Configure to Run on Bootup

Coming soon


#### View Service Status

Coming soon.
