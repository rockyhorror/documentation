---
layout: default
title: RB5 Camera Server
parent: Qualcomm Flight RB5 SDK
grand_parent: Qualcomm Flight RB5
nav_order: 30
has_children: false
permalink: /Qualcomm-Flight-RB5-sdk-camera-server/
---

# Qualcomm Flight RB5 SDK Camera Server
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## rb5-camera-server

The `rb5-camera-server` allows you to publish data from any of the 6 cameras on the platform.

### Camera Information

| ID  | Summary             | Usage          | Sensor         | Pipeline |
| --- | ---                 | ---            | ---            | --- |
| 0   | Front Stereo, Left  | VOA (front)    | ov7251         | 640x480, Bayer B&W 10bit, 30 FPS, hw synced with 1 |
| 1   | Front Stereo, Right | VOA (front)    | ov7251, master | 640x480, Bayer B&W 10bit, 30 FPS, hw synced with 0 |
| 2   | Front Tracking      | VIO (front)    | ov7251         | 640x480, Bayer B&W 10bit, 30 FPS |
| 3   | Front Hi-res        | FPV (front)    | IMX214         | 4208x3120 (4k), Bayer RGGB 10bit, 30 FPS |
| 4   | Rear Stereo, Left   | VOA (rear)     | ov7251         | 640x480, Bayer B&W 10bit, 30 FPS, hw synced with 5 |
| 5   | Rear Stereo, Right  | VOA (rear)     | ov7251, master | 640x480, Bayer B&W 10bit, 30 FPS, hw synced with 4 |

### Architecture

In summary, this is what's happening:

```bash
Image Sensor 
  --> Kernel 
    --> CameraX/GST
      --> rb5-camera-server  
        --> a named pipe getting frames
           --> code reading from named pipe doing stuff
```

### How to Build and Install

If you have a `Qualcomm Flight RB5`, it comes pre-loaded with the software and there's no need to re-install unless you want to.  If you want to see the source, build and tweak it, the repo is [COMING-SOON](https://docs.modalai.com/Qualcomm-Flight-RB5-sdk/).


### How to Use

#### Configure to Run on Bootup

Generally, we want to have the computer vision processing start up on bootup.  On the `Qualcomm Flight RB5`, that's the default setup for camera IDs 0, 1, 2, 4 and 5 (the core computer vision sensors)

The `rb5-camera-server` package installs with a configuration script.  You can run the following after you adb onto the target.

- adb onto target

```
adb shell
sh-4.4# 
```

- Enable the default computer vision cameras (0, 1, 2, 4, 5) to start on bootup by selecting `1) yes`

```
sh-4.4# rb5-camera-server-configure 

Would you like to enable the camera service?
This will start all cameras in a background process
Selecting yes will start the service, no will disable the service

1) yes
2) no
#? 1
```

#### View Service Status

We use a 'templated' systemd unit file.... so the services show up a little different:

| ID  | Summary             | Usage          |
| --- | ---                 | ---            |
| 0   | Front Stereo, Left  | `systemctl status rb5-camera-server@0` |
| 1   | Front Stereo, Right | `systemctl status rb5-camera-server@1` |
| 2   | Front Tracking      | `systemctl status rb5-camera-server@2` |
| 3   | Front Hi-res        | not started by default, used upon request for FPV stream |
| 4   | Rear Stereo, Left   | `systemctl status rb5-camera-server@4` |
| 5   | Rear Stereo, Right  | `systemctl status rb5-camera-server@5` |

For example, camera 0 after successfully starting:

```
sh-4.4# systemctl status rb5-camera-server@0

● rb5-camera-server@0.service - "rb5-camera-server-0"
   Loaded: loaded (/usr/bin/rb5-camera-server; indirect; vendor preset: enabled)
   Active: active (running) since Mon 2021-07-26 16:28:00 UTC; 15s ago
 Main PID: 9522 (rb5-camera-serv)
    Tasks: 4 (limit: 4915)
   CGroup: /system.slice/system-rb5\x2dcamera\x2dserver.slice/rb5-camera-server@
           └─9522 /usr/bin/rb5-camera-server -c0

Jul 26 16:28:00 qrb5165-rb5 systemd[1]: Started "rb5-camera-server-0".
Jul 26 16:28:00 qrb5165-rb5 rb5-camera-server[9522]: gbm_create_device(156): Inf
```

#### Use a Provided Test Client

If you want to test a camera, for example camera ID 5, run the following:


```
sh-4.4# rb5-camera-client -c 0

camera ID is set to 0
Waiting for server to start: /dev/rb5-camera-pipe-0

```

#### View Named Pipes in File System

The interface the `rb5-camera-server` provides are named pipes where you can read camera frames and meta data from.

```
sh-4.4# ls -la /dev/rb5-camera*

prw-r--r-- 1 root root 0 Jul 26 15:45 /dev/rb5-camera-pipe-0
prw-r--r-- 1 root root 0 Jul 26 15:45 /dev/rb5-camera-pipe-1
prw-r--r-- 1 root root 0 Jul 26 15:45 /dev/rb5-camera-pipe-2
prw-r--r-- 1 root root 0 Jul 26 15:45 /dev/rb5-camera-pipe-4
prw-r--r-- 1 root root 0 Jul 26 15:45 /dev/rb5-camera-pipe-5
```

#### How to Change What Cameras Are Started on Bootup

Currently, the camera's initialization setup is handled here:

```
/usr/bin/rb5-camera-server-configure
```

You'll find usage like below, where you can enable/disable by ID:

```
# enables service which starts all cameras
enable_service () {
	echo "reloading systemd services"
	systemctl daemon-reload

	echo "enabling /starting rb5-camera-server systemd service"

	# front tracking
	systemctl enable rb5-camera-server@2.service
	systemctl restart rb5-camera-server@2.service
```

