---
layout: default
title: RB5 Using VIO
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 50
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-using-vio/
---
# Qualcomm Flight RB5 VIO
{: .no_toc }

## Enabling VIO

VIO on the RB5 is disabled by default. The service file for VIO can be started by configuring the qvio server:
```bash
rb5-qvio-server-configure
```
It will show the following prompt:
```bash
Would you like to enable the qvio service?
This will start qvio as a background process
Selecting yes will start the service, no will disable the service

1) yes
2) no
```

Enter ```1 (yes)``` to enable the service. This will immediately start VIO and enable it on boot.

## Using VIO
In order to check if VIO is running you can check with the following command:
```bash
systemctl status rb5-qvio-server
```

This will show you the status of the rb5-qvio-server. 

If the status displays ```active(runnning)``` then VIO is running and the VIO data is being sent to PX4.

If the status displays ```failed``` then VIO failed to run. Make sure that camera index 2(downward facing tracking camera) and PX4 are running.

To restart VIO, you can simply restart the service file:
```bash
systemctl restart rb5-qvio-server
```

VIO should be restarted and the drone should be idle before any flight where VIO will be used.

## Viewing VIO Data
VIO data can be viewed by manually running the rb5-camera-client command:

```bash
rb5-qvio-client
```

**NOTE** A library path was set for this command, if you're command doesn't execute, inside your adb shell run ```ldconfig```

This will print out position data in the following format:
```
    T_imu_wrt_vio (m)   |Roll Pitch Yaw (deg)| state| error_code
   -0.11   -0.12   -0.06| -14.6  -30.1   -2.6| OKAY | 
```

T_imu_wrt_vio represents your drones XYZ position in meters from your starting position. To validate this data, you can move the drone forwards and observe as the X value increases.

## Next Steps

Now let's using [obstacle avoidance features](/Qualcomm-Flight-RB5-user-guide-using-voa/)
