---
layout: default
title: RB5 First Flight
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 30
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-first-flight/
---

# Qualcomm Flight RB5 First Flight
{: .no_toc }

## Preflight Checks

Disconnect the power supply now and move the Qualcomm Flight RB5 to a safe location where you wish to fly and connect a battery for flight while the Qualcomm Flight RB5 is on the ground.

Wait for the connection with QGroundControl to be established.

### Attitude Check

Lift the vehicle and move it around, verify that the attitude reported in QGroundControl GUI looks and responds correctly. Try not to cover the tracking camera during this process.

![qgc_attitude_gui.png](/images/qgc/qgc_attitude_gui.png)

### Arming Vehicle Without Props

With the propellers **off** arm the vehicle.

- Set killswitch to off ("Aux2 Gov" switch down)
- **Left** stick hold **down** and to the **right**

Validate the motor rotation is as expected and the vehicle responds to throttling up momentarily.

Disarm the vehicle:

- **Left** stick hold **down** and to the **left**

Correct rotation direction of motors:

<img src="/images/qgc/QuadRotorX-small.svg" alt="vehicle-overview-props-144-1024.jpg" width="50%">

### Install Propellers

Install the propellers following this orientation. Each motor shaft has a white or black top to indicate which propeller goes where. Note that the clockwise spinning propellers are reverse threaded to prevent loosening during flight.

Ensure you tighten the propellers down tightly by hand or they may spin off when engaging the kill switch.

![RB5-Prop-Locations](/images/quickstart/RB5/QC-Flight-RB5-Prop-Locations.png)

### First Flight (Manual Mode)

You should be comfortable flying before proceeding!

Arm the vehicle, now safely fly in manual mode! No instructions here, you should know what you are doing if you're flying!

Land and disarm.

## Next Steps

Now let's get started with [using the VIO feature](/Qualcomm-Flight-RB5-user-guide-using-vio/)
