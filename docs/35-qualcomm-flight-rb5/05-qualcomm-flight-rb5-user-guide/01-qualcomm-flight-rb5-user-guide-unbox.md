---
layout: default
title: RB5 Unboxing
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 1
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-unboxing/
---

# Qualcomm Flight RB5 Unboxing
{: .no_toc }

This guide will walk you from taking the Qualcomm Flight RB5 out of the box and up into the air!

For technical details, see the [datasheet](/Qualcomm-Flight-RB5-datasheet/).


## WARNING
{: .no_toc }

*Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

![RB5-safety-card](/images/quickstart/RB5/RB5_Safety_Card-1.png)

---

## Overview

### What's in the Box

Drone Only:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- 10" props
- 5G Modem (Optional)

![RB5-drone-only](/images/quickstart/RB5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K0-FINAL.png)

Basic Dev Kit:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- 10" props
- 5G Modem (Optional)
- Desktop Development Fan
- Power Supply

![RB5-basic-kit](/images/quickstart/RB5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K1-FINAL.png)

Pro Dev Kit:

- Qualcomm Flight RB5 Development Kit with Spektrum Receiver
- (2 Pair) 10" props
- 5G Modem (Optional)
- Desktop Development Fan
- Power Supply
- Spektrum DX6e Transmitter
- Hard Case

![RB5-pro-kit](/images/quickstart/RB5/MRB-D0004-3-V1-F1-B1-C11-M7-T1-G0-K2-FINAL.png)

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- Power Supply or Battery with XT60 connector
  - e.g. Gens Ace 3300mAh, or any 4S battery with XT60 connector
  - Buy Battery [Here](https://www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) 
  - Buy Power Supply [Here](https://www.modalai.com/products/ps-xt60?_pos=1&_sid=f5b241f03&_ss=r)  
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi

## Next Steps

Get an overview of the [vehicle components](/Qualcomm-Flight-RB5-user-guide-components/)
