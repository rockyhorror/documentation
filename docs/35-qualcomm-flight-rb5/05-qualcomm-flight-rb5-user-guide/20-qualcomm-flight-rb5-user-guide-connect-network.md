---
layout: default
title: RB5 Connect Network
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 20
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-connect-network/
---

# Qualcomm Flight RB5 Connect Network
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Connect Using WiFi

### Update wpa_supplicant.conf

First, ADB onto your device if you haven't already done so:

```bash
adb shell
```

In order to connect your Qualcomm Flight RB5 to WiFi, modify the `wpa_supplicant` file using the following:

```
vi /data/misc/wifi/wpa_supplicant.conf
```

Replace the default `SSID` and `psk` with the credentials for your WiFi network

```
# Only WPA-PSK is used. Any valid cipher combination is accepted.
ctrl_interface=/var/run/wpa_supplicant
update_config=1

network={
#Open
#        ssid="example open network"
#        key_mgmt=NONE
#WPA-PSK
	ssid="Monkey1" # <-- Modify SSID here
	#proto=WPA
	key_mgmt=WPA-PSK
	pairwise=TKIP CCMP
	group=TKIP CCMP
	psk="tUgasw4z" # <-- Modify psk here
#WEP
#	ssid="example wep network"
#	key_mgmt=NONE
#	wep_key0="abcde"
#	wep_key1=0102030405
#	wep_tx_keyidx=0
}
wowlan_triggers=magic_pkt
```

### Reboot

To take effect, you need to reboot.

```bash
# exit from adb
exit

# reboot
adb reboot
```

### Check IP Address

Get the RB5 shell over adb again:

```bash
adb shell
```

Run the following to find the IP of the RB5:

```bash
ifconfig wlan0
```

You'll see something like:

```
wlan0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.188  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::7970:262b:29b8:37e9  prefixlen 64  scopeid 0x20<link>
        ether 00:03:7f:12:33:6e  txqueuelen 3000  (Ethernet)
        RX packets 51  bytes 7204 (7.2 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 18  bytes 1889 (1.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

In this case, the Qualcomm Flight RB5's IP address on the network is `192.168.1.188`

### SSH onto Qualcomm Flight RB5

Now, from you host machine, we can SSH on target using the following instead of adb:

```
ssh root@192.168.1.188
```

The default SSH password is `oelinux123`

If the SSH connection is successful, you should see the following:

```
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.19.125-perf aarch64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation
This system has been minimized by removing packages and content that are
not required on a system that users do not log into.

To restore this content, you can run the 'unminimize' command.

The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

root@qrb5165-rb5:~#
```

### Next Steps

There are roughly two paths you could take:

- To check out PX4 and Fly, [go here](/Qualcomm-Flight-RB5-user-guide-px4/)
- To skip to Machine Vision features to mess around at your desktop, [go here](/Qualcomm-Flight-RB5-user-guide-using-vio/)

## Connecting to 5G network

### Install SIM Card

To install a SIM into your RB5, start by disconnecting the battery.

![RB5-SIM-4](/images/userguides/rb5/rb5-sim-install-4.png)

Then, remove the two screws holding in the flight deck lid.
<hr>

![RB5-SIM-2](/images/userguides/rb5/rb5-sim-install-2.png)

Next, remove the 4 screws holding in the chirp sensor. You may need to hold the bolts in place from the inside while unscrewing from the outside.   
<hr>

![RB5-SIM-1](/images/userguides/rb5/rb5-sim-install-1.png)

 The SIM slot is located under the top PCB as shown above. Slide the SIM into the slot until you hear the click.
You  might find this process easier using forceps.
<hr>

![RB5-SIM-3](/images/userguides/rb5/rb5-sim-install-3.png)
Make sure the SIM is fully inserted. If you ever want to swap the SIM, just push on it, and it will pop out.
<hr>

![RB5-SIM-4](/images/userguides/rb5/rb5-sim-install-4.png)
Screw the chirp sensor back in place. Then, screw the flight deck lid back on the same way you took it off.
<hr>

### Configuring Service

In order to establish a connection to the 5G network, `rb5-flight-modem` is required.

The `rb5-flight-modem` service can be configured by running the following:

```
rb5-flight-configure-modem
```

The user will be prompted to select their modem hardware and APN (Access Point Name) for their chosen SIM card.

This will enable the `rb5-flight-modem` service in the background and it will be started at boot.


### Disabling / enabling service

The `rb5-flight-modem` service can be disabled by running the following:

```
systemctl disable rb5-flight-modem
```

The service can be re-enabled by running:

```
systemctl enable rb5-flight-modem
```

The status of the service can be viewed by running:

```
systemctl status -l rb5-flight-modem
```

### Next Steps

There are roughly two paths you could take:

- To check out PX4 and Fly, [go here](/Qualcomm-Flight-RB5-user-guide-px4/)
- To skip to Machine Vision features to mess around at your desktop, [go here](/Qualcomm-Flight-RB5-user-guide-using-vio/)
