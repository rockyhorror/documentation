---
layout: default
title: RB5 Power Supply and Battery
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 10
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-power/
---

# Qualcomm Flight RB5 Power Supply and Battery
{: .no_toc }

## Safety First!

For setup and configuration of your Qualcomm Flight RB5 you must **Remove the Propellers** for safety.

![vehicle-battery-connected](/images/userguides/rb5/rb5-battery-in-noprop2.png)

## Battery Information

To install a 4S battery, slide into the body as shown. There is a stop at the front of the vehicle that will prevent the battery from sliding too far forward. The battery should be pressed all the way in until it hits the stop for a consistent center of mass. Power on the vehicle by connecting the battery to the XT60 connector on the fan and connect the other end of the connector to the power module shown in yellow below. Always use the fan with when developing on the bench to avoid overheating.

![vehicle-battery-connected](/images/userguides/rb5/rb5-battery-in-noprop3.png)


## Benchtop Power Supply

For desktop use, a 12VDC wall power supply can be used and is [available here](https://www.modalai.com/products/ps-xt60).  this is convenient when doing development.

## Next Steps

Connect over adb following [this guide](/Qualcomm-Flight-RB5-user-guide-connect-adb/)
