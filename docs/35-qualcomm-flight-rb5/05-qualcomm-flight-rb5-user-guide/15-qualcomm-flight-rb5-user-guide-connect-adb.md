---
layout: default
title: RB5 Connect Over ADB
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 15
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-connect-adb/
---

# Qualcomm Flight RB5 Connect Over ADB
{: .no_toc }

## How to Setup ADB

See [here](/setup-adb/)

## How to Connect to RB5 Over ADB

After installing adb, it's as simple as running the following:

```bash
adb shell
```

Now you are running a shell inside the RB5's Ubuntu OS!

## Next Steps

Connect over to a network following [this guide](/Qualcomm-Flight-RB5-user-guide-connect-network/)