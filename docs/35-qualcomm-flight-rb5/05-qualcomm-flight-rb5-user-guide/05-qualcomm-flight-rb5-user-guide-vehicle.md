---
layout: default
title: RB5 Vehicle Components
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 5
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-components/
---

# Qualcomm Flight RB5 Vehicle Components
{: .no_toc }


## Overview

The following images give an overview of the vehicle components.

[View in fullsize](/images/datasheet/RB5/rb5-labeled-5.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-5.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-6.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-6.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-3.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-3.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-4.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-4.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-2.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-2.png)

[View in fullsize](/images/datasheet/RB5/rb5-labeled-1.png){:target="_blank"}
![Qualcomm-Flight-RB5-Labeled](/images/datasheet/RB5/rb5-labeled-1.png)

## Next Steps

Power on the unit using a power supply or battery following [this guide](/Qualcomm-Flight-RB5-user-guide-power/)
