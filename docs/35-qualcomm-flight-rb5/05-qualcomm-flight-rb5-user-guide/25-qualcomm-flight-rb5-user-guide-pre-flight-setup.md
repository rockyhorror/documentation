---
layout: default
title: RB5 Pre-Flight Setup
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 25
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-pre-flight-setup/
---

# Qualcomm Flight RB5 Pre-Flight Setup
{: .no_toc }

## Using QGroundControl for QGC

We will be using QGroundControl as a means to perform the pre-flight setups and checks as needed.

## Sensor Calibration

Qualcomm Flight RB5 flight sensors are delivered pre-calibrated from the factory. PX4 will warn you if your compass needs to be re-calibrated depending on your location and environmental conditions. If so, or if you'd like re-calibrate all sensors, follow the [px4 sensor calibration precedure](/px4-calibrate-sensors/).

## RC Radio Setup

### Bind Receiver to Transmitter

The RB5 has a Spektrum receiver, it needs to be paired (our "bound") with a transmitter.

Initially, an additional piece of hardware with instructions will be shipped to enable the receiver to be put in binding mode until the software solution is available

### Calibrate Radio

Once bound, power cycle the vehicle and restart QGroundControl, otherwise the radio channels will fail to show up.  Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/qgc/8-calibrate-radio.png)

### Confirm RC Settings

Every user will want to use different flight modes and different switch assignments, but for getting started with Qualcomm Flight RB5 we suggest starting with something similar to this configuration and working from there.

- "Flap Gyro" switch left of Spektrum Logo
  - Channel 6
    - Up position:     Manual Flight Mode
    - Middle Position: Position Flight Mode
    - Down Position:   Offboard Flight mode

- "Aux2 Gov" switch right of Spektrum Logo
  - Channel 7
    - Up position:     Motor Kill Switch Engaged
    - Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/qgc/9-flight-mode-config.png)

If you have a Spektrum DX6e or DX8 radio with a clean acro-mode model you can accomplish the above channel mapping by loading the following config file

[https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params](https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params)

Still confirm the mapping in QGroundControl before flight!

## Next Steps

Now you are ready for [first flight with this guide](/Qualcomm-Flight-RB5-user-guide-first-flight/)
