---
layout: default
title: RB5 Connect to GCS
parent: Qualcomm Flight RB5 User Guide
grand_parent: Qualcomm Flight RB5
nav_order: 25
has_children: false
permalink: /Qualcomm-Flight-RB5-user-guide-connect-gcs/
---

# Qualcomm Flight RB5 Connect to GCS
{: .no_toc }

## Connect to QGroundControl

We'll use QGroundControl for the GCS.  First, verify that PX4 is running on your Qualcomm Flight RB5 using the following 

```bash
systemctl status rb5-px4-start
```

If it is not running, it can be enabled to run on boot using the following:

```bash
systemctl enable rb5-px4-start
```

It can also be disabled from running at boot using:

```
systemctl disable rb5-px4-start
```

More information about the services is [located here](/Qualcomm-Flight-RB5-sdk-services/)

## Network Layout

In order to connect to QGC, ensure that your host computer is connected to the same network as your Qualcomm Flight RB5.

If PX4 is running and both devices are connected to the network then the Qualcomm Flight RB5 should automatically detect the host PC running QGC and connect.

![voxl-qgc-connected.png](/images/userguides/m500/voxl-qgc-connected.png)

## Connecting to QGC over 5G

In order to connect QGC over the LTE or 5G network, a VPN will be needed in order to complete the bridge from the cellular network to the 

A typical use case of an LTE connection is to connect a drone to a Ground Control Station (GCS) such as [QGroundControl](http://qgroundcontrol.com/). Typically the GCS is also connected to an LTE network. In this case, both the drone and the GCS have been assigned IP addresses on the LTE network that are private to the carrier network. This prevents the drone from directly connecting to the GCS since their IP addresses are not visible to each other over the internet.

There are a variety of ways to solve this issue. ModalAI uses a VPN as the preferred solution.

![LTE data flow](/images/userguides/rb5/lte-data-flow.png)

This diagram shows how the VPN solution works. In this example a server is allocated with a static IP address, `35.236.55.229`. You can easily set your own server up with [Google Cloud Platform](https://cloud.google.com/), [Amazon Web Services](https://aws.amazon.com/), [Microsoft Azure](https://azure.microsoft.com/en-us/), etc. Ubuntu is our preferred OS for our servers. Our VPN is setup using the [OpenVPN](https://openvpn.net/) software package.

In the example above, once the drone has connected to the AT&T network, it obtains the IP address `10.47.x.y` and the GCS, once connected, obtains the address `10.47.x.z`.

With the OpenVPN server software running on the cloud server and OpenVPN client software on both the drone and the GCS, the devices can now connect and get VPN IP address assignments. In this diagram, the drone is assigned `10.8.0.6` and the GCS is assigned `10.8.0.8`. The drone can now communicate directly to the GCS using the `10.8.0.8` IP address.

It is desirable for the drone and the GCS to always get the same IP address when connecting to the VPN. This is possible by assigning each separate network endpoint a unique security certificate. When each endpoint connects using it's certificate it can be configured to receive the same address every time.

### OpenVPN client on Qualcomm Flight RB5

The Qualcomm Flight RB5 comes with OpenVPN pre-installed

In order to use OpenVPN on the Qualcomm Flight RB5, you will need to supply a configuration file and the certificates / keys for your VPN. These files can be placed in the directory `/etc/openvpn`.

The following is an example of running OpenVPN as a daemon:

```bash
cd /etc/openvpn

openvpn --daemon --script-security 2 --config vpn-key.ovpn
```

## Confirm Connection

If connection succeeds, you should get a connected status in QGC.

## Next Steps

Prepare for first flight with [this guide](/Qualcomm-Flight-RB5-user-guide-pre-flight-setup/)
