---
layout: default
title: Qualcomm Flight RB5 System Image
parent: Qualcomm Flight RB5 
nav_order: 19
has_children: false
permalink: /Qualcomm-Flight-RB5-system-image/
---

# Qualcomm Flight RB5 System Image

[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## What Is It?

The system image is essentially "the operating system", and consists of the kernel, root file system, and various firmwares for the plentiful amount of processors on board to name a few items.

## Do I need to Install?

If you have a `Qualcomm Flight RB5`, it comes pre-installed so there's nothing to do.

Occasionally we will ship updates, and you can follow the update procedure below.

## Where To Download

Please visit <https://downloads.modalai.com> and download the newest `Qualcomm Flight RB5 System Image`.

## Backup Files

<span style="color:red"> - **WARNING: ALL DATA IS LOST IN THE CURRENT SYSTEM IMAGE INSTALLATION** </span>

- Please backup the following manually for now:

```bash
adb pull /etc/modalai .
adb pull /data/misc .
```

## How to Upgrade

- Unzip the download, in this example we'll assume the download name was `1.0.3-M0052-9.1-perf.tar.gz`

```bash
tar -xzvf 1.0.3-M0052-9.1-perf.tar.gz
```

- Get ready to run the script by going into the directory you just unzipped

```bash
cd 1.0.3-M0052-9.1-perf
```

- Now, attach the RB5 via USBC and ensure the unit is powered on
- Run the following:

```bash
sudo ./full-flash.sh
```

## What's Next

You should [install the SDK](/Qualcomm-Flight-RB5-sdk-installation/).

## Release Notes

Coming soon.
