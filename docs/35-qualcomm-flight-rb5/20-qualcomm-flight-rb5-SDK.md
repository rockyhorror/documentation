---
layout: default
title: Qualcomm Flight RB5 SDK
parent: Qualcomm Flight RB5 
nav_order: 20
has_children: true
permalink: /Qualcomm-Flight-RB5-sdk/
---

# Qualcomm Flight RB5 Reference Drone SDK Overview

[Buy Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---
## RB5 Flight SDK Overview

![RB5 SDK Overview](/images/userguides/rb5-sdk/rb5-sdk-overview.png)

## Available Packages

| Name              | Description |
| ---               | --- |
| rb5-camera-server | Publishes camera frames and meta data to named pipes |
| rb5-qvio-server   | Publishes VIO data to named pipe |
| rb5-voa-server    | Published VOA data (front stereo, rear stereo, chirp) |
| rb5-voa-to-px4    | Published VOA data to PX4 |
| rb5-chirp-server  | Published depth data from chirp sensors to named pipe |
| rb5-modem         | Manages 5G modem connection |
| rb5-streamer      | Video streaming management |
| rb5-mv            | 32-bit support libraries to use Qualcomm MV SDK on 64-bit system |


## Image Sensor Support

Below captures the image sensor data paths at a high level.

[View in fullsize](/images/datasheet/RB5/rb5-camera-framework.png){:target="_blank"}

![RB5.png](/images/datasheet/RB5/rb5-camera-framework.png)
