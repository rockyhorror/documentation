---
layout: default
title: VOXL ESC with PX4
parent: Modal ESC User Guides
grand_parent: Modal ESC
nav_order: 20
permalink: /modal-esc-px4-user-guide/
youtubeId: 4cXsuSaW-3k
---

# Using VOXL ESCs with PX4
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL Flight and Flight Core systems provides the ability to easily and quickly interface with UART ESCs.  The following guide provides you the necessary details on doing so.

## Requirements

**NOTE!  This feature is currently in developer preview and requires you to load the PX4 FW from a branch**

- VOXL Flight or Flight Core with [this branch](https://github.com/modalai/px4-firmware/tree/modalai-esc)
  - Note: this is scheduled to release with PX4 v1.12
- A ModalAI UART ESC [VOXL ESC v2, PN: M0049 - 4-in-1 UART ESC (gen2)](/modal-esc-datasheet/)
- UART ESC Serial Cable [MCBL-00013-1](/cable-datasheets/#mcbl-00013)

## Video Tutorial

For those of you who prefer videos, we have a video tutorial here that covers the software configuration and usage.  Note, you'll want to look at the hardware connections below as that part is skipped in this video.

TL;DR

{% include youtubePlayer.html id=page.youtubeId %}

## Hardware Connections

The guide here describes the connectivity: [VOXL ESC v2 Manual](/modal-esc-v2-manual/)

## PX4 Setup

### Enable UART ESC

To enable the UART ESC feature, set the `UART_ESC_CONFIG` parameter to a value of `1` and power cycle the vehicle.

You can use QGroundControl's **Parameter** feature to query and modify the `UART_ESC` params.

The remainder of the settings can be left as default if you followed the hardware connections described in the previous section.

#### TELEM3

The UART ESC uses /dev/ttyS1, same as `TELEM3`.  On bootup, if any MAVLink instance is configured to use `TELEM3`, it will be disabled via `boards/modalai/fc-v1/init/rc.board_defaults`.

### PX4 Params

The following are the PX4 parameters that are used by the `modalai_esc` driver:

| Name | Description | Min > Max (Incr.) | Default | Units |
|--- |--- | :-: | :-: | :-: |
| `UART_ESC_BAUD` | Baud rate between FlightController and ESC | (1) | 250000 | Bits/sec |
| `UART_ESC_CONFIG` | UART ESC Configuration <br>**Values:** <br> - 0: Disabled <br> - 1: ModalAI ESC <br><br> **Reboot Required** | | 0 (Disabled) | |
| `UART_ESC_MOTOR1` | ESC CH0 (Motor 1) mapping to PX4 motor index.  Negative value reverses spin. | -4 > 4 <br>(1) | 3 | |
| `UART_ESC_MOTOR2` | ESC CH1 (Motor 2) mapping to PX4 motor index.  Negative value reverses spin. | -4 > 4 <br>(1) | 2 | |
| `UART_ESC_MOTOR3` | ESC CH2 (Motor 3) mapping to PX4 motor index.  Negative value reverses spin. | -4 > 4 <br>(1) | 4 | |
| `UART_ESC_MOTOR4` | ESC CH3 (Motor 4) mapping to PX4 motor index.  Negative value reverses spin. | -4 > 4 <br>(1) | 1 | |
| `UART_ESC_RPM_MAX` | Maximum RPM, used by actuator output to determine thrust fullscale. | (1) | 15000 | RPM |
| `UART_ESC_RPM_MIN` | Minimum RPM, used by actuator output to determine thrust fullscale. | (1) | 5500 | RPM |

### RGB LED Codes

This is currently a work in progress.  The standard LED codes should output to the all the LED channels in a similar way as other PX4 RGB LED peripherals with the exception that the `breathing` pattern is replaced by a fast blink.
