---
layout: default
title: VOXL ESC v2
parent: Modal ESC User Guides
grand_parent: Modal ESC
nav_order: 5
permalink: /modal-esc-v2-manual/
---

# VOXL ESC v2 Manual
{: .no_toc }

## Software Components

| Function | Description | Resource Link |
| :------: | ----------- | :------------: |
| **Bootloader** | Provides firmware update capability over UART and ensures integrity of the firmware before executing it | Installed on board during production |
| **Firmware** | Main software that performs all motor control functionality | Coming soon to [developer.modalai.com](developer.modalai.com) |
| **Firmware Parameters** | Parameters that allow user to change behavior of the Firmware without changing the Firmware itself | [github](https://gitlab.com/voxl-public/voxl-esc) |
| **VOXL ESC Tools** | Tools for testing / tuning ESC using a PC | [github](https://gitlab.com/voxl-public/voxl-esc) |

## VOXL ESC Tools
VOXL ESC Tools is a Python software package which allows testing, calibration and diagnostics of ESCs using a PC. The ESC tools consist of two parts:
- voxl-esc-tools-bin : proprietary software (voxl-esc-tools-bin-x.x.x.zip) : download from [developer.modalai.com](https://developer.modalai.com/asset)
- voxl-esc-tools : open source software : [github link](https://gitlab.com/voxl-public/voxl-esc)

---

## Hardware Diagrams

The 4-in-1 ESC board consists of four almost identical copies of a single ESC design.

### Single ESC Block Diagram
![Single ESC Channel Block Diagram](/images/userguides/escs/voxl-v2-esc-single-block-diagram.png)

The four ESCs share some components
* 3.3V Voltage regulator
* UART communication lines
* ID_SELECT2 pin (see ID Assignment section)

---

### Communication Block Diagram
![Communication Block Diagram](/images/userguides/escs/voxl-v2-esc-comms-diagram.png)

### Connector Information and Pin-outs
**UART Connector J2**
* Connector on board : Hirose DF13A-6P-1.25H
* Mating connector : DF13-6S-1.25C
* Pre-crimped wires : (Digikey) H4BBT-10112-B8 or similar
* Connector is compatible with VOXL ESC V1, except V2 board has additional UART2 functionality
* 3.3V signals (5.0V input is acceptable)

| Pin >      |     1    |     2    |     3    |     4    |   5   |     6    |
| :------:   | :------: | :------: | :------: | :------: | :---: | :------: |
| Function > | VREF_OUT | UART1 RX | UART1 TX | UART2 RX |  GND  | UART2 TX |

---

**PWM Input Connector J3**
* Connector on board : BM06B-GHS-TBT
* Mating connector : GHR-06V-S
* Pre-crimped wires : (Digikey) AGHGH28K305 or similar
* 3.3V signals (5.0V input is acceptable)

| Pin >      |     1    |     2    |     3    |     4    |     5    |     6    |
| :--------: | :------: | :------: | :------: | :------: | :------: | :------: |
| Function > | VREF_OUT | PWM_IN0  | PWM_IN1  | PWM_IN2  | PWM_IN3  |    GND   |

---

## ESC Software Start-up Procedure
![Boot-up Procedure](/images/datasheet/escs/modalai_esc_bootup_process.png)
### Start-up Error Cases

| Error | Notes |
| ----- | ----- |
| Firmware Verification | Verification of the firmware could fail either because the firmware was never installed (fresh board) or the firmware has been corrupted. In this case, the status LED will continuously blink fast (10Hz) and the ESC will remain in bootloader mode indefinitely, waiting for host to install the firmware |
| Firmware Update | Firmware update could be rejected by the Bootloader if incompatible firmware is provided by host. Incompatible firmware is detected before erasing existing firmware, so the old firmware should be retained in this case. Also, firmware update could fail in the middle of the update (loss of power or communication) - in this case the previous firmware would be lost and the ESC would end up without valid firmware. To fix this, simply re-install the firmware. |
| Firmware Params Verification | If firmware parameters are corrupt or were never installed, ESC cannot be used normally. The firmware will enter a fallback mode, where it uses 57600 baud rate for UART communication and waits for host to upload valid parameters. Upon entering such condition, the firmware will blink 10 times with status LED and make a "sad" motor tone to signal the error. To fix this, use ESC Tools to upload valid firmware parameters |

---

## ID Assignment
* Each of the 4 ESCs on the 4-in-1 board has a unique ID (0-7). The ID is used in software for UART communication.
* IDs are hard-wired on the PCB using 3 inputs to each MCU (thus giving 2^3=8 possible IDs). Two of these pins are different for each MCU and third pulled low by default for 0-3 IDs. (third pin is ID_SELECT2 - see Hardware Diagrams section) is shared for all ESCs, and is selectable via resistor (see [datasheet](/modal-esc-datasheet/), "ID 4-7 Selection Jumper" in Hardware Overview). If pulled high via a resistor, all ESCs will have IDs in range (4-7).
* This allows to easily configure a board (using hardware resistor) for the desired ID range.
* The bootloader and firmware will read the state of all three ID_SELECTx pins and determine their IDs during initialization.

---

## Communication protocols
Coming Soon

## Other Resources
 - [VOXL ESC V2 Datasheet](/modal-esc-datasheet/)
 - [PX4 Integration User Guide](/modal-esc-px4-user-guide/)

## FAQ
Q: When I apply power, the board continuously flashes the LED very quickly and nothing else happens (not motor tones). What is wrong?  
A: If board is in this state, it means it has only bootloader installed but no firmware. Upload appropriate firmware using ESC Tools

---

Q: When I apply power, the board flashes LED for 1 second, then flashes 10 times more slowly and emits a sad motor tone. What is wrong?  
A: Try to use ESC scan feature in ESC Tools. If it detects ESC at 57600, then it means there are no params uploaded. Upload appropriate params using the ESC Tools
