---
layout: default
title: Flight Core
nav_order: 14
has_children: true
permalink: /flight-core/
---

# Flight Core
{: .no_toc }

Flight Core is a PX4 Drone Flight Controller - Assembled in the USA. Flight Core can be paired with VOXL for obstacle avoidance and indoor or outdoor GPS-denied navigation. Flight Core can also be used independently as a standalone, high-performance, secure flight controller. Flight Core is a part of the Blue UAS Framework and is NDAA '20 Section 848 compliant.

Buy [Here](https://www.modalai.com/products/flight-core)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/10/flight-core)

![flight-core-img](/images/flight-core/flight-core.jpg)


