---
layout: default
title: VOXL CAM
nav_order: 25
has_children: true
permalink: /voxl-cam/
---

# VOXL-CAM
{: .no_toc }
VOXL-CAM is for robotic perception what GoPro™ was for action video.

Buy [Here](https://www.modalai.com/products/voxl-cam)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/21/voxl-cam)

![voxl-cam-r1-c1-index.png](/images/userguides/voxl-cam/voxlcam.jpg)


## Versions and Options
{: .no_toc }

## Version 1

- [Datasheet](/voxl-cam-v1-datasheet/)
- [Manual](/voxl-cam-v1-manual/)


| Options                       | Description                                                     | Status             | SKU Configuration | 
|---                            |--                                                               |---                 |---                |
| Compute                       | [VOXL](/voxl-datasheet/)                                        | Available          | C0001             | 
| Perception                    | ToF, Hires and Tracking                                         | Coming Soon        | -C06              | 
|                               | ToF, Stereo and Tracking                                        | Available          | -C07              | 
| Flight Controller (optional)  | [Flight Core v1](/flight-core-datasheet/)                       | Coming Soon        | -F1               |
| Modem Options     (optional)  | [LTE v2, WP7610](/lte-modem-and-usb-add-on-v2-datasheet/)       | Coming Soon        | -M4               | 
|                               | [LTE v2, WP7607](/lte-modem-and-usb-add-on-v2-datasheet/)       | Coming Soon        | -M5               |