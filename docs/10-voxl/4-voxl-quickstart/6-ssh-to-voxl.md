---
layout: default
title: SSH to VOXL
parent: VOXL Quickstart
grand_parent: VOXL
nav_order: 6
permalink: /ssh-to-voxl/
---

# SSH to VOXL

## Instructions

If your host computer is on the same network as the VOXL, you can SSH into it.

If the VOXL is setup as a Soft AP, the IP address is `192.168.8.1`, otherwise you'll need to find it's IP address with `ifconfig` as described in the [previous step](/wifi-setup/).

```bash
me@mylaptop:~$ ssh root@192.168.8.1
(password: oelinux123)
```

Later, in the [ROS Host PC Setup Instructions](/setup-ros-on-host-pc/), you will configure an alias to speed up the ssh process.


## Known Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the GNOME Terminal, the default terminal emulator of Ubuntu. The solution is to use another terminal emulator such as xterm or Konsole instead of GNOME Terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4
