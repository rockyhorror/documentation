---
layout: default
title: VOXL Hardware Setup
parent: VOXL Quickstart
grand_parent: VOXL
nav_order: 2
permalink: /voxl-hardware-setup/
---

# VOXL Hardware Setup

![VOXL HW Setup](/images/quickstart/voxl/voxl-dk-hw-setup.png)

- Connect the power cable to J1 as shown
- Connect the other side of the power cable to the VOXL Power Module as shown
- Connect one side of the USB cable to the host computer and connect the other side to J8 as shown.
  - **Note:** the Micro USB connector will consume only a portion of the USB 3.0 port.  A USB 3.0 cable can be used as well.
- Now plug in the power supply (**5V/6A DC output**) to the wall, and plug it in to the VOXL Power Module barrel jack as shown

WARNING!
{: .label .label-red }
- If your Power Module has a barrel jack, **DO NOT** plug anything other than 5V into the barrel jack or you **WILL DAMAGE THE BOARD**. The barrel jack bypasses the regulator and feeds VOXL directly.

## Next Steps

Now that the hardware is setup, let's [set up ADB](/setup-adb/).
