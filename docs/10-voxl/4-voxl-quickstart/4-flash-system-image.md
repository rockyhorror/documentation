---
layout: default
title: Flash System Image
parent: VOXL Quickstart
grand_parent: VOXL
nav_order: 4
permalink: /flash-system-image/
---

# Flash System Image (optional)

**THIS PROCESS WILL WIPE VOXL's FILE SYSTEM AND OPTIONALLY THE DATA PARTITION**

The VOXL System Image includes the root file system and bootloader partitions. For details on the system image changes, see the [VOXL System Image ChangeLog](/voxl-system-image/). These instructions assume the user has [set up ADB](/setup-adb/).

Starting with system image v2.3.0, all software updates (with the exception of kernel upgrades) are handled by the opkg package manager and can be done over WiFi. We **HIGHLY RECOMMEND** proceeding with the following steps if you are on system image v2.2.0 or older. Once you are on v2.3.0 or newer, software updates will be much easier.

Once you are on system image v2.3.0 or newer, it is only necessary to update the system image to add new hardware driver support. Updating the System Image should not be necessary unless you are aware of hardware support additions that you need. If you are on v2.3.0 or newer, you can likely skip this step and proceed to [WiFi setup](/wifi-setup/).


## Check current system image version

Power on the VOXL board and connect a USB cable. You can now use the voxl-version utility through ADB to check the system image version. This will also print out version information for other key system components. When sending troubleshooting requests to a ModalAI team member, please begin by sending the output of this command so we may better help you.

```bash
me@mylaptop:~$ adb shell voxl-version
--------------------------------------------------------------------------------
system-image:    ModalAI 2.3.0 BUILDER: ekatzfey BUILD_TIME: 2020-04-11_23:05
kernel:          #1 SMP PREEMPT Mon Nov 11 22:28:13 UTC 2019 3.18.71-perf
factory-bundle:  1.0.1
sw-bundle:       0.1.1
--------------------------------------------------------------------------------
```

## 1. Download and Unpack Image

Download latest image from [https://developer.modalai.com/asset](https://developer.modalai.com/asset). Then unpack the image (where `M-m-b` describes the version, `x.y.z` describes included factory bundle):

```bash
me@mylaptop:~$ tar -xvf modalai-M-m-b-x.y.z.tar.gz
me@mylaptop:~$ cd modalai-M-m-b-x.y.z
me@mylaptop:~/modalai-M-m-b-x.y.z$
```

## 2. Power up and connect VOXL

Ensure the VOXL is powered from the DC power supply (**5V/6ADC output**) and connect the VOXL to the host system via micro USB cable. Then ensure the device is visible with `adb devices`.

```bash
me@mylaptop:~$ adb devices
List of devices attached
da2ec579	device
```

If the device fails to connect using ADB, please check the [Troubleshooting](#troubleshooting) section below

## 3. Start the Flashing Process

*Note: the following are required for the script*
- `android-tools-adb` Linux package
- `android-tools-fastboot` Linux package

*Note: you may need to run the install script as `sudo` user inside the folder you just unpacked to begin the process*

*Note: this same process can be used if you have forced the device into fastboot [using this procedure](/usb-expander-and-debug-manual/#forcing-the-voxl-into-fastboot)*

```bash
me@mylaptop:~/modalai-M-m-b$ sudo ./install.sh
```

## 4. Choose to retain the /data/ partition or not


Starting with system image `2.3.0`, you will be prompted if you'd like to retain the `/data` partition. This is the largest partition and is used for some calibration files and large docker images that you may want to preserve when flashing.

```bash
This process will completely wipe your VOXL
Would you like to preserve the /data/ parition which contains
things like calibration, wifi config, and docker images?
To preserve files in /data/ answer Y
To wipe /data/ for a full factory flash answer N

Enter Y or N: N
```

Make your selection and hit `ENTER` to continue.

This process will take approximately 3 minutes. When the process is complete VOXL will reboot automatically.

With System Image `2.3.0` and newer, the version will display when the process finishes:


## 5. Optionally install the voxl-suite software packages

Later you can connect VOXL to a WiFi network and install/upgrade software packages through the internet with the opkg package manager. If you don't wish to connect VOXL to the internet or just want to shortcut that process now, the system image installer can fetch the latest stable voxl-suite packages through your computer's internet connection and install them on VOXL over ADB now.

We suggest answering yes to this question so you don't have to install the packages yourself later.

```bash
Finished installing the System Image!

Next, you will want to install the voxl-suite software packages.

Do you want this installer to fetch the latest packages and automatically install them? (y/n)
```

## 6. Check the Version Manually (Optional)

Once VOXL has rebooted from the install script, you can adb back in and see the updated voxl-version output.

```bash
me@mylaptop:~$ adb shell voxl-version
--------------------------------------------------------------------------------
system-image:    ModalAI 2.3.0 BUILDER: ekatzfey BUILD_TIME: 2020-04-11_23:05
kernel:          #1 SMP PREEMPT Mon Nov 11 22:28:13 UTC 2019 3.18.71-perf
factory-bundle:  1.0.1
sw-bundle:       0.1.1
--------------------------------------------------------------------------------
```

## Troubleshooting

Ensure that the fastboot package is installed using `apt-get install android-tools-fastboot`.

If you think VOXL is bricked, you can usually [Force VOXL into Fastboot Mode](/usb-expander-and-debug-manual/#forcing-the-voxl-into-fastboot).

If problems continue, please feel free to reach out and [contact us](https://modalai.com/support).

## Next Steps

Next, we can [set up WiFi](/wifi-setup/).
