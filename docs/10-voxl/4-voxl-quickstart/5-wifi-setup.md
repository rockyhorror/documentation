---
layout: default
title: WiFi setup
parent: VOXL Quickstart
grand_parent: VOXL
nav_order: 5
permalink: /wifi-setup/
---

# WiFi setup
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL supports two wifi modes, Station and SoftAP. Station Mode refers to the VOXL being configured into a typical 'WiFi client' on a wireless network like your phone or laptop. Software enabled Access Point Mode (SoftAP) configures VOXL to generate its own hotspot so that other WiFi devices can connect to it. This is useful if you are in the field away from your home or office WiFi network.

By default, the unit ships configured in SoftAP mode with an SSID of `VOXL-XX:XX:XX:XX:XX:XX` with password `1234567890` (where XX:XX:XX:XX:XX:XX is the unit's MAC address).

These instructions require the `voxl-utils` package which is included in the base system image starting with v2.3.0 and was installed on your VOXL at the factory prior to that.

To continue to the next step in the [quickstart sequence](/voxl-quickstarts/), you will need to configure VOXL's WiFi in station mode and connect to a network with an internet connection so you can upgrade software packages. After that, you may refer to this page again to reconfigure WiFi in SoftAP mode or to connect to a new network.

## Setup Hardware

- Disconnect power from the VOXL
- Attach Wi-Fi antennas as shown below (note it takes a little force to connect the MHF4 U.FL connectors)
- Power up VOXL
-

![VOXL Wi-Fi](/images/quickstart/voxl/wifi-antennas.png)



## Configure Station Mode

To configure the VOXL into Station Mode, run the following command:

```bash
me@mylaptop:~$ adb shell voxl-wifi station <SSID> <Password>
```

A reboot of VOXL is required to finish the setup:

```bash
me@mylaptop:~$ adb reboot && adb wait-for-device
```


## Configure SoftAP Mode

To configure the VOXL into Access Point Mode with your desired SSID, use the `voxl-wifi` utility and reboot for the changes to take effect.

```bash
me@mylaptop:~$ adb shell voxl-wifi softap <SSID>
me@mylaptop:~$ adb reboot && adb wait-for-device
```

The password to connect to the VOXL's hotspot is `1234567890` and VOXL will assign itself the IP `192.168.8.1`. Your device will be assigned an IP with DHCP in the 192.168.8.X subnet, usually 192.168.8.60, but not always.


## Check VOXL's IP Address

VOXL will connect to the network when it powers up. If you need to locate its IP address, you can connct via ADB and run the following command:

```bash
me@mylaptop:~$ adb shell ifconfig
lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:81 errors:0 dropped:0 overruns:0 frame:0
          TX packets:81 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:6140 (5.9 KiB)  TX bytes:6140 (5.9 KiB)

wlan0     Link encap:Ethernet  HWaddr 48:5F:99:9D:40:9D
          inet addr:192.168.8.1  Bcast:192.168.8.255  Mask:255.255.255.0
          inet6 addr: fe80::4a5f:99ff:fe9d:409d/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:218 errors:0 dropped:0 overruns:0 frame:0
          TX packets:141 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:3000
          RX bytes:23365 (22.8 KiB)  TX bytes:13134 (12.8 KiB)
```

Here you can see the localhost interface at 127.0.0.1 just as you have on your Linux host computer. The wlan0 interface is for the onboard WiFi.

Note: It's not currently possible to assign a static IP, only DHCP is supported. If you need a static IP on your own wifi network it's best to configure that in your router's software.



## Check WiFi Mode

Print the current mode (softap or station)

```bash
me@mylaptop:~$ voxl-wifi getmode
station
```


### Configure hostname for Soft AP Mode (Optional)

It may be desired to have a dedicated hostname for your target instead of using an IP. This [thread](https://gitlab.com/voxl-public/documentation/issues/3) describes how to enable.

__TODO__ Clean up contents of [thread](https://gitlab.com/voxl-public/documentation/issues/3) into clear instructions.



## Next Steps

Next, we can [SSH into](/ssh-to-voxl/) VOXL.

