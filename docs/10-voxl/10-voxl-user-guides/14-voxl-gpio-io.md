---
layout: default
title: VOXL GPIO
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 14
permalink: /voxl-gpio-io/
---

# VOXL GPIO
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

All non-PMIC GPIO pins that are broken out on VOXL's headers are internally mapped to the Sensors DSP (SDSP). This also means that you cannot manipulate GPIO with reads and writes to /dev/gpio* as other embedded Linux systems may allow. We provide a simple library, libvoxl-io to enable communication with GPIO functionality from the applications processor (Linux userspace). This layer hides the SDSP RPC calls and allows compiling programs that use GPIO without the need of Hexagon DSP toolchain or build environment.

The libvoxl-io library header and API description can be found here:
[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/include/voxl_io.h)

The library is included with the [VOXL Software Bundle](/voxl-supporting-software/).


## GPIO Pin Numbers

Any IO pin exposed on J7, J10, J11 and J12 can be used as GPIO pin (not concurrently with any other functionality, such as UART, I2C, or SPI)

More details on the above ports and their pinouts can be seen on the [datasheet page](/voxl-datasheet/).


## Examples

### voxl-gpio
This tool allows you to read or write GPIO state from linux command line. Upon executing ```voxl-gpio```, the GPIO pin will be initialized to the desired function (read or write).

[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-gpio.c](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-gpio.c)

```
/ # voxl-gpio
Usage:
voxl-gpio read <pin_number>
voxl-gpio write <pin_number> <0,1>
```
- ```voxl-gpio write``` will not print anything to command line, unless there is an error (printed to stderr). Return value is 0 if success, otherwise -1.
- ```voxl-gpio read``` will print the state of the pin to cmd line (0 or 1), unless there is an error (printed to stderr). Return value is 0 if success, otherwise -1.

### voxl-gpio-loopback
Another simple GPIO example is GPIO loopback tester.

[https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-gpio-loopback.c](https://gitlab.com/voxl-public/libvoxl_io/blob/master/lib/apps/voxl-gpio-loopback.c)

```
/ # voxl-gpio-loopback
GPIO Loopback Test
Usage:
voxl-gpio-loopback <output_pin> <input_pin>
  Output pin will be toggled high / low while reading input pin state.
  If the two pins are externally connected, their state should match.
```

## Performance
- GPIO state is persistent after exiting libvoxl_io functions or calls to voxl-gpio
- Opening the GPIO Pin (```voxl_gpio_init```) can take around 20 ms (if ```libvoxl_io``` is already loaded in SDSP) or up to 100ms (if ```libvoxl_io``` is not loaded on SDSP).
- Once GPIO Pin is open, reading / writing (```voxl_gpio_read``` and ```voxl_gpio_write```) should take 1-2ms (assuming ```libvoxl_io``` is already loaded in SDSP). However, since ```voxl-gpio``` tool opens and closes the pin every time, the minimum call time for ```voxl-gpio``` read or write is 20ms and can be higher depending on CPU load.
- Actual GPIO read and write calls on SDSP are much quicker (100us or less), but CPU<->SDSP communication introduces additional delay, resulting in 1-2ms as seen on the CPU side.
- "Loaded on SDSP" means there is another process currently using ```libvoxl_io``` on SDSP. SDSP will clean up and unload any library which is not used.
- For example, calling ```voxl-gpio``` multiple times (one after another) will result in loading and unloading ```libvoxl_io``` each time, unless another process is continuously using ```libvoxl_io```.

## Warnings and Limitations
- A GPIO pin should not be open concurrently by several processes, otherwise undesired behavior may occur (such as one process closing the pin while another is still using it - would result in second process unable to continue using the pin).
- Concurrent usage of the same pin by multiple processes is not prohibited by ```libvoxl_io```, but has to be handled carefully, especially setting pin direction and closing the pin.
