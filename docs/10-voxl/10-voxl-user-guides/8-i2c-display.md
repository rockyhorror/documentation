---
layout: default
title: How to connect an I2C display to VOXL
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 8
permalink: /i2c-display/
---

# How to connect an I2C display to VOXL
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

Here at ModalAI, Inc. we like to use a small I2C display on our robot to give us some
visual feedback on what is happening in, for example, the boot up sequence.

We are use a 0.96" 128x64 ssd1306 OLED display.

Here is a link to the one we use: [Amazon page](https://www.amazon.com/DIYmall-Serial-128x64-Display-Arduino/dp/B00O2KDQBE/ref=sr_1_2?keywords=DIYmall+0.96%22+Inch+I2c+IIC+Serial+128x64+Oled+LCD+LED+White+Display+Module+for+Arduino+51+Msp420+Stim32+SCR&qid=1568326764&s=gateway&sr=8-2)

![voxl-dk](../../images/userguides/voxl/i2c-display.jpg)

## Hardware

- Custom cable with the following pinout:

| VOXL - J10,  DF13-6S-1.25C connector | I2C Display |
|---  | --- |
| Pin 1 - 3.3V | VCC |
| Pin 2 - TX| NC |
| Pin 3 - RX | NC |
| Pin 4 - SDA | SDA |
| Pin 5 - GND | GND |
| Pin 6 - SCL | SCL |

## Target Software

-  I2C Display Example Application: [Download](https://storage.googleapis.com/modalai_public/modal_packages/latest/i2c_display_0.0.2_8x96.ipk)

- Install the package to the target:

```bash
adb push i2c_display_x.x.x.ipk /home/root/ipk
adb shell "opkg install /home/root/ipk/i2c_display_x.x.x.ipk"
```

## Usage

Once the software package has been installed you use the command `i2c_display`. If you do not supply
an argument it will display the ModalAI, Inc. splash screen. Otherwise you can give it
a one or two word phrase to put on the display. For example: `i2c_display "TIME INIT"`

## Hardware Setup

- Disconnect power from the VOXL
- Connect the VOXL's J10 to the i2c display using the custom cable described above
- Reconnect power

