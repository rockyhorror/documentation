---
layout: default
title: Thermal and Performance
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 10
permalink: /thermal-and-performance/
---

# Thermal and Performance
{: .no_toc }

Resources for debugging thermal performance on VOXL platform

VOXL, and all Snapdragon platforms, use frequency scaling to control thermal and try to maximize performance. The VOXL will limit maximum frequency if the SoC becomes too hot. Therefore it is important to manage resources carefully, like in all embedded platforms. A good article on thermal frequnecy scaling can be found [here](https://www.androidauthority.com/what-is-cpu-frequency-scaling-672253/).

This page is dedicated to some of the tools available on VOXL to manage performance.

Mechanical solutions, such as a [fan](https://www.modalai.com/products/voxl-cooling-fan) or a heatsink, help maximize performance. We have found this 5g [cooling fan](https://www.modalai.com/products/voxl-cooling-fan) to have a good tradeoff in performance and weight.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## voxl-inspect-cpu

[voxl-inspect-cpu](https://docs.modalai.com/voxl-inspect-cpu/) is a useful tool for monitoring CPU and GPU utilization and core temperatures.

## voxl-perfmon

(deprecated in favor of [voxl-inspect-cpu](https://docs.modalai.com/voxl-inspect-cpu/))

A Python script to monitor CPU and GPU usage as well as individual core temperatures called `voxl-perfmon` is bundled in the `voxl-utils` package that is installed by default on the target.

[Usage can be found here](/voxl-utils/)

[Source code can be found here](https://gitlab.com/voxl-public/voxl-utils/tree/master/src/voxl-perfmon)


## Commands

### Governor

Set CPU 0 to performance mode

```$ echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor```

View CPU 0's governor mode

```$ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor```

Set GPU to performance mode

```$ echo performance > /sys/class/kgsl/kgsl-3d0/devfreq/governor```

View GPU's governor mode

```$ cat /sys/class/kgsl/kgsl-3d0/devfreq/governor```

#### Other CPU Governor Options

| Governor | Description |
| --- | --- |
| performance | Run the CPU at the maximum frequency. |
| powersave | Run the CPU at the minimum frequency. |
| userspace | Run the CPU at user specified frequencies. |
| ondemand | Scales the frequency dynamically according to current load. Jumps to the highest frequency and then possibly back off as the idle time increases. |
| conservative | Scales the frequency dynamically according to current load. Scales the frequency more gradually than ondemand. |

### Frequency

View CPU 0's current frequency

```$ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq```

View CPU 0's minimum frequency

```$ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq```

View CPU 0's maximum frequency

```$ cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq```

View CPU 3's available frequencies for scaling

```
$ cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_available_frequencies
$ 307200 403200 480000 556800 652800 729600 806400 883200 940800 1036800 1113600 1190400 1248000 1324800 1401600 1478400 1555200 1632000 1708800 1785600 1824000 1920000 1996800 2073600 2150400
```

Set CPU 3's maximum frequency to 1785600  in the governor

```$ echo 1785600 > /sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq```


View GPU's current frequency

```cat /sys/class/kgsl/kgsl-3d0/devfreq/cur_freq\```

View GPU's minimum frequency

```$ cat /sys/class/kgsl/kgsl-3d0/devfreq/min_freq```

View GPU's maximum frequency

``` cat /sys/class/kgsl/kgsl-3d0/devfreq/max_freq```

View GPU's available frequencies

```
$ cat /sys/class/kgsl/kgsl-3d0/devfreq/available_frequencies
$ 3133000000 214000000 315000000 401800000 510000000 560000000 624000000
```

Set GPU's maximum frequency to 560000000  in the governor

```$ echo 560000000 > /sys/class/kgsl/kgsl-3d0/devfreq/max_freq```

### Online

See which CPU's are online

```$ cat /sys/devices/system/cpu/online```

Set CPU 2 offline

```$ echo 0 > /sys/devices/system/cpu/cpu2/online```
