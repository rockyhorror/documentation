---
layout: default
title: VOXL Onboard LED Control
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 30
permalink: /voxl-onboard-led-control/
---

# How to Control the On-board RGB LEDs on VOXL
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

VOXL has a small surface mount RGB LED located next to the fan connector J6. LEDs can be accessible using the following devices:

```
/sys/class/leds/red
/sys/class/leds/green
/sys/class/leds/blue
```

Query maximum brightness setting.. Should be 255 (optional):
```
cat /sys/class/leds/red/max_brightness
255
```

Set PWM period (microseconds) for LED PWM control (optional.. default 1khz)
```
#set to 1kHz
echo 1000 > /sys/class/leds/red/pwm_us
```

Set brightness of 127 (out of 255) for red LED:
```
echo 127 > /sys/class/leds/red/brightness
```

Turn red LED off:
```
echo 0 > /sys/class/leds/red/brightness
```
