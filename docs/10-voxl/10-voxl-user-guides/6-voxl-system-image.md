---
layout: default
title: VOXL System Image
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 6
has_children: false
permalink: /voxl-system-image/
---

# VOXL System Image

This page describes the Yocto System Image that runs on VOXL. In the [VOXL Quickstarts](/voxl-quickstarts/), you can find details on [how to flash the VOXL system image](/flash-system-image/).

Yocto Jethro Linux kernel 3.18 [Source](https://gitlab.com/voxl-public/voxl-build), [Build Instructions](https://gitlab.com/voxl-public/voxl-build/blob/master/README.md)

Yocto Jethro Linux Userspace: [Source to add new userspace packages](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro).

Many new packages can be added using bitbake. We have a bitbake project that enables building new IPK packages for install on target here: [Source](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro)

## Download Location

You can [download the system image](https://developer.modalai.com/asset) from our protected downloads page.

## Changelog

| VOXL Release   | Release Notes                                                                                                         |
|:---------------|:----------------------------------------------------------------------------------------------------------------------|
| modalai-3.3.0  | * change inodes to 3m on data partition <br> * add quectel modem support <br> * add aarch64 opencl <br> * v1.0.2 camera repo with multiple ov7251 drivers for 8 bit, vflip, hflip, vhflip(180 rotation) |
| modalai-3.2.0  | **Camera Pipeline:** <br> * add TOF in camera slot 0 to camera_config.xml <br> * remove IMX214GP and IMX378 entries in camera_config.xml <br> * set default imx214 driver to imx214_rot in camera_config.xml <br> * comment out annying log messages about focus set to infinity and mIsDepthCamera == false <br> * disable analysis stream regardless of persist parameters persist.camera.analysis.enable and persist.stereo.analysis.enable <br> * updates to support TOF mode 5 and 9, selectable at run-time <br> * enable raw mode for all native resolutions <br> * enable support for two concurrent hi-res cameras <br> * imx214: add driver with inverse readout (rot180) (additional .so) <br> * ov7251: update maximum gain and exposure settings per spec sheet <br> * ov7251: add driver with raw8 output (additional .so) <br> * imx477: fix lens roll-off correction <br><br> **TOF-Bridge / libtof-interface:** <br>  * update dep royale-spectre to 0.0.3 <br> * change library and ipk name to libtof-interface (from libtof_interface) <br> * disable using reset pin by default, since camera pipeline already resets the sensor during start-up <br> * updates to support TOF mode 5 and 9, selectable at run-time, add checks and cleanup <br> * change the lib and package name to libtof_interface, add ipk dependency on libroyale-331-spectre-4-7 <br> * add persist param for selecting camera port used for TOF, which is used to determine the gpio pin for device reset <br> <br> **Royale Spectre** <br> * modify the lib install process to make yocto happy (install to /usr/lib/royale first then copy to /usr/lib/ in postinst) <br> * export common royale headers to ipk, will be installed to /usr/include/royale <br> * disable toggling of GPIO0 in order to enable concurrency with Stereo <br> * install royale and spectre libs directly to /usr/lib/ <br><br> **VOXL HAL3 TOF** <br> * update library dependency to libtof-interface, since the lib was renamed <br> * removed proprietary PMD headers (they will be coming from royale-331-spectre-4-7) <br> * changes to support selecting tof mode and fps <br> * remove libtofbridge.a and royale and spectre .so libs, since they are part of platform build <br> * add autodetection of TOF camera port <br> * enable tof processing by default if persist.camera.modalai.tof param is not set |
| modalai-3.1.0  | Added rsync, screen, and libgphoto2 packages |
| modalai-3.0.0  | Added factory bundle 1.0.1 into the system image. Added gstreamer OMX and extra plugins to the system image. Bumped Avahi version to 0.6.32-r12.0. |
| modalai-2-5-3 (beta)  | Adds support for: <br> *IMX412 image sensors <br> * imx412: enable full range of gains and exposures |
| modalai-2-5-2  | Adds support for: <br> * VOXL HDMI Input Accessory (Auvidea B102) <br> * Microchip lan75xx Ethernet devices (VOXL Microhard Add-On v2) <br> |
| modalai-2-3-0  | Updated to new PMD libraries for latest  Time-of-Flight modules (A65) module                                          |
| modalai-2-2-0  | Added kernel driver support for FTDI Serial IO devices                                                                |
| modalai-2-1-0  | Updated to new build architecture (Docker, meta-voxl, meta-voxl-prop) <br>  Adds support for: <br> * WP760x Sierra modems <br> * Microchip lan78xx Ethernet devices <br> *  /bin/ar fix |
| modalai-1-10-0 | Removed CONFIG_ANDROID_PARANOID_NETWORK config from kernel to fix ping permissions problem when running docker images |
| modalai-1-9-0  | Adds support for: <br> * SCTP sockets <br> * IM214 image sensors                                                     |
| modalai-1-7-0  | Increase /data/ parition to 16GB                                                                                      |
| modalai-1-6-0  | Fix for DNS issue, adds virtualization support for Docker                                                             |
| modalai-1-5-0  | Uses open source kernel                                                                                               |
| modalai-1-3-0  | Adds support for: <br> * external cellular modems <br> * PMD Time of Flight cameras over MIPI <br> * UVC (webcams)    |
