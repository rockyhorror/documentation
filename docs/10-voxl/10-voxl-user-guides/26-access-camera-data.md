---
layout: default
title: How to Access Camera Frame Data
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 26
permalink: /access-camera-data/
---

# How to Access Camera Data
{: .no_toc }

The offical supported method for accessing camera data on VOXL is through MPA and voxl-camera-server. This page covers lower level methods for accessing camera data and should not be used unless you really know what you are doing and are okay breaking voxl-camera-servera nd other MPA services.

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

There are multiple ways to access camera data on VOXL. The following table enumerates those ways in increasing order of abstraction. This information is applicable to the [high-resolution](https://docs.modalai.com/voxl-hires-camera-datasheet/), [tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) and [stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) sensors.

| Method | Information | Notes |
| --- | --- | --- |
| v4l2 | TBD | Lowest level access, currently not documented for VOXL. [Kernel Docs](https://www.kernel.org/doc/html/v4.15/media/v4l-drivers/qcom_camss.html) |
| HAL3 | [Source](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocamera) | Lowest level currently documented. Supports image quality adjusted images through ISP as well as raw. |
| QMMF | [Example](https://source.codeaurora.org/quic/le/platform/vendor/qcom-opensource/libRoboticsCamera/tree/libcamera/src/qmmf_sdk?h=LE.UM.1.3.r4.5) | QMMF is Qualcomm Multi-media Framework. Has interesting video recording and streaming features, not well documented publicly. |
| libcamera | [Source](https://source.codeaurora.org/quic/le/platform/vendor/qcom-opensource/libRoboticsCamera/tree/?h=LE.UM.1.3.r4.5) <br> [Example in ROS Code](https://gitlab.com/voxl-public/voxl-cam-manager/blob/master/src/SnapdragonCameraManager.cpp) | Supports image quality adjusted images through ISP as well as raw. |
| ROS | [Docs](https://docs.modalai.com/voxl-cam-ros/) <br> [Source](https://gitlab.com/voxl-public/voxl-cam-ros) | Integrate camera processing in a standard ROS format |
| ROS2 | Coming soon | |

## Time of Flight Camera
The ToF camera pipeline is treated slightly differently. The following table contains multiple references for how to utilize ToF on VOXL.

| Method | Information | Notes |
| --- | --- | --- |
| ROS | [Docs](https://docs.modalai.com/tof-cam-ros/) <br> [Source](https://gitlab.com/voxl-public/tof_cam_ros) | | 
| HAL3 | [Source](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellotof) | |

## Video Encoding

VOXL supports up to 250Mbps of video encoding through hardware acceleration. Examples of how to use the video encoder can be found at the following resources.

| Method | Information | Notes |
| --- | --- | --- |
| OpenMax | [Example Code](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellovideo). | Standard embedded video encoding acceleration | 


