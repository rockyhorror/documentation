---
layout: default
title: VOXL Hi-res Sensor User Guide
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 36
permalink: /voxl-hi-res-sensor-user-guide/
---

# VOXL Hi-res Sensor User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Streaming High-resolution Camera

See [voxl-streamer](https://docs.modalai.com/voxl-streamer)

### Accessing Two High-resolution Cameras Simultaneously

The following are _very_ technical notes on how to enable dual high-resolution imager streaming. In short, the two sensors need different i2c addresses. That means either two different imagers need to be used, like IMX214 and IMX377, or their i2c selection addresses need to be modified. 

### Using two hi-res cameras at the same time
- two hi-res cameras need to be plugged into connectors J2 (CAM0) and J3 (CAM1) (see connectors datasheet).
- the hi-res cameras need to have different I2C addresses, otherwise I2C communication will collide and cause unpredictable behavior
   - IMX412 camera has a resistor pair which can be used to change the default I2C address of the camera. R11 and R12 on M0061
   - see M0061_i2c_selection_R11_R12.png
   - R12 is installed by default, so I2C ID is 0x1a (7bit) 0x34 (8bit)
   - Removing R12 and installing R11 results in I2C ID 0x10 (7bit) 0x20 (8bit) -- this will be referred to as IMX412(alt) : alternate i2c address
   - when updating i2c addres in camera_confi.xml, use 8-bit address : `<SensorSlaveAddress>0x20</SensorSlaveAddress>`
   - default camera_config.xml already looks for IMX412 with default i2c ID on J2 (CAM0) and alternate i2c ID on J3 (CAM1)
- only two non-raw streams are possible total (third camera would need to use RAW format)

### Supported Concurrent Output Formats and FPS
- all formats are supported in dual hires camera configuration
- h264/h265 encoder has a maximum throughput limitation
- concurrent dual RAW10
   - any raw resolution / FPS supported by each camera (no additional limitations)
- concurrent YUV
   - any processed resolution / FPS supported by each camera (no additional limitations)
- concurrent h264 / h265 :
   - 4K20 + 4K20
   - 4K30 + 1080p30
   - 1080p60 + 1080p60
   - 1080p90 + 1080p60


### I2C addressing
- camera_config.xml expect 8-bit I2C id
  - e.g. `<SensorSlaveAddress>0x20</SensorSlaveAddress>`
- dmesg will print debug statements using 7-bit I2C id
  - use dmesg to dump kernel messages..
```
# IMX412 on J2 (CAM0)
[    6.771738] msm_camera_cci_i2c_read: CDBG: READ dev addr 0x1a reg addr = 0x16
[    6.771949] msm_cci_i2c_read_bytes: Trying to read address 0x16
[    6.771954] msm_camera_cci_i2c_read: CDBG: SUCCESS data: 0x577
[    6.771958] msm_camera_cci_i2c_read addr = 0x16 data: 0x577
[    6.771964] imx412 probe succeeded
```

```
# IMX412 on J3 (CAM1) (alternate I2C address)
[    8.432678] msm_camera_cci_i2c_read: CDBG: READ dev addr 0x10 reg addr = 0x16
[    8.433288] msm_cci_i2c_read_bytes: Trying to read address 0x16
[    8.433301] msm_camera_cci_i2c_read: CDBG: SUCCESS data: 0x577
[    8.433314] msm_camera_cci_i2c_read addr = 0x16 data: 0x577
[    8.433328] imx412 probe succeeded
```

```
# IMX377 on J2 (CAM0)
[    8.149673] msm_camera_cci_i2c_read: CDBG: READ dev addr 0x1a reg addr = 0x3044
[    8.149948] msm_cci_i2c_read_bytes: Trying to read address 0x3044
[    8.149959] msm_camera_cci_i2c_read: CDBG: SUCCESS data: 0x32
[    8.149969] msm_camera_cci_i2c_read addr = 0x3044 data: 0x32
[    8.149979] imx377 probe succeeded
```

```
OV7251 Tracking on J4 (CAM2)
[    6.586116] msm_camera_cci_i2c_read: CDBG: READ dev addr 0x70 reg addr = 0x300a
[    6.586857] msm_cci_i2c_read_bytes: Trying to read address 0x300a
[    6.586880] msm_camera_cci_i2c_read: CDBG: SUCCESS data: 0x7750
[    6.586902] msm_camera_cci_i2c_read addr = 0x300a data: 0x7750
[    6.586922] ov7251 probe succeeded
```
- IC2 Addresses of ModalAi cameras:
  - IMX214        : 0x10 (7-bit)  0x20 (8-bit)
  - IMX230        : 0x10 (7-bit)  0x20 (8-bit)
  - IMX377        : 0x1a (7-bit)  0x34 (8-bit)
  - IMX412        : 0x1a (7-bit)  0x34 (8-bit)
  - IMX412 (alt)  : 0x10 (7-bit)  0x20 (8-bit)
  - ov7251        : 0x70 (7-bit)  0xE0 (8-bit)
  - ov7251 stereo : 0x60 (7-bit)  0xC0 (8-bit) (both cameras)
  - ov9782 stereo : 0x60 (7-bit)  0xC0 (8-bit) (both cameras)
  - ov9281        : 0x10 (7-bit)  0x20 (8-bit)
  - ov9282        : 0x10 (7-bit)  0x20 (8-bit)

### Possible Dual Hires Combinations
- Since IMX412 allows to change the I2C address in hardware (resistor), the following combinations are possible
   - IMX412 + IMX412 (alt)
   - IMX377 + IMX412 (alt)
   - IMX214 + IMX412
   - IMX377 + IMX214
   - it does not matter which camera slot they are plugged into : CAM0 (J2), CAM1 (J3), CAM2 (J4)
   - make sure camera_config.xml has an entry for each camera in correct slot : `<CameraId>` entry corresponds to camera slot 0,1, or 2
- default camera_config.xml (as of platform version 3.2.0) supports the following dual hires camera configurations:
   - IMX412: CAM0=J2, CAM1=J3, IMX412(alt) : CAM1=J3
   - IMX377: CAM0=J2, CAM1=J3
   - IMX214: CAM0=J2, CAM1=J3
- CAM2 slot (J4) can also be used for a hires camera, but camera_config.xml would need to be modified to reflect the camera slot 2

### Camera Start Order
- if camera with RAW stream is present, it needs to be started first (ov7251 raw or TOF)
- it does not matter which hires camera is started first
- it seems if one hires camera stops streaming (process stopped), the other camera process my also die as well

### Synchronization
- currently synchronization between dual hires camera setup is not implemented
- it is possible to do in hardware (primary hires camera will generate sync signal for secondary) or in software (adjust fps to align incoming SOF)
