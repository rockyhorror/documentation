---
layout: default
title: Unbrick VOXL and VOXL Flight
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 28
permalink: /unbrick-voxl/
youtubeId: 6_WzFYnpu40
---

# Unbricking Procedure for VOXL and VOXL Flight
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The System Image flashing process requires the device to be in `fastboot` mode.  The typical process uses `adb` to put the VOXL into `fastboot` mode over USB.  If the device is bricked and one cannot get the VOXL into `fastboot` using USB, then you'd follow this process before running the `install.sh` script as described in the [Flash System Image](/flash-system-image/) process.

The following VOXL add-on boards support the force fastboot mode switch:
* USB and Serial Debug Expander ([datasheet](https://docs.modalai.com/usb-expander-and-debug-manual))

### Video Tutorial
{% include youtubePlayer.html id=page.youtubeId %}

### Force VOXL into fastboot

To force the VOXL to boot into `fastboot` mode:

- Power off the VOXL
- Detach VOXL USB cable (to ensure USB is not supplying any power VOXL)
- Attach the USB Expander and Debug Add-On board
- Ensure the `FASTBOOT` labeled switch is set to **on**
- Ensure the `EMERG BOOT` labeled switch is set to **off**
  - NOTE: if your board is missing the `EMERG BOOT` switch, this is OK as its default un-populated state is OFF
- Power on the VOXL
- Reattach VOXL USB cable
- After VOXL boots up (about 5 seconds), set the `FASTBOOT` back to **off**

You can verify you are in `FASTBOOT` with the following command:

```bash
sudo fastboot devices
```

You should see an output like:

```bash
a0259b4a    fastboot
```

### Reflashing System Image after Forcing the VOXL into Fastboot

To reflash the System Image, run the following while in Fastboot mode:

```
$ sudo ./install.sh -f
``` 
 
Flashing the System Image using `install.sh` is [covered in this section](/flash-system-image/).

## Prevent Flash Corruption (i.e., Bricking)

With flash, like what is used on VOXL, removing power while the kernel is writing will result in corruption and bricking of the board. It can happen during development when a lot of changes are being made on the board and then power is removed. 

Use the /data partition for file storage to avoid this issue.

Linux has a setting for how often the flash is committed to reduce the risk, but of course there could be different side effects. You can see a writeup on the topic [here](https://superuser.com/questions/1057007/adjust-linux-disk-flush-intervals-to-avoid-blocking-user-processes), as it is common in all of this type of single board computers

Your smartphone fixes this by making you press the poweroff button before shutting off power.

To prevent flash data corruption, run poweroff before removing power: 

```
$ poweroff -f
```
