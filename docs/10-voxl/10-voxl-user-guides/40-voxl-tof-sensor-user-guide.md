---
layout: default
title: VOXL ToF Sensor User Guide
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 40
permalink: /voxl-tof-sensor-user-guide/
---

# VOXL ToF Sensor User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![VOXL ToF](/images/userguides/voxl-tof-sensor/voxl-tof.png)

## Requirements

- VOXL or VOXL-Flight with [VOXL System Image](/voxl-system-image/) 3.2.0 or newer
- VOXL ToF Sensor ([available here](https://www.modalai.com/products/voxl-dk-tof))
- If trying to visualize with ROS:
	- Linux with Rviz
	- [Setup ROS on Host PC](https://docs.modalai.com/setup-ros-on-host-pc/)
	- [Setup ROS on VOXL](https://docs.modalai.com/setup-ros-on-voxl/)

## Hardware

- Ensure VOXL/VOXL-Flight is powered off
- Connect to VOXL/VOXL-Flight connector `J3` as shown (or `J2` if using concurrently with stereo cameras)

![VOXL ToF Install](/images/userguides/voxl-tof-sensor/voxl-tof-install-0.png)

## Connect to VOXL

### Over ADB

``` bash
adb shell
```

### Over Wifi

- Setup and [connect](/wifi-setup/) VOXL/VOXL-Flight
- SSH onto the target:
	- VOXL IP will be 192.168.8.1 if in softap mode
	- If in station mode, you will have to run `ifconfig` on voxl to see what IP your network has assigned the voxl

```bash
ssh root@IP-ADDR-OF-VOXL
oelinux123
```

## Configure Cameras on VOXL

On VOXL:

- Ensure the voxl is set up by running the following commands on voxl:

```bash
voxl-configure-cameras
```

- Select the option that best fits your setup

- Restart the camera server
```bash
systemctl restart voxl-camera-server
```

## Viewing the Data with voxl-portal

Ensure that voxl-camera-server and voxl-portal are running with:

```bash
voxl-inspect-services
```

If either are not, start them with 
```bash
systemctl start voxl-camera-server
```
or
```bash
systemctl start voxl-portal
```

Once they're both running, open a web browser on a device connected to the same network as the drone, type in the drone's IP, click on the camera dropdown, and select one of the tof images to be shown a page like this:

![VOXL ToF Install](/images/userguides/voxl-tof-sensor/TOF-Sensor-Portal.png)

voxl-portal does not currently support viewing the 3d pointcloud, but this is in the works and should be available in the coming weeks!

## Viewing the Data with ROS

### Configure ROS on voxl

On VOXL:

```bash
# view network information
ifconfig
```

- This will give you the IP address of the VOXL, configure ROS by modifying the `/home/root/my_ros_env.sh` file:

```bash
vi /home/root/my_ros_env.sh
exec bash
```

### Configure ROS On Desktop

On Linux desktop:

- Find IP address

```bash
# view network information
ifconfig
```

- Configure ROS environment variables, where the ROS_MASTER_URI is what you saw on the VOXL

```bash
export ROS_IP=IP-ADDR-OF-PC
export ROS_MASTER_URI=http://AAA.BBB.CCC.DDD:XYZUV/
```

- source variables, where xxxxxx is the ROS version you have installed

```bash
source /opt/ros/xxxxx/setup.bash
```

### Launch Node on VOXL

```bash
roslaunch /opt/ros/indigo/share/voxl_mpa_to_ros/launch/voxl_mpa_to_ros.launch
```

### Launch RViz on Desktop

- On the host computer, run RViz

```bash
rviz
```

- On the leftmost column click on the "Add" button
- In the pop-up options click on "Image"
- Change Display Name to "IR-Image"
- In the left column under "IR-Image" tab select type in Image Topic as the TOF IR Image
- Click on the "Add" button again
- In the pop-up options click on "Image"
- Change Display Name to "Depth-Image"
- In the left column under "Depth-Image" tab select type in Image Topic as the TOF Depth Image
- "Add" PointCloud2 with TOF pointcloud topic

![ToF Visualized](/images/userguides/voxl-tof-sensor/voxl-tof-visualized.png)
