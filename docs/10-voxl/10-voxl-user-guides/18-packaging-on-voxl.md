---
layout: default
title: Software Packaging on VOXL
parent: VOXL User Guides
grand_parent: VOXL
nav_order: 18
permalink: /packaging-on-voxl/
---

# Software packaging on VOXL
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

VOXL runs a variant of Linux created by ModalAI using the Yocto build system (Jethro version). This
environment has a simple package management system known as [opkg](https://en.wikipedia.org/wiki/Opkg).
This is very useful for managing the software on VOXL but it does not have access
to an external package feed like other more full featured package managers (e.g. apt).
This means that the VOXL user must install packages and deal with all of the
dependencies without the direct support of the package manager.

## Creating packages

There are a couple of ways to create your own ipk packages.
1. When building software on target directly, in the voxl-emulator, or in a cross
compile environment the relevant artifacts can be archived into an ipk using one
of our example packaging scripts. The opkg format is fairly straightforward to
understand and creating your own utility to handle this task is not that much effort.
One example to help you get started is located in the ModalAI project
[voxl-ffmpeg](https://gitlab.com/voxl-public/voxl-ffmpeg).
Look at the make_package.sh bash shell script and the ipk subdirectory.
2. You can use Yocto to create a package. This is useful if a Yocto build recipe or layer
already exists for the desired package. You do not need the full Yocto environment that
ModalAI uses to create the system image. An example of this approach is in the [poky project](https://gitlab.com/voxl-public/poky).
 This is forked from the main Yocto Poky repository
and has VOXL specific documentation / configuration. This is a nice way to build a
package because it will also build all of the package dependencies.

## Docker

Running Ubuntu or other Linux distro in a Docker on target is a great way to get
access to a more full featured package management system such as apt. Documentation
on this approach is located [here](https://docs.modalai.com/docker-on-voxl/). Please
note that there are some limitations that are listed at the end of that document.
