---
layout: default
title: VOXL Quickstart
nav_order: 4
parent: VOXL
has_children: true
permalink: /voxl-quickstarts/
youtubeId: Kjw7X6WKDUw
---

# VOXL Quickstart
{: .no_toc }

This section contains simple topics geared towards helping you get up and running with your VOXL Core for the first time. They are in a logical order that guides you through tasks a normal developer would typically go through to get started.

## Quickstart Video

{% include youtubePlayer.html id=page.youtubeId %}

# VOXL Quickstart Requirements

Before you start, you'll need to gather up the following:

## VOXL-DK

The VOXL Development Kit is available for purchase from [here](https://www.modalai.com/development-kits/) and contains the following:

![voxl-dk](/images/quickstart/voxl/voxl-dk.jpg)

| Component | Part Number | Description |
|:--- |:--- |:--- |
| VOXL Core Board | MCCA-M0006  | Development board |
| [VOXL Power Module v3](/power-module-v3-datasheet/) | NA  | Provides regulated power to the board |
| Power Cable | MCBL-00001 | Connects development board to regulator board, also available [here](http://www.robotis.us/robot-cable-4p-140mm-10pcs/) |
| Wi-Fi Antennas (QTY2) | MANT-00001  | also availabe [here](https://www.digikey.com/product-detail/en/molex/1461531100/WM17824-ND/8543422) |

## Power Supply

A 5V/6A rated power supply is required:

| Component         | Part Number | Description                                                                                                                                                                                                                       |
|:------------------|:------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| VOXL Power Supply | MPS-00001-1 | Recommended: GlobTek, Inc. WR9QA6000LCP-N(R6B) with Q-NA(R) plug, [here](https://www.digikey.com/products/en?keywords=wr9qa6000lcp) and [here](https://www.digikey.com/product-detail/en/globtek-inc/Q-NA-R/1939-1106-ND/8597909) |

WARNING!
{: .label .label-red }

You **CANNOT USE** common 12v power supplies even though their connectors may fit the Power Module's barrel jack. The APM's barrel jack bypasses the regulator and can accept **5V ONLY**.


## USB cable and Computer

The following will be required to interact with the VOXL:

* USB 2.0 A-Male to Micro B Cable
* Host computer running Linux flavour of your choice


## Next Steps

Next, we'll look at [basic hardware setup](/voxl-hardware-setup/).

