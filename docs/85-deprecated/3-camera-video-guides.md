---
layout: default
title: Camera and Video Guides
parent: Deprecated
search_exclude: true
nav_order: 3
permalink: /camera-video-guides/
---

# Camera and Video Guides
{: .no_toc }

---

## This package is deprecated
{: .no_toc }

Please refer to [VOXL SDK](/voxl-sdk/) and [VOXL Camera Server](/voxl-camera-server/) for interfacing with cameras.

---

## Overview

This section contains instructions on how to use VOXL's camera, computer vision, and video features.

## Supported Cameras

The VOXL architecture (VOXL and VOXL Flight) support the following interfaces for cameras and image sensors

| Interface | Types of Cameras | Information |
| --- | --- | --- |
| MIPI-CSI2 <br> (Direct connection to VOXL/VOXL Flight PCB) | RGB rolling and global shutter image sensors such as Sony IMX377 and OV7251. Depth imagers such as PMD Time of Flight (TOF). The list of supported sensors includes: <br>&nbsp; Sony IMX412 <br>&nbsp;&nbsp;Sony IMX377 <br>&nbsp;&nbsp;Sony IMX214 <br>&nbsp;&nbsp;Sony IMX378 <br>&nbsp;&nbsp;Sony IMX296 <br>&nbsp;&nbsp;OV7251 <br>&nbsp;&nbsp;PMD TOF with Infineon IRS1645C  | [Datasheets](https://docs.modalai.com/voxl-image-sensor-datasheets/) |
| USB (UVC) | Webcams | [Instructions](https://docs.modalai.com/uvc-streaming/) | 
| USB (libgphoto2) | [Hundreds](http://www.gphoto.org/proj/libgphoto2/support.php) of DSLR and point-and-shoot Cameras with varying capabilities based on support | [Instructions](https://docs.modalai.com/voxl-libgphoto2/) |
| HDMI | Generally larger cameras with HDMI output | [Datasheet](https://docs.modalai.com/hdmi-input-accessory-datasheet/) <br> [Manual](https://docs.modalai.com/hdmi-input-accessory-manual/) |
| FLIR Thermal Imaging via USB | Lepton and Boson | Instructions Coming Soon |

### VOXL MIPI-CSI2 Supported configurations

The following table conveys the camera sensor configurations currently supported on VOXL directly through MIPI CSI-2. Additional cameras can be supported through UVC (Linux USB Camera). 

| Configuration | Sensor Interface 0 (CSI0) | Sensor Interface 1 (CSI1) | Sensor Interface 2 (CSI2) | 
| --- | --- | --- | --- |
| 1 | [High-res](https://docs.modalai.com/voxl-hires-camera-datasheet/) 4k RGB rolling shutter camera 30Hz (IMX214, IMX296, IMX377, or IMX412) | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) Synchronized  Stereo Sensor Pair B&W (OV7251) | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 2 | [High-res](https://docs.modalai.com/voxl-hires-camera-datasheet/) 4k RGB rolling shutter camera 30Hz (IMX214, IMX296, IMX377, or IMX412) | [ToF](https://docs.modalai.com/voxl-tof-sensor-datasheet/) Active Time of Flight Sensor | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 3 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) Synchronized  Stereo Sensor Pair B&W (OV7251) | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |
| 4 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) | [ToF](https://docs.modalai.com/voxl-tof-sensor-datasheet/) Active Time of Flight Sensor | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) VGA B&W global shutter up to 90Hz (OV7251) |

## Stream and Retrieve Video and Images

| Method | Information |
| --- | --- |
| ROS | [Setup](https://docs.modalai.com/setup-ros-on-voxl/) [voxl-cam-ros node](https://docs.modalai.com/voxl-cam-ros/) |
| RTSP | [UVC](https://docs.modalai.com/uvc-streaming/) or [MIPI](https://docs.modalai.com/voxl-rtsp/) |
| Image Streamer | [Instructions](https://docs.modalai.com/image-streamer/) |

