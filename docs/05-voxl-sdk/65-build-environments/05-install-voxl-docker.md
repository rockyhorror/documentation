---
layout: default
title: Install VOXL Docker
parent: Build Environments
grand_parent: VOXL SDK
nav_order: 05
permalink: /install-voxl-docker/
---

# Install VOXL Docker
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Develop for VOXL in a Docker running on your Linux PC! This tool lets you use the docker build environments described in the [build environments page](/build-environments/).


## Prerequisite: Install Docker CE

1) Official instructions are here:

https://docs.docker.com/install/linux/docker-ce/ubuntu/

However, they are summarized at time of writing as follows. we can't guarantee these commands are current whereas the above link should be current.

```bash
# install instructions from https://docs.docker.com/install/linux/docker-ce/ubuntu/
sudo apt remove docker docker-engine docker.io containerd runc
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository -y \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
```

2) Add current user to docker group to allow usage of docker commands without root privileges. You will need to restart your OS after running this command for the group permissions to be applied.

```bash
sudo usermod -a -G docker $USER
sudo reboot
```

## Install the voxl-docker script

The voxl-docker repository contains dockerfiles and helper scripts used to build almost all voxl projects.

```bash
mkdir -p ~/git
cd ~/git
git clone https://gitlab.com/voxl-public/voxl-docker
cd voxl-docker
./install-voxl-docker-script.sh
```


## Use of voxl-docker Script

```bash
$ voxl-docker -h

This is primarily used for running the voxl-emulator image for compiling ARM
apps-proc code for VOXL and the voxl-hexagon docker image for cross-compiling
hexagon SDSP programs. This can also launch any other installed docker image
with the -i argument.

By default this mounts the current working directory as the home directory
inside the docker for easy compilation of whichever project you are currently
working in. The directory that gets mounted inside the docker can be manually
specified with the -d argument.

The voxl-hexagon image starts with the username "user" with UID and GID 1000
which should match the first user on your desktop to avoid permissions issues.

The voxl-emulator image starts, by default, with the same username, UID, and GID
inside the docker as the user that launched it.

Since the voxl-emulator image is designed to emulate the userspace environment
that runs onboard the VOXL itself, you may wish to run as the root user inside
the voxl-emulator docker image to test certain behaviors as the root user.
This more closely mimics the on-target environment as the VOXL image runs as
root by default. Enter this mode with the -p option.

You can also specify the entrypoint for the docker image launch. By default this
is set to /bin/bash but can be user-configured with the -e option. This is most
likely used to pass the docker a command to execute before exiting automatically.
For example, to build the librc_math project in one command:

~/git/librc_math$ voxl-docker -i voxl-emulator -e "/bin/bash build.sh"


ARGUMENTS:
  -h:      : Print this help message
  -d <name>: The name of the directory to mount as ~/ inside the docker
  -i <name>: Docker image to run, usually voxl-emulator or voxl-hexagon
  -p       : for voxl-emulator image ONLY, runs as root user inside docker
  -l       : list installed docker images
  -e       : set the entrypoint for the docker image launch
```

```bash
$ voxl-docker -l
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
voxl-hexagon        latest              637d89d3a530        19 minutes ago      5.28GB
voxl-emulator       latest              0e15518b8f95        1  months ago       1.26GB
```


## TODO review this page and add note about voxl-docker on voxl itself