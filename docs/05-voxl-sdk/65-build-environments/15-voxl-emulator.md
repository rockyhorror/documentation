---
layout: default
title: VOXL Emulator Docker Image
parent: Build Environments
grand_parent: VOXL SDK
nav_order: 15
permalink: /voxl-emulator/
---

# VOXL Emulator Docker Image
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

Develop for VOXL in a Docker running on your Linux PC! This tool lets you use the docker build environments described in the [build environments page](/build-environments/).


## Prerequisite

Follow the instructions to install Docker CE and the voxl-docker script [here](https://developer.modalai.com/asset/download/).



## Install the voxl-emulator Docker Image

voxl-emulator enables one to develop CPU-based applications for VOXL and test in an emulated environment.

4) Download archived docker image from ModalAI Developer portal (login required). Currently it is on version 1.4 [voxl-emulator_v1.4.tgz](https://developer.modalai.com/asset/download/59) from [developer.modalai.com](https://developer.modalai.com)
