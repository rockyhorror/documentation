---
layout: default
title: Debug Tools
parent: VOXL Portal
grand_parent: VOXL SDK
nav_order: 10
has_children: false
permalink: /voxl-portal-debug-tools/
youtubeId:
---

# Debug Tools
{: .no_toc }

## Battery and GPS

Regardless of which page you are on, the top right corner of the browser will display your current battery percentage as well as how many satellites you have a connection with.<br>
![voxl-portal-gps-sat.png](/images/voxl-portal/voxl-portal-gps-sat.png)<br>
They will both display "N/A" if PX4 is not correctly configured for your GPS module or battery type.


## Plotting Tools

voxl-portal is able to produce detailed plots with CPU and IMU information.

### CPU
The CPU debug page contains the VOXL Cpu Plotter along with a live view of the running cpu stats. This tool will plot ten second windows of data, with various cpu, gpu, and memory options. You may choose to plot as few or as many of the listed options as you wish simply by selecting their individual buttons. Red options will be ignored, and green options will be plotted. Additional plot tools are listed in the top right corner of the plot, with a plot, pause, and clear button below.
![voxl-portal-cpu-plot.png](/images/voxl-portal/voxl-portal-cpu-plot.png)<br>

### IMU
The IMU debug page contains the VOXL Imu Plotter. This tool will plot ten second windows of data, with gyro and acceleration data options. You may choose to plot as few or as many of the listed options as you wish simply by selecting their individual buttons. Red options will be ignored, and green options will be plotted. Additional plot tools are listed in the top right corner of the plot, with a plot, pause, and clear button below.
![voxl-portal-imu-plot.png](/images/voxl-portal/voxl-portal-imu-plot.png)<br>
