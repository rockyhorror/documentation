---
layout: default
title: Viewing Cameras
parent: VOXL Portal
grand_parent: VOXL SDK
nav_order: 5
has_children: false
permalink: /voxl-portal-cameras/
---

# Viewing Cameras
{: .no_toc }

voxl-portal uses the MPA library to dynamically list and display any open camera pipe.
When the "Cameras" dropdown is selected, any available image output will be displayed, regardless of supported type.
![voxl-portal-cameras.png](/images/voxl-portal/voxl-portal-cameras.png)

Select the camera you wish to view, and the overlay will be displayed as long as the image format is supported.
![voxl-portal-qvio-overlay.png](/images/voxl-portal/voxl-portal-qvio-overlay.png)

Supported image types:
* RAW8
* STEREO_RAW8
* RGB
* YUV420
* YUV422
* NV21
* NV12
