---
layout: default
title: Mavlink Camera Manager
parent: SDK Utilities
grand_parent: VOXL SDK
nav_order: 70
permalink: /mavlink-camera-manager/
---

# Manage RTSP streams with Mavlink
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager) advertises camera names and RTSP stream URIs using the [Mavlink camera protocol](https://mavlink.io/en/services/camera.html) for use in [QGroundControl](http://qgroundcontrol.com/). This allows you to use the camera selector drop down menu in the camera controls to choose an RTSP stream by camera name. The camera RTSP streams must already be setup using, for example, [voxl-streamer](https://docs.modalai.com/voxl-streamer/)

## Prerequisites

An autopilot connection must already be established with QGroundControl otherwise
it will ignore any cameras attempting to connect.

## Running mavlink-camera-manager

mavlink-camera-manager is a command line application and can be started on the command line. Use the -h option to view online help:
```
yocto:/home/root# mavlink-camera-manager -h
Usage: mavlink-camera-manager <options>
Options:
-h     print this help message
-d     Enable debug messages
-g     GCS IP address
-r     RTSP URI
-i     camera id (default is 0)
-n     camera name (default is "Camera X" where X is camera id)
```
The GCS IP address and RTSP URI parameters are required.
Some examples:
```bash
mavlink-camera-manager -g 192.168.0.10 -r rtsp://192.168.0.9:8900/live -n hires
```
```bash
mavlink-camera-manager -g 192.168.0.10 -r rtsp://192.168.0.9:8901/live -i 1 -n stereo
```
```bash
mavlink-camera-manager -g 192.168.0.10 -r rtsp://192.168.0.9:8902/live -i 2 -n tracking
```
