---
layout: default
title: VOXL List Pipes
parent: SDK Utilities
grand_parent: VOXL SDK
nav_order: 03
permalink: /voxl-list-pipes/
---

# VOXL List Pipes
{: .no_toc }

## Overview
{: .no_toc }

```voxl-list-pipes``` is a Simple tool to list all of the MPA pipes currently available in the system. Running the command alone is identical to the command ```ls /run/mpa```, but when giving it the ```-t``` or ```--mode-types``` flag, it will sort the pipes by type, allowing you to easily see which cameras/imus/etc are available in the system.

---

## Example Output:
{: .no_toc }

```
yocto:/$ voxl-list-pipes -t
camera_image_metadata_t
    hires
    qvio_overlay
    tof_conf
    tof_depth
    tof_ir
    tof_noise
    tracking

cpu_stats_t
    cpu_monitor

imu_data_t
    imu0
    imu1

mavlink_message_t
    vvpx4_gps_raw_int
    vvpx4_mavlink_io
    vvpx4_sys_status
    vvpx4_vehicle_gps

point_cloud_metadata_t
    tof_pc

pose_vel_6dof_t
    vvpx4_body_wrt_fixed
    vvpx4_body_wrt_local

qvio_data_t
    qvio_extended

text
    vvpx4_shell

tof_data_t
    tof

vio_data_t
    qvio

yocto:/$
```

---

## Source:
{: .no_toc }

The source code for this can be found [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/bin/voxl-list-pipes)
