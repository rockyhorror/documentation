---
layout: default
title: VOXL-GPhoto2 (beta)
parent: SDK Utilities
grand_parent: VOXL SDK
nav_order: 68
permalink: /voxl-gphoto2/
---

# VOXL GPhoto2: How To Stream and Control Popular DSLR Cameras to your VOXL-based Drone
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)
is a Modal Pipe Architecture (MPA) (documentation link TBD) compatible application to stream camera video frames. This is the replacement for [voxl-libgphoto2](https://docs.modalai.com/voxl-libgphoto2/)

[gPhoto2](http://www.gphoto.org/) is a free, redistributable, ready to use set of digital camera software libraries and applications for Unix-like systems, supporting more than [2,500 cameras](http://www.gphoto.org/proj/libgphoto2/support.php). These cameras include popular options like Sony A6500, Panasonic DC-GH5, Nikon DSC, Nikon CoolPix, Canon PowerShot, and many more!

voxl-gphoto2 detects a supported camera on USB and starts streaming video
frames from it via MPA. These frames can then be streamed over RTSP using [voxl-streamer](https://docs.modalai.com/voxl-streamer/)

## Hardware Setup

In order to stream a video feed to QGroundControl, voxl-libgphoto2 requires the use of a [Flight Core](https://www.modalai.com/products/flight-core) or [VOXL Flight](https://www.modalai.com/4g-add-on).

The following additional hardware is required to stream video from a digital camera through VOXL to QGroundControl.

| Part Number | Description | Link |
| --- | --- | --- |
| MCBL-00009-1 | USB Cable (Host, 4-pin JST to USB 2.0 Type A Female) | [Purchase](https://www.modalai.com/products/usb-cable-4-pin-jst-to-usb-a-female)
| Camera USB Cable | USB Cable included with camera e.g. (USB-C, Mini USB, Micro USB) |

One of the following VOXL Add-on boards is required.

| Part Number | Description | Link |
| --- | --- | --- |
| VOXL-ACC-MH | VOXL Microhard Modem Add-on | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-microhard-modem-usb-hub)
| VOXL-ACC-HUB | VOXL USB Expansion Board with Fastboot and Emergency Boot | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot)
| VOXL-ACC-LTEH | VOXL Cellular LTE and USB hub add-on  | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-lte?variant=32449203601459)

- Before beginning setup, make sure the VOXL is disconnected from both it's power source and it's USB to host PC cable

- Attach the add-on board to the VOXL and make sure that the boot switch(es) are turned off or to the left

- Plug the jst side of the MCBL-00009-1 USB Cable into the add-on board

- Plug the camera's USB cable into both the camera and the USB female side of the MCBL-00009-1 USB cable

- Power the VOXL

![MH Standalone](/images/userguides/SDK/dslr-setup.jpg)

## Running voxl-gphoto2

voxl-gphoto2 is a command line application and can be started on the command line:
```bash
yocto:/# voxl-gphoto2 -h
Usage: voxl-gphoto2 <options>
Options:
-d                Show extra debug messages.
-v                Show extra frame level debug messages.
-g                Show lots of gphoto2 debug messages.
-h                Show help.
```

## ModalAI Tested Cameras

The following cameras were tested by ModalAI and successfully used to stream video to QGroundControl:

- Canon EOS 60D
- Canon EOS 700D
- Canon Powershot G7 X Mark III
