---
layout: default
title: VOXL Wifi
parent: SDK Utilities
grand_parent: VOXL SDK
nav_order: 78
permalink: /voxl-wifi/
---

# VOXL Wifi
{: .no_toc }

## Overview

```voxl-wifi``` is a tool to configure the wifi on-board on voxl. The voxl will need to reboot after running any wifi changes. The script comes with three main modes:

### SoftAP

SoftAP sets up the wifi so that the voxl acts as an access point that other devices can connect to. The format for this command is: ```voxl-wifi softap <ssid> [optional country code]```. The password in this mode is "1234567890". The optional country code defaults to United States.

### Station

Station mode connects voxl to an existing wifi network. The format for this command is: ```voxl-wifi station <ssid> [password] [optional country code]```. Like softap mode, the country code defaults to United States. the password should be surrounded by quotation marks if it contains special characters.

### Factory

Factory mode sets the voxl into softap with ssid:```VOXL-{MAC Address}``` and password:```1234567890```.
