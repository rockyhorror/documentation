---
layout: default
title: Build ROS Nodes for VOXL
parent: ROS
grand_parent: VOXL SDK
nav_order: 20
has_children: false
permalink: /build-ros-nodes-for-voxl/
---

# Build ROS Nodes for VOXL
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL ships with the [`voxl-utils`](https://gitlab.com/voxl-public/voxl-utils) and [`voxl-nodes`](https://gitlab.com/voxl-public/voxl-nodes) package pre-installed alongside ROS Indigo so you can start developing with ROS right away.

You can choose to compile ROS nodes either directly on-target or in the voxl-emulator docker. If you intend to generate an IPK package of our ROS node we suggest building in the emulator docker image and pushing the IPK to VOXL.

How to setup ROS on VOXL can be found [here](https://docs.modalai.com/docs/quickstarts/setup_ros/)

### Setup Notes

The ROS environment running on VOXL is sensitive to the shell environment on the host PC! For best results, SSH from an Ubuntu host machine using XTerm when calling `roslaunch` or starting `roscore`.

If you see an error similar to the following, there is likely an issue with the terminal emulation on the host PC. This error is frequently seen when SSH'ing from Windows or MAC, and even the default Ubuntu terminal.

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the GNOME Terminal, the default terminal emulator of Ubuntu. The solution is to use another terminal emulator such as xterm or Konsole instead of GNOME Terminal.

## Building on VOXL

1) Prerequisite: you will need to have [`ros-indigo-build-deps`](https://storage.googleapis.com/modalai_public/modal_packages/latest/ros-indigo-build-deps_1.0.0.ipk) installed to build ROS nodes directly on VOXL. This is not necessary when building in the voxl-emulator docker image.

2) ADB or ssh into VOXL and go to your VOXL's home directory. This assumes you have followed the previous setup pages in this manual to get ADB and/or ssh working.

```bash
james@silverstone:~$ adb shell
/ # bash
/# cd ~/
/home/root#
```

3) make a new catkin workspace and initialize it with catkin_make

```bash
/home/root# mkdir -p catkin_ws/src
/home/root# cd catkin_ws
/home/root/catkin_ws# catkin_make
```

4) Make a new node source directory in the src folder. Here we use the VOXL time-of-flight node as an example.

```bash
/home/root/catkin_ws# cd src
/home/root/catkin_ws/src# git clone https://gitlab.com/voxl-public/tof_cam_ros.git
/home/root/catkin_ws/src# ls
CMakeLists.txt  tof_cam_ros
/home/root/catkin_ws/src# cd ../
/home/root/catkin_ws#
```

5) Build!

```bash
/home/root/catkin_ws# catkin_make install
```

6) This installed the newly built ROS node to `~/catkin_ws/install` which should be part of your ROS_PACKAGE_PATH if you have `voxl-utils` installed and it is sourced by the `~/my_ros_env.sh` script. Check to confirm

```bash
/home/root/catkin_ws# exec bash
/home/root/catkin_ws# env | grep "ROS_PACKAGE_PATH"
ROS_PACKAGE_PATH=/home/root/catkin_ws/install/share:/home/root/catkin_ws/install/stacks:/opt/ros/indigo/share:/opt/ros/indigo/stacks
```

7) Now you can launch your new ROS node! **Note** the tof_cam_ros node will fail to start unless you have a TOF sensor installed on VOXL.

```bash
/home/root/catkin_ws#
/home/root/catkin_ws# cd ../
/home/root# rospack list | grep tof_cam_ros
tof_cam_ros /home/root/catkin_ws/install/share/tof_cam_ros

/home/root# roslaunch tof_cam_ros tof.launch
```

## Building in voxl-emulator Docker Image

You can build a ROS node in the voxl-emulator docker image following the same steps as you did for building on the VOXL itself. However, the process of getting the built package onto the VOXL itself will be different.

The `voxl-nodes` package and it's README file provide a great example of how to do this. We recommend copying this project and swapping out the nodes in `catkin_ws/src` for your own and working from there. It includes instructions on how to build and helper scripts for making a package and installing on VOXL.

<https://gitlab.com/voxl-public/voxl-nodes>
