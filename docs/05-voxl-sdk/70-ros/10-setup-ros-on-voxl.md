---
layout: default
title: Setup ROS on VOXL
parent: ROS
grand_parent: VOXL SDK
nav_order: 10
has_children: false
permalink: /setup-ros-on-voxl/
---

# Setup ROS on VOXL

*ModalAI Top Tip:* ROS is not required to use or develop with VOXL! If you don't like ROS or don't want to use ROS, then you are done with the VOXL quickstarts! You can look through the more in-depth [Developing with VOXL](/developing-with-voxl/) and [Flying with VOXL](/flying-with-voxl/) guides.

If you like ROS, let's proceed:

---

## Set up Environment Variables

VOXL ships with ROS Indigo and the `voxl-utils` package pre-installed. `voxl-utils` installs the script `my_ros_env.sh` to `/home/root/` which sources the normal `/opt/ros/indigo/setup.bash` script to configure the ROS environment variables. It also sets the IP address environment variables. Since everyone's VOXL will be on a different IP address, you will need to edit this script to match the network configuration for your desired ROS master/slave configuration which will be different depending on your use case.

We recommend having the ROS Master be on VOXL. The Master IP is already set up to `192.168.8.1` to match the Soft Access Point use case. If you have your VOXL on a WiFi network you will need to change this address to match your configuration.

```bash
# On VOXL, begin by switching to the bash shell (by default, ADB puts you in the sh shell)
/# bash
yocto:/# vi ~/my_ros_env.sh

...

# IP Environment variables must be set after setup scripts
# Set ROS_IP & ROS_MASTER_URI appropriately for your configuration
# 192.168.8.1 is default for the robot in soft access point mode
export ROS_MASTER_URI=http://localhost:11311/
export ROS_IP=192.168.8.1
unset ROS_HOSTNAME
```

You can refresh your environment variables after updating this script by running:

```bash
yocto:/# exec bash
```

And then check that they have been updated by using the `voxl-env` tool or just grep'ing the bash environment variables.

```bash
yocto:/# voxl-env show
ROS_MASTER_URI=http://localhost:11311/
ROS_IP=192.168.1.150
CAM_CONFIG_ID=1
HIRES_CAM_ID=-1
TRACKING_CAM_ID=0
STEREO_CAM_ID=1
TOF_CAM_ID=-1

yocto:/# env | grep "ROS"
ROS_ROOT=/opt/ros/indigo/share/ros
ROS_PACKAGE_PATH=/home/root/catkin_ws/install/share:/home/root/catkin_ws/install/stacks:/opt/ros/indigo/share:/opt/ros/indigo/stacks
ROS_MASTER_URI=http://localhost:11311/
ROSLISP_PACKAGE_DIRECTORIES=
ROS_DISTRO=indigo
ROS_IP=192.168.8.1
ROS_ETC_DIR=/opt/ros/indigo/etc/ros
```

Both of these methods require you to run inside a bash shell, NOT sh. If you logged in over ADB you are put in an sh shell by default and need to start bash manually.



## Testing ROS Configuration

To test that your ROS configuration is working, start roscore on VOXL. Double check you are in a bash prompt and not sh before doing so.

ModalAI Top Tip: Currently an [issue](https://gitlab.com/voxl-public/documentation/issues/4) can be seen when attempting to use ROS using the native Ubuntu terminal. As a work around XTerm can be used.

```bash
# ON VOXL
yocto:/# echo $SHELL
/bin/bash
yocto:/# roscore

process[rosout-1]: started with pid [5706]
started core service [/rosout]

```

Now on your host PC use rostopic to see if it can communicate with VOXL.

```bash
# ON HOST PC
me@mylaptop:~$  rostopic list
/rosout
/rosout_agg
```

## Known Issue Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the GNOME Terminal, the default terminal emulator of Ubuntu. The solution is to use another terminal emulator such as xterm or Konsole instead of GNOME Terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4


## Next Steps

Now explore the [VOXL MPA to ROS](/voxl-mpa-to-ros/) package.

See the [building ROS nodes for VOXL](/build-ros-nodes-for-voxl/) for information on making your own ROS nodes.

