---
layout: default
title: Configure OPKG
parent: Software Configuration
grand_parent: VOXL SDK
nav_order: 3
permalink: /configure-opkg/
---

# Configure OPKG

VOXL software is distributed as IPK packages managed by the OPKG package manager very similarly to .deb packages in Debian/Ubuntu.


## Available Repositories

All modalai package repositories are available at [http://voxl-packages.modalai.com/](http://voxl-packages.modalai.com/) allowing opkg to install and update packages using an internet connection.

### stable

This is the default-enabled repository and consists of the latest stable packages. This is what we recommend you use, no further configuration is necessary as VOXL comes with the stable repository enabled out of the box.

### dev

This is the development repository containing untested experimental software that is automatically pushed via CI on every commit made by ModalAI's software team. This repository mostly serves for ModalAI to use internally, but is also useful for customers who are testing new features before they are considered stable.

### customer

This repository contains customer-specific packages that are not part of the standard [voxl-suite](/voxl-suite/). Packages here are not intended for general public use, but are still publically available with permission from the customer for ease of distribution.



## Configuring

### voxl-configure-opkg (recommended)

Run voxl-configure-opkg to bring up the wizard

```
yocto:/$ voxl-configure-opkg

Which repository do you want to use?
stable or development?

1) stable
2) dev
#? 1

Done configuring opkg
run opkg update to pull new repository manifest
```

Or provide "dev" or "stable" as arguments

```
yocto:/$ voxl-configure-opkg stable

Done configuring opkg
run opkg update to pull new repository manifest
```


Then tell OPKG to download the new package manifest, this is the equivalent of `apt update` in Debian/Ubuntu.

```
yocto:/$ opkg update
Downloading http://voxl-packages.modalai.com/stable/Packages.gz.
Updated source 'stable'.
```




### modify opkg.conf manually

To enable/disable repositories on VOXL edit the end of the /etc/opkg/opkg.conf file.


```
yocto:/# vi /etc/opkg/opkg.conf
```

```
.
.
.
src/gz stable http://voxl-packages.modalai.com/stable
## uncomment line below to enable development repository
#src/gz dev http://voxl-packages.modalai.com/dev
```

Then tell OPKG to download the new package manifest, this is the equivalent of `apt update` in Debian/Ubuntu.

```
yocto:/# opkg update
Downloading http://voxl-packages.modalai.com/stable/Packages.gz.
Updated source 'stable'.
yocto:/#
```
