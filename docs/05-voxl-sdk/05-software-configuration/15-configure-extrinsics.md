---
layout: default
title: Configure Extrinsics
parent: Software Configuration
grand_parent: VOXL SDK
nav_order: 15
permalink: /configure-extrinsics/
---

# Configure Extrinsics

`/etc/modalai/extrinsics.conf` is a json file that describes the intrinsics between the coordinate frames of critical components of the drone body. This file can be read using the library `libvoxl_common_config.so` and the `voxl-configure-extrinsics` tools, both of which are provided by the [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools) package.

This document serves to describe how to use and configure this file if needed for a custom application. The relation between camera, imu, and drone body (center of mass) is critical for proper VIO function.

An overview of the coordinate frames used in the extrinsics config file and throughout VOXL-SDK:

![geometry_overview](/images/voxl-vision-px4/px4_chart-geometry_overview.png)

## Preset Configurations with voxl-configure-extrinsics

The voxl-configure-extrinsics utility will allow you to use preset extrinsics config files or select your own file.

```
yocto:/$ voxl-configure-extrinsics -h

Start wizard with prompts:
voxl-configure-extrinsics

Shortcut configuration arguments for scripted setup.
leave_alone doesn't touch the file, it does nothing
other factory options wipe /etc/modalai/extrinsics.conf

factory_custom allows you to specify your own custom file which
will then be copied into /etc/modalai/extrinsics.conf

voxl-configure-extrinsics leave_alone
voxl-configure-extrinsics factory_m500_flight_deck
voxl-configure-extrinsics factory_seeker_v1_voxlcam
voxl-configure-extrinsics factory_starling_v1
voxl-configure-extrinsics custom /xx/yy/my_custom_file.conf

show this help message:
voxl-configure-extrinsics help
```

Running it without arguments will prompt you with a question on how to proceed as follows:

```
yocto:/$ voxl-configure-extrinsics
Starting Wizard
which preset extrinsics file would you like to use?
1) leave_alone        3) seeker_v1_voxlcam  5) custom
2) m500_flight_deck   4) starling_v1
#? 2
wiping old extrinsics config file
copying /usr/share/modalai/extrinsic_configs/m500_flight_deck.conf to /etc/modalai/extrinsics.conf
loading and updating file with voxl-inspect-extrinsics -q
done configuring extrinsics
```


## File Structure

This configuration file serves to describe the static relations (translation and rotation) between sensors and bodies on a drone. Mostly importantly it configures the camera-IMU extrinsic relation for use by VIO. However, the user may expand this file to store many more relations if they wish. By consolidating these relations in one file, multiple processes that need this data can all be configured by this one configuration file. Also, copies of this file may be saved which describe particular drone platforms. The defaults describe the VOXL M500 drone reference platform.

The file is constructed as an array of multiple extrinsic entries, each describing the relation from one parent to one child. Nothing stops you from having duplicates but this is not advised.

The rotation is stored in the file as a Tait-Bryan rotation sequence in intrinsic XYZ order in units of degrees. This corresponds to the parent rolling about its X axis, followed by pitching about its new Y axis, and finally yawing around its new Z axis to end up aligned with the child coordinate frame.

The helper read function will read out and populate the associated data struct in both Tait-Bryan and rotation matrix format so the calling process can use either. Helper functions are provided to convert back and forth between the two rotation formats.

Note that we elect to use the intrinsic XYZ rotation in units of degrees for ease of use when doing camera-IMU extrinsic relations in the field. This is not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by the rc_math library. However, since the camera Z axis points out the lens, it is helpful for the last step in the rotation sequence to rotate the camera about its lens after first rotating the IMU's coordinate frame to point in the right direction by Roll and Pitch.

The following online rotation calculator is useful for experimenting with rotation sequences: [https://www.andre-gaschler.com/rotationconverter/](https://www.andre-gaschler.com/rotationconverter/)

The Translation vector should represent the center of the child coordinate frame with respect to the parent coordinate frame in units of meters.

The parent and child name strings should not be longer than 63 characters.

The relation from Body to Ground is a special case where only the Z value is read by voxl-vision-px4 and voxl-qvio-server so that these services know the height of the drone's center of mass (and tracking camera) above the ground when the drone is sitting on its landing gear ready for takeoff.


## Example

Default file for the VOXL M500 and Flight Deck:

```
{
	"name":	"M500_flight_deck",
	"extrinsics":	[{
			"parent":	"imu1",
			"child":	"imu0",
			"T_child_wrt_parent":	[-0.0484, 0.037, 0.002],
			"RPY_parent_to_child":	[0, 0, 0]
		}, {
			"parent":	"imu0",
			"child":	"tracking",
			"T_child_wrt_parent":	[0.065, -0.014, 0.013],
			"RPY_parent_to_child":	[0, 45, 90]
		}, {
			"parent":	"imu1",
			"child":	"tracking",
			"T_child_wrt_parent":	[0.017, 0.015, 0.013],
			"RPY_parent_to_child":	[0, 45, 90]
		}, {
			"parent":	"body",
			"child":	"imu0",
			"T_child_wrt_parent":	[0.02, 0.014, -0.008],
			"RPY_parent_to_child":	[0, 0, 0]
		}, {
			"parent":	"body",
			"child":	"imu1",
			"T_child_wrt_parent":	[0.068, -0.015, -0.008],
			"RPY_parent_to_child":	[0, 0, 0]
		}, {
			"parent":	"body",
			"child":	"stereo_l",
			"T_child_wrt_parent":	[0.1, -0.04, 0],
			"RPY_parent_to_child":	[0, 90, 90]
		}, {
			"parent":	"body",
			"child":	"tof",
			"T_child_wrt_parent":	[0.1, 0, 0],
			"RPY_parent_to_child":	[0, 90, 90]
		}, {
			"parent":	"body",
			"child":	"ground",
			"T_child_wrt_parent":	[0, 0, 0.1],
			"RPY_parent_to_child":	[0, 0, 0]
		}]
}
```

Other default files can be set up with `voxl-configure-extrinsics` and can be viewed [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/tree/master/extrinsic_configs)


## IMU locations on VOXL

On VOXL, there are two IMUs: IMU1 (primary) and IMU0 (secondary). Their locations and physical orientation on the VOXL PCB are as follows:

![voxl-core-imu-locations](/images/datasheet/voxl/voxl-core-imu-locations-small.png)

NOTE that voxl-imu-server rotates the IMU data into a common and more useful orientation such that both IMU's appear to be oriented in FRD frame for the VOXL M500, flight deck, and Starling reference platforms. In the top image of the diagram above, this corresponds to X pointing Left, Y pointing to the right, and Z pointing out of the diagram. This conveniently allows the rotation matrix between IMU and body frame to be identity for most use cases.

voxl-imu-server can be configured to not rotate the imu data into this common frame, but this is not recommended as this results in both IMUs presenting data in different orientations, neither of which match the configuration in the standard extrinsics config files.

TODO update image.


### Camera Coordinate Frame

VOXL-SDK uses the convention of X to the right, Y down, and Z out the front of the lens.

![Camera Coordinate Frame](/images/userguides/voxl/camera_frame.png)


## Stereo Camera Extrinsics

Note that the stereo cameras go through a precise calibration procedure to determine the orientation between them, so only the left stereo camera is provided in the extrinsics config file. The right camera has a specified relation to the left camera in `/data/modalai/opencv_stereo_extrinsics.conf` which is created by the [stereo calibration](/calibrate-cameras/) process.


