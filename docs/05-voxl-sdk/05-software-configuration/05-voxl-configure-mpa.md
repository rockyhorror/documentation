---
layout: default
title: voxl-configure-mpa
parent: Software Configuration
grand_parent: VOXL SDK
nav_order: 5
permalink: /voxl-configure-mpa/
---

# voxl-configure-mpa

## voxl-configure-xxx scripts

MPA services all have an associated voxl-configure-xxx script to assist in enabling, disabling, and resetting service configuration files back to default. Each of these scripts will run as a wizard that asks questions if no arguments are given. For example voxl-imu-server:

```
yocto:/# voxl-configure-imu
Starting Wizard

Do you want to reset the config file to factory defaults?
1) yes
2) no
#? 1
wiping old config file
Creating new config file: /etc/modalai/voxl-imu-server.conf

Do you want to enable the voxl-imu-server
This is required for visual inertial odometry.
1) yes
2) no
#? 1

enabling  voxl-imu-server systemd service
starting  voxl-imu-server systemd service
Done configuring voxl-imu-server
yocto:/#
```

OR, you can tab-out arguments from the command line as a shortcut

```
yocto:/$ voxl-configure-imu {PRESS TAB}
disable         enable          factory_enable  help
```

The factory-enable option will reset voxl-imu-server.conf back to factory defaults, then enable the systemd service.



## voxl-configure-mpa

voxl-configure-mpa is a top-level config script for configuring all of the individual voxl-configure-xxx scripts at once.

```
yocto:/# voxl-configure-mpa

-------------Options for End Users-------------

        Welcome to voxl-configure-mpa!
  This tool is here to help you configure the
   multitude of MPA (Modal Pipe Architecture)
           services in voxl-suite.

      As an end-user you have 3 options

  Start the configuration wizard that asks
  questions (recommended)
  voxl-configure-mpa --wizard

  Perform a reset back to how MPA services were
  configured at the factory
  voxl-configure-mpa --factory-reset

  Show this help message:
  voxl-configure-mpa --help

-----------------------------------------------

yocto:/#
```

### Wizard Mode

Wizard mode will run you through each of the voxl-configure-xxx wizards, including [voxl-configure-cameras](/configure-cameras/).


### Factory Reset Mode

VOXL Boards that were shipped as part of a kit ([M500](/m500/), [Seeker](/seeker/), [VOXL CAM](/voxl-cam/), etc) were configured at the factory with kit-specific MPA configuration starting around June 2021 with the introduction of MPA. In these cases, the kit part number is saved to `/data/modalai/factory_mode.txt` and can be used to put your VOXL MPA services back to their kit-specific factory defaults by running:

```
voxl-configure-mpa --factory-reset
```

Note this file will be wiped along with factory calibration files if you elect to wipe your /data/ partition while flashing a system image (which is not a recommended procedure).

