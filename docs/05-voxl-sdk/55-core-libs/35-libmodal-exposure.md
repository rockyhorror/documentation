---
layout: default
title: libmodal_exposure
parent: Core Libs
grand_parent: VOXL SDK
nav_order: 35
permalink: /libmodal-exposure/
---

# libmodal-exposure
{: .no_toc }

libmodal_exposure is a beta project that applies an opencv histogram to an image (with metadata) and returns what the new exposure/gain should be.

Additional usage details are coming soon, in the meantime you can find the code and README on [Gitlab](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)



