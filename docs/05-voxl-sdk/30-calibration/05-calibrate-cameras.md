---
layout: default
title: Calibrate Cameras
parent: Calibration
grand_parent: VOXL SDK
nav_order: 05
permalink: /calibrate-cameras/
---

# Calibrate Cameras
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[VOXL Camera Calibration](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-calibration) is a package in voxl-suite >= 0.5.0 for calibrating cameras on-board on voxl. If you are on an older voxl-suite image, we recommend that you upgrade and use this tool, but you can find a link to the deprecated ROS calibration process below.

Use of this tool requires the ability to view the overlay image coming off of voxl. We recommend using the [VOXL Web Portal](/voxl-portal/) to view the overlay images.

### Old Calibration Process
{: .no_toc }

If you are heart-set on using ROS to calibrate your cameras, you can find the info for that deprecated process [here](/ros-calibrate-cameras/)

---

## Command Line Options

| Short Option | Long Option             | Effect                                                                            |
|:-------------|:------------------------|:----------------------------------------------------------------------------------|
| ```-e```     | ```--extrinsics-only``` | For a stereo pair, pull an existing intrinsics file and only run extrinsics       |
| ```-f```     | ```--fisheye        ``` | Tells opencv to use fisheye parameters for finding the board and calibrating      |
| ```-h```     | ```--help           ``` | Prints this help information                                                      |
| ```-l```     | ```--length         ``` | Specify the edge length of a single square in meters (default 0.038)              |
| ```-m```     | ```--mirror         ``` | Mirror the overlay output, this is just personal preference                       |
| ```-s```     | ```--size           ``` | Specify the number of corners for the calibrator to look for (default 6x8)        |
| ```-t```     | ```--threshold      ``` | Display the thresholded image in the overlay (debug tool for lighting conditions) |

A standard command will look something like:
```
voxl-calibrate-camera tracking -f -s 6x8 -l 0.038
```

or

```
voxl-calibrate-camera stereo -s 6x8 -l 0.038
```

---

## Monocular Calibration Process

To calibrate a monocular camera, such as the tracking camera on voxl, you can simply run the command: ```voxl-calibrate-camera tracking --fisheye```, open the web portal, and fill the rectangles like such:

![Tracking Cal](/images/camera-calibration/tracking-cal.png)

Once you've filled all the rectangles, the calibration will run, which will take a few seconds, and then the calibration will be printed to the terminal window. If the calibration was successful, it will also write to ```/data/modalai/opencv_{camera-name}_intrinsics.yml```

```
yocto:/$ voxl-calibrate-camera tracking -f
Matrix
[277.7090664510777, 0, 290.6865358454301;
 0, 277.5687125242331, 240.1597738541476;
 0, 0, 1]
Distortion
[-0.006319990900085937;
 0.00155893534296691;
 0;
 0]
distortion_model: fisheye
Re-projection error reported by calibrateCamera: 0.172684
Calibration Succeded!

Writing data to: /data/modalai/opencv_tracking_intrinsics.yml
Saved!
Exiting Cleanly
yocto:/$
```

### NOTE:
{: .no_toc }

When calibrating the standard OV7251 Tracking Camera, make sure to use the `-f` or `--fisheye` flag to make sure that the calibrator uses the fisheye lens distortion model.

---

## Stereo Calibration Process

To calibrate a stereo camera, you can simply run the command: `voxl-calibrate-camera stereo`, open the web portal, and fill the rectangles like such:


![Stereo Cal](/images/camera-calibration/stereo-cal-intrinsics.png)

The stereo calibrator calculates the intrinsics for each camera separately, so you will need to fill in all of the left rectangles, then all of the right rectangles. Once the intrinsics are done, you will have to manually add the extrinsic samples. To do so, hit enter in the terminal window that is running the calibrator to take a frame, once you have enough frames, type ```stop```. These images do not have pre-specified locations since different orientations lead to different requirements. It is STRONGLY recommended that you do 2 images for the extrinsics, one skewed image on either side of the frame, like such:


![Stereo Cal Left](/images/camera-calibration/stereo-cal-left.png)
![Stereo Cal Right](/images/camera-calibration/stereo-cal-right.png)

If everything has been done properly, you will see output like this:

```
yocto:/$ voxl-calibrate-camera stereo
Sampling overlapping images now
Enter an empty line to take a sample or "stop" to finish sampling

Taking Frame

Taking Frame
stop
Stopping sampling
Calibrating Left Camera
Matrix
[493.7322756669245, 0, 306.7896643027532;
 0, 493.1380517350153, 217.4680594645219;
 0, 0, 1]
Distortion
[-0.1698366125016952;
 0.06988854948207439;
 -0.001138684086637882;
 -0.0007932118450081079;
 0]
distortion_model: plumb_bob
Re-projection error reported by calibrateCamera: 0.177678
Calibration Succeded!
Calibrating Right Camera
Matrix
[496.1483207581481, 0, 320.9542358074322;
 0, 495.5756728109903, 223.7654477880258;
 0, 0, 1]
Distortion
[-0.1725992238299667;
 0.06664783196280108;
 -0.00172072316840736;
 -0.0004606934467252117;
 0]
distortion_model: plumb_bob
Re-projection error reported by calibrateCamera: 0.179598
Calibration Succeded!
Calibrating Extrinsics
2 frames will be processed
R
[0.999777546053251, -0.0004080345076243199, 0.02108771954432934;
 0.002055883177889161, 0.9969371281114418, -0.07818015053240687;
 -0.02099123036171473, 0.07820611293723473, 0.9967161943839129]
T
[-0.08011754799309605;
 -0.001690038045698758;
 -0.0008435695774040179]
Re-projection error reported by stereoCalibrate: 0.259720
Extrinsics Calibration Succeded!

Saved intrinsics to: /data/modalai/opencv_stereo_intrinsics.yml
Saved extrinsics to: /data/modalai/opencv_stereo_extrinsics.yml
Exiting Cleanly
yocto:/$
```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-calibration).


