---
layout: default
title: Inspect Tools
parent: VOXL SDK
nav_order: 15
has_children: true
has_toc: true
permalink: /inspect-tools/
---

# Inspect Tools

Most pipes in Modal Pipe Architecture have some sort of `voxl-inspect-xxx` tool to allow easy debug and visualization of data.

Most of these tools are part of the [voxl-mpa-tools project on GitLab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/).
