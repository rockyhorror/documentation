---
layout: default
title: VOXL Wait for FS
parent: MPA Services
grand_parent: VOXL SDK
nav_order: 40
permalink: /voxl-wait-for-fs/
---

# VOXL Wait for FS
{: .no_toc }

voxl-wait-for-fs is a simple bash script with an accompanying systemd service of the same name that makes tweaks to the file system write parameters and then waits for the /data/ partition to be mounted before exiting.


## Sync Rate Tweaks

The Linux kernel typically wakes up to sync data to disk every 5 seconds, and will only sync data that is older than 30 seconds at that time. This is fine for desktop computers which are on for long periods and are shutdown properly every time. However, in an embedded system that is often shutdown by pulling the power plug this can easily result in a corrupt disk.

We therefore speed up the sync period to twice per second to reduce the chance of a corrupted file system. You should also be sure only to write data to the /data/ partition on VOXL since corrupting the /data/ partition will still leave the system in a bootable state.


## Wait for partitions to be mounted

The /data/ partition is the last partition to be mounted and it's possible it is not mounted before your own systemd services start on boot which is problematic if your program needs to read/write to /data/.

To make use of this service, make your other systemd service files reference the voxl-wait-for-fs.service like this:

```
[Unit]
After=voxl-wait-for-fs.service
Requires=voxl-wait-for-fs.service
```

For example, voxl-qvio-server needs to read the tracking camera lens calibration file from /data/modalai/ so we ensure that service starts after the /data/ partition is mounted in its service file [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server/-/blob/dev/service/voxl-qvio-server.service).




## Source

Source code for the script is [here](https://gitlab.com/voxl-public/utilities/voxl-utils/-/blob/master/bin/voxl-wait-for-fs).
Source code for the service is [here](https://gitlab.com/voxl-public/utilities/voxl-utils/-/blob/master/service/voxl-wait-for-fs.service).

