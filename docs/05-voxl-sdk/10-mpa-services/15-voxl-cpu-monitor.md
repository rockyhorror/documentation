---
layout: default
title: VOXL CPU Monitor
parent: MPA Services
grand_parent: VOXL SDK
nav_order: 15
permalink: /voxl-cpu-monitor/
---

# VOXL CPU Monitor
{: .no_toc }


voxl-cpu-monitor checks the current state of the CPU, GPU, and memory including statistics such as clock speeds, usage, and temperatures. This is the "Resource Monitor" for MPA. It publishes data once per second to the `cpu_monitor` pipe for other processes to read.

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## voxl-inspect-cpu

The primary client for this pipe is [voxl-inspect-cpu](/voxl-inspect-cpu/). See its [source code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor/-/blob/master/clients/voxl-inspect-cpu.c) for a good example of using this data.


## Data Type

### cpu_stats_t

voxl-cpu-monitor publishes a cpu_stats_t struct to the pipe  as defined in cpu_monitor_interface.h. Note, this struct will become standardized and included in modal_pipe_interfaces.h at some point. For now it's in the voxl-cpu-monitor package.

```
typedef struct cpu_stats_t{
	uint32_t magic_number;		///< unique 32-bit number used to signal the beginning of a packet
	int32_t  num_cpu;			///< number of CPU's
	float    cpu_freq[MAX_CPU];	///< CPU freq (MHz)
	float    cpu_t[MAX_CPU];	///< CPU temperature (C)
	float    cpu_t_max;			///< max core temp (C)
	float    cpu_load[MAX_CPU];	///< CPU load (%)
	float    cpu_load_10s;		///< CPU load for past 10 seconds (%)
	float    total_cpu_load;	///< calculate total cpu load (%)
	float    gpu_freq;			///< GPU freq (MHz)
	float    gpu_t;				///< GPU temperature (C)
	float    gpu_load;			///< current gpu load (%)
	float    gpu_load_10s;		///< gpu load for past 10 seconds (%)
	float    mem_t;				///< Memory Temperature
	uint32_t mem_total_mb;		///< total memory in MB
	uint32_t mem_use_mb;		///< used memory in MB
	uint32_t flags;				///< flags
	uint32_t reserved;			///< spare reserved bytes for future expansion
}__attribute__((packed)) cpu_stats_t;
```

### flags

Several flags can be set in the cpu_stats_t struct to indicate high-level conditions. Most importantly are the flags for CPU_OVERLOAD and CPU_OVERHEAT. The intention is to allow CPU-intensive processes to monitor these flags and pro-actively lower their workload to free up CPU for more flight-critical processes such as VIO.

The CPU will automatically throttle its clock speed back when it gets too hot to protect itself, but it may be advantageous to avoid that scenario all together if possible to keep the CPU at full speed for the benefit of other running processes.

```
#define CPU_STATS_FLAG_CPU_MODE_AUTO	(1<<0)	// indicates CPU clock scaling is in Auto mode
#define CPU_STATS_FLAG_CPU_MODE_PERF	(1<<1)	// indicates CPU clock scaling is in performance mode
#define CPU_STATS_FLAG_GPU_MODE_AUTO	(1<<2)	// indicates GPU clock scaling is in Auto mode
#define CPU_STATS_FLAG_GPU_MODE_PERF	(1<<3)	// indicates GPU clock scaling is in performance mode
#define CPU_STATS_FLAG_CPU_OVERLOAD		(1<<4)	// indicates the total CPU usage is over %80 use
#define CPU_STATS_FLAG_CPU_OVERHEAT		(1<<5)	// indicates the max CPU core temp is over 90C
```


## Configuration

voxl-cpu-monitor has no configuration file, but in keeping with MPA standards you can use `voxl-configure-cpu-monitor` to enable/disable the service. It is enabled by default from the factory and when running voxl-configure-mpa


## Source

The code is available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor)

