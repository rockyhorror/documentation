---
layout: default
title: VOXL TFLite Server
parent: MPA Services
grand_parent: VOXL SDK
nav_order: 35
permalink: /voxl-tflite-server/
youtubeId: a0nCIrRbx_o
---

# VOXL-TFLite-Server
{: .no_toc }

The voxl-tflite-server is a GPU-optimized TensorFlow Lite environment that can run as a a background systemd service. It processes standard TensorFlow Lite models in real-time using the GPU on VOXL.

![voxl-portal-tflite](/images/voxl-portal/voxl-portal-tflite.png)

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## voxl-configure-tflite

Like all MPA services, voxl-tflite-server has a basic configure script to enable/disable the service as well as to reset the config file back to default for factory resets. This specific configuration script also allows you to select a default input pipe: tracking(grayscale) or hires(color).

```
yocto:/$ voxl-configure-tflite -h

Start wizard with prompts:
voxl-configure-tflite-server

Shortcut configuration arguments for scripted setup.
factory_enable will reset the config file to factory defaults
before enabling the service.

voxl-configure-tflite-server factory_disable
voxl-configure-tflite-server disable
voxl-configure-tflite-server factory_enable
voxl-configure-tflite-server enable

show this help message:
voxl-configure-tflite-server help
```

```
yocto:/$ voxl-configure-tflite
Starting Wizard

Do you want to reset the config file to factory defaults?
1) yes
2) no
#? 1
wiping old config file
Creating new config file: /etc/modalai/voxl-tflite-server.conf
The config file was modified during parsing, saving the changes to disk

do you want to enable or disable voxl-tflite-server
1) enable
2) disable
#? 1

do you want to run the tflite server with:
 (1) Mobilenet + Hires camera
 (2) Mobilenet + Tracking camera

1) 1
2) 2
#? 1
enabling  voxl-tflite-server systemd service
Created symlink from /etc/systemd/system/multi-user.target.wants/voxl-tflite-server.service to /etc/systemd/system/voxl-tflite-server.service.
starting  voxl-tflite-server systemd service
Done configuring voxl-tflite-server
```


## TFlite Server Configuration File

The default configuration file has recommended settings for a background service. If the output rate is too slow, try decreasing the skip_n_frames parameter.


```
yocto:/$ cat /etc/modalai/voxl-tflite-server.conf
/**
 * This file contains configuration that's specific to voxl-tflite-server.
 *
 * skip_n_frames -   how many frames to skip between processed frames. For 30hz
 *                   input frame rate, we recommend skipping 5 frame resulting
 *                   in 5hz model output.
 * model         -   which model to use. Currently only support mobilenet for
 *                   object detection.
 * input_pipe    -   which camera to use (tracking or hires).
 */
{
	"skip_n_frames":	5,
	"model":	"/usr/bin/dnn/ssdlite_mobilenet_v2_coco.tflite",
	"input_pipe":	"/run/mpa/hires/"
}

```


## Visualization Options

You can view either the MobileNet output stream as another normal camera pipe. As such, it can be viewed with [voxl-portal](voxl-portal), converted to ROS with [voxl_mpa_to_ros](/voxl-mpa-to-ros/), and logged to disk with [voxl-logger](/voxl-logger/).

See the image at the top of this page for an example.

## Demo

{% include youtubePlayer.html id=page.youtubeId %}


## Using Your Own Models

This server can run both CPU and GPU accelerated models but in order to utilize the GPU, models must be float16 quantized. See the [Tensorflow Guide](https://www.tensorflow.org/lite/performance/post_training_float16_quant) for more instructions.

Once you have a model ready, use our [MobileNet](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server/-/blob/master/server/models.cpp#L204) code as an example of how to get started!

NOTE: The current version of Tensorflow Lite is 2.2.3 and only supports the TFLITE_BUILTINS and custom opsets.


## Source

Source code is available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server)
