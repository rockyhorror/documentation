---
layout: default
title: VOXL DFS Server
parent: MPA Services
grand_parent: VOXL SDK
nav_order: 30
permalink: /voxl-dfs-server/
---

# VOXL DFS Server
{: .no_toc }

voxl-dfs-server is a background systemd service that subscribes to a stereo image feed and outputs depth information via three pipes at /run/mpa/dfs_[either disparity, dfs_disparity_scaled, or dfs_point_cloud]. Currently only grayscale stereo is supported.

![geometry_overview](/images/voxl-portal/voxl-portal-dfs.png)

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Output Format

The data format is unique to each output pipe:

dfs_disparity:
Grayscale image with pixel values corresponding to calculated disparity. Each pixel will be within the range [0, `n_disparity_levels`], which can be changed in the configuration file.

dfs_disparity_scaled:
Grayscale image as above, but pixel values will correspond to scaled disparity values in the range of [0, 255].

dfs_point_cloud:
An XYZ pointcloud (as specified by [libmodal_pipe](/libmodal-pipe/) in `modal_pipe_interfaces.h`) with the reprojected distances.

For an example of how to read this data, see [voxl-inspect-points](/voxl-inspect-points/).


## voxl-configure-dfs

Like all MPA services, voxl-dfs-server has a basic configure script to enable/disable the service as well as to reset the config file back to default for factory resets.

```
yocto:/$ voxl-configure-dfs -h

Start wizard with prompts:
voxl-configure-dfs-server

Shortcut configuration arguments for scripted setup.
factory_enable will reset the config file to factory defaults
before enabling the service.

voxl-configure-dfs-server disable
voxl-configure-dfs-server factory_disable
voxl-configure-dfs-server factory_enable
voxl-configure-dfs-server enable

show this help message:
voxl-configure-dfs-server help
```

```
yocto:/$ voxl-configure-dfs
Starting Wizard

Do you want to reset the config file to factory defaults?
1) yes
2) no
#? 1
wiping old config file
Creating new config file: /etc/modalai/voxl-dfs-server.conf

do you want to enable or disable voxl-dfs-server
1) enable
2) disable
#? 1
enabling  voxl-dfs-server systemd service
Created symlink from /etc/systemd/system/multi-user.target.wants/voxl-dfs-server.service to /etc/systemd/system/voxl-dfs-server.service.
starting  voxl-dfs-server systemd service
Done configuring voxl-dfs-server
```


## DFS Server Configuration File

The default configuration file has recommended settings for our stereo cameras.


```
yocto:/$ cat /etc/modalai/voxl-dfs-server.conf
/**
 * This file contains configuration specific to voxl-dfs-server.
 *
 * skip_n_frames
 *                      how many frames to skip between processed frames. For 30hz
 *                      input frame rate, we recommend skipping 5 frames resulting
 *                      in 5hz disparity and depth data.
 *
 * n_disparity_levels
 *                      number of disaprity levels to check, must be positive
 *                      but doesn't need to be a multiple of 16 like opencv.
 *                      For VGA resolution 48 works up to less than a meter away
 *                      but is a bit slow. 24 is faster but only works at longer
 *                      distances.
 *
 * sobel_cutoff
 *                      regions of the image with gradients below this value will
 *                      be ignored. Increase this to reduce false-positives, but
 *                      doing so will also reduce the amount of depth reported
 *                      across the image. 5-10 is a reasonable range.
 *
 * first_median_filter_size
 *                      must be an odd number. This filter serves to remove
 *                      speckles and noise after calculating pixel disparity.
 *
 * en_second_median_filter & second_median_filter_size
 *                      The second median filter can be smaller and serves
 *                      to clean up the edges of objects as well as remove
 *                      any remaining speckles the first filter left
 */
{
	"skip_n_frames":	5,
	"n_disparity_levels":	48,
	"sobel_cutoff":	5,
	"first_median_filter_size":	17,
	"en_second_median_filter":	true,
	"second_median_filter_size":	7
}

```


## Visualization Options

You can view either the regular disparity or scaled image streams as another normal camera pipe. As such, it can be viewed with [voxl-portal](voxl-portal), converted to ROS with [voxl_mpa_to_ros](/voxl-mpa-to-ros/), and logged to disk with [voxl-logger](/voxl-logger/).

See the image at the top of this page for an example.

The pointcloud can only be viewed with [voxl_mpa_to_ros](/voxl-mpa-to-ros/), and we currently do not support logging for this type.


## Troubleshooting

Accurate depth from stereo requires [calibrated cameras](/calibrate-cameras/). The voxl-dfs-server service won't even start until the specified camera calibration file cannot be found.


## Source

Source code is available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)

