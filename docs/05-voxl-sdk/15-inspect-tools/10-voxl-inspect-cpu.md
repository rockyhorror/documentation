---
layout: default
title: VOXL Inspect CPU
parent: Inspect Tools
grand_parent: VOXL SDK
nav_order: 10
permalink: /voxl-inspect-cpu/
---

# VOXL Inspect CPU


`voxl-inspect-cpu` is a tool to check the CPU/GPU utilization and temperature of the various voxl components. It is part of the [VOXL CPU Monitor](/voxl-cpu-monitor/) package and subscribes to the `cpu_monitor` pipe. If you don't get data using this tool, make sure the voxl-cpu-monitor systemd service is running using [VOXL Inspect Services](/voxl-inspect-services/).



## Use

```
yocto:/$ voxl-inspect-cpu

Name   Freq (MHz) Temp (C) Util (%)
-----------------------------------
cpu0        307.2     54.6    10.41
cpu1        307.2     53.9     8.80
cpu2       1785.6     61.0    73.52
cpu3       1920.0     56.5     0.00
Total                 61.0    23.18
10s avg                       11.08
-----------------------------------
GPU           0.0     48.0     0.00
GPU 10s avg                    0.00
-----------------------------------
memory temp:       52.0 C
memory used:   884/3770 MB
-----------------------------------
Flags
CPU freq scaling mode: auto
GPU freq scaling mode: auto
-----------------------------------
```


## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor/-/blob/master/clients/voxl-inspect-cpu.c).
