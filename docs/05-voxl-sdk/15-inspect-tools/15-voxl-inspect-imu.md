---
layout: default
title: VOXL Inspect IMU
parent: Inspect Tools
grand_parent: VOXL SDK
nav_order: 15
permalink: /voxl-inspect-imu/
---

# VOXL Inspect IMU
{: .no_toc }


`voxl-inspect-imu` is a tool to check the imu measurements of the imus on voxl. It requires that [VOXL IMU Server](/voxl-imu-server/) is running in the background, which can be checked with [VOXL Inspect Services](/voxl-inspect-services/).

---

## Arguments

### Required
<br>
IMU: Which IMU to display data from. Available imus can be seen by typing ```voxl-inspect-imu {TAB} {TAB}```. In almost all scenarios, your options are imu0 or imu1

### Optional

| Parameter    | Description                                                                               | Example                        |
|---           |---                                                                                        |---                             |
|-a --all      | Show all imu data, not scaled down for screen performance (this will print lines at ~1khz)| ```voxl-inspect-imu imu0 -a``` |
|-h --help     | Print help message                                                                        | ```voxl-inspect-imu --help```  |
|-n --newline  | Print each sample on a new line instead of updating the current output line               | ```voxl-inspect-imu imu0 -n``` |


## Example Output

```
yocto:/$ voxl-inspect-imu imu0

Acc in m/s^2, gyro in rad/s, temp in C

gravity| accl_x accl_y accl_z| gyro_x gyro_y gyro_z|  Temp |
  9.63 |  0.12  -9.57   1.08 |  0.01   0.02   0.00 | 35.03 | 
^C
received SIGINT Ctrl-C

closing and exiting
yocto:/$ 
```

## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-inspect-imu.c).

