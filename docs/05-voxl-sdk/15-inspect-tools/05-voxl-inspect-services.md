---
layout: default
title: VOXL Inspect Services
parent: Inspect Tools
grand_parent: VOXL SDK
nav_order: 05
permalink: /voxl-inspect-services/
---

# VOXL Inspect Services
{: .no_toc }


`voxl-inspect-services` is a simple tool to show which of ModalAI's systemd background services are enabled (start on boot), running, and what their cpu usage is (if running). The CPU usage should be taken with a grain of salt, as it a momentary snapshot of CPU usage. We recommend using `top` for more accurate process-specific cpu utilization.


## Typical Use

```
yocto:/$ voxl-inspect-services
 Service Name        |  Enabled  |   Running   |  CPU Usage
--------------------------------------------------------------
 docker-autorun      | Disabled  | Not Running | Not Running
 docker-daemon       | Disabled  | Not Running | Not Running
 modallink-relink    | Disabled  | Not Running | Not Running
 voxl-camera-server  |  Enabled  |   Running   |     3.4
 voxl-cpu-monitor    |  Enabled  |   Running   |     0.0
 voxl-dfs-server     | Disabled  | Not Running | Not Running
 voxl-imu-server     |  Enabled  |   Running   |     1.9
 voxl-modem          | Disabled  | Not Running | Not Running
 voxl-portal         |  Enabled  |   Running   |     0.0
 voxl-qvio-server    |  Enabled  |   Running   |    11.7
 voxl-streamer       | Disabled  | Not Running | Not Running
 voxl-tag-detector   |  Enabled  | Not Running | Not Running
 voxl-tflite-server  | Disabled  | Not Running | Not Running
 voxl-time-sync      | Disabled  | Not Running | Not Running
 voxl-vision-px4     |  Enabled  |   Running   |     4.0
 voxl-wait-for-fs    |  Enabled  |  Completed  |  Completed
```


## Optionally Show Version Numbers

The version column is disabled by default since the version number lookup is slow compared with the default three columns. However, it can be enabled with the `-v` or `--version` flag

```
yocto:/$ voxl-inspect-services --version
 Service Name        |  Version  |  Enabled  |   Running   |  CPU Usage
--------------------------------------------------------------------------
 docker-autorun      |    1.1.3  | Disabled  | Not Running | Not Running
 docker-daemon       |    1.1.3  | Disabled  | Not Running | Not Running
 modallink-relink    |   0.12.0  | Disabled  | Not Running | Not Running
 voxl-camera-server  |    0.8.1  |  Enabled  |   Running   |     5.2
 voxl-cpu-monitor    |    0.2.0  |  Enabled  |   Running   |     0.0
 voxl-dfs-server     |    0.2.2  | Disabled  | Not Running | Not Running
 voxl-imu-server     |    0.9.0  |  Enabled  |   Running   |     1.7
 voxl-modem          |   0.12.0  | Disabled  | Not Running | Not Running
 voxl-portal         |    0.1.2  |  Enabled  |   Running   |     0.0
 voxl-qvio-server    |    0.3.4  |  Enabled  |   Running   |    12.2
 voxl-streamer       |    0.2.6  | Disabled  | Not Running | Not Running
 voxl-tag-detector   |    0.0.2  |  Enabled  | Not Running | Not Running
 voxl-tflite-server  |    0.1.6  | Disabled  | Not Running | Not Running
 voxl-time-sync      |    0.8.3  | Disabled  | Not Running | Not Running
 voxl-vision-px4     |    0.9.5  |  Enabled  |   Running   |     4.0
 voxl-wait-for-fs    |    0.8.3  |  Enabled  |  Completed  |  Completed
```


## Source

Source code available on [Gitlab](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/bin/voxl-inspect-services).

