---
layout: default
title: VOXL Inspect Tags
parent: Inspect Tools
grand_parent: VOXL SDK
nav_order: 55
permalink: /voxl-inspect-tags/
---

# VOXL Inspect Tags
{: .no_toc }


voxl-inspect-tags is a test utility that's part of the [voxl-tag-detector](/voxl-tag-detector/) project. It has no command line arguments, it just subscribes to the voxl-tag-detector output pipe at /run/mpa/tag-detections as prints all detections to the screen for debug.

---

## Use

```
yocto:/$ voxl-inspect-tags

connected to voxl-tag-detector
id:  0 name: default_name
XYZ:  -1.70  -1.52   1.68
RPY:   34.1  -36.0  153.5
size_m: 0.40 latency_ms:  46.11
cam: tracking type: unknown
```

## Source

Source code available on [GitLab here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tag-detector/-/blob/master/clients/voxl-inspect-tags.c).



