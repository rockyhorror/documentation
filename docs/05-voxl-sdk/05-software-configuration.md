---
layout: default
title: Software Configuration
parent: VOXL SDK
nav_order: 05
has_children: true
has_toc: true
permalink: /software-configuration/
---

# Software Configuration

This section covers how to configure VOXL-SDK software that runs on VOXL itself.
