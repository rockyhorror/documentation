---
layout: default
title: How to Run MAVSDK on VOXL with PX4
parent: Other Software
grand_parent: VOXL SDK
nav_order: 8
has_children: false
permalink: /mavsdk/
---

# How to Run MAVSDK on VOXL with PX4
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

<iframe width="560" height="315" src="https://www.youtube.com/embed/c5xz5IDccr0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This tutorial demonstrates how to fly a VOXL-based drone autonomously, using [MAVSDK](https://mavsdk.mavlink.io/develop/en/index.html).

Link to C++ [tutorial](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-mavsdk-cpp)

Link to Python [tutorial](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-mavsdk-python)

**WARNING:** At the end of this tutorial, your drone will be flying autonomously. Please only run this in a **safe location** away from people and **wear protective eyewear!!**

## System Architecture


![modalai-voxl-mavsdk-px4-architecture.png](/images/resources/modalai-voxl-mavsdk-px4-architecture.png)


## MAVSDK Behavior with MAVLink

MAVSDK works by sending MAVLink commands to the flight computer in order to control the drone.  

When the drone is being operated by MAVSDK, it is placed in "offboard" mode.  During operation in "offboard" mode, the drone is still able to respond to "RC_CHANNELS_OVERRIDE" commands if configured correctly.  This allows the user to manually control gimbals using RC pass-though from the RC input to PWM outputs.  

Though "RC_CHANNELS_OVERRIDE" commands work at the same time as MAVSDK is operating, they do not work if an RC transmitter is connected.  
- The flight computer will only respond to the first type of input it sees.  
- If the flight controller receives "RC_CHANNELS_OVERRIDE" MAVLink messages first, then all RC transmitter input is ignored until the flight computer is rebooted.  
- If the flight controller sees RC transmitter data before any "RC_CHANNELS_OVERRIDE" MAVLink messages are received then all "RC_CHANNELS_OVERRIDE" messages are ignored.  

MAVSDK can operate regardless of which form of input is used ("RC_CHANNELS_OVERRIDE" messages or RC transmitter input) but when in offboard mode, MAVSDK will cause the flight computer to ignore flight control input for thrust, yaw, roll and pitch.  

All other RC channel inputs will be honored if appropriate (such as an RC channel configured to be an RC pass-through.)