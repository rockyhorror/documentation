---
layout: default
title: Flying Outdoors with GPS
parent: VOXL Vision PX4
grand_parent: VOXL SDK
nav_order: 40
permalink: /flying-with-gps/
---

# Overview

voxl-vision-px4 supports both indoor and outdoor flight. Currently the best performance is if the drone is configured for one mode or the other, but if you are comfortable working with PX4's EKF flying from outdoors to GPS-denied is [possible](https://docs.modalai.com/flying-without-gps/).

# PX4 Parameters

To enable GPS, the `EKF2_AID_MASK` needs to have "bit 0" enabled.

To make this easier, we have a paremeters file helper [located here](https://gitlab.com/voxl-public/flight-core-px4/px4-parameters/-/blob/master/helpers/ekf2_outdoor_gps_mag.params).

The parameters listed above will configure for outdoor flight with GPS, magnetometer, and Barometer, but without computer vision(VIO).
