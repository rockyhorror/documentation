---
layout: default
title: Nuttx Shell
parent: VOXL Vision PX4
grand_parent: VOXL SDK
nav_order: 30
permalink: /voxl-vision-px4-nuttx-shell/
---

# VOXL Vision PX4 Nuttx Shell
{: .no_toc }


As long as voxl-vision-px4 is running, you can access PX4's Nuttx shell with the voxl-px4-shell utility.

Note that this functionality is done through the Mavlink protocol. As such, your experience with the Nuttx shell with be much like that when accessing it through qGroundControl. There is no bash history in the Nuttx shell, so you can't tap the 'up' arrow to go through recent commands.

This method is, however, a good alternative to starting qGroundControl when you need to access PX4's Nuttx shell.


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Interactive Mode

Simply run `voxl-px4-shell` with no arguments to start an interactive shell. Use `Ctrl-C` to exit the shell, not the `exit` command.

```bash
yocto:/# voxl-px4-shell
Welcome to PX4's Nuttx Shell
hit Ctrl-C to exit and return to VOXL
nsh>
nsh> param show BAT_*
Symbols: x = used, + = saved, * = unsaved
x   BAT_A_PER_V [0,17] : -1.0000
x + BAT_CAPACITY [1,18] : 2200.0000
x + BAT_CRIT_THR [2,19] : 0.1000
x   BAT_EMERGEN_THR [3,20] : 0.0500
x   BAT_LOW_THR [4,21] : 0.1500
x + BAT_N_CELLS [5,22] : 4
x + BAT_R_INTERNAL [6,23] : 0.0080
x + BAT_V_CHARGED [7,24] : 4.1000
x   BAT_V_DIV [8,25] : -1.0000
x + BAT_V_EMPTY [9,26] : 3.5000
x   BAT_V_LOAD_DROP [10,27] : 0.3000

 692/1286 parameters used.
nsh> param set BAT_CAPACITY 3000
+ BAT_CAPACITY: curr: 2200.0000 -> new: 3000.0000
nsh>

```

## Sending Single Commands

If you are scripting something or just want to do something simple with one command, like set a parameter or restart px4, you can send that command in one of two ways:

```bash
yocto:/# voxl-px4-shell param set BAT_CAPACITY 3001

+ BAT_CAPACITY: curr: 3000.0000 -> new: 3001.0000
yocto:/#
```

Or, you can echo straight to the pipe which is what voxl-px4-shell is doing anyway:

```bash
yocto:/# echo "param set BAT_CAPACITY 3002" > /tmp/to_px4_shell
```

You can even read the response come back from the "from_px4_shell" pipe

```bash
yocto:/# cat /tmp/from_px4_shell
param set BAT_CAPACITY 3002
+ BAT_CAPACITY: curr: 3001.0000 -> new: 3002.0000
```

## Common Commands

You should start by running the "help" command in nuttx to see all available programs and commands.

```bash
nsh> help
help usage:  help [-v] [<cmd>]

  [        cp       export   ls       mw       set      true
  ?        date     false    mkdir    ps       sh       umount
  break    echo     free     mkfatfs  pwd      sleep    unset
  cat      exec     help     mount    rm       test     usleep
  cd       exit     kill     mv       rmdir    time

Builtin Apps:
  esc_calib                 hott_telemetry            mtd
  hello                     adc                       land_detector
  work_queue                bmm150                    serdis
  perf                      iridiumsbd                camera_trigger
  sih                       hardfault_log             roboclaw
  tune_control              bmi088                    ms4525_airspeed
  rgbled                    srf02                     uavcan
  pca9685                   frsky_telemetry           blinkm
  bmp280                    dataman                   safety_button
  ets_airspeed              landing_target_estimator  motor_ramp
  ak09916                   pwm_out_sim               led_control
  reflect                   sf0x_tests                hott_sensors
  camera_feedback           lis3mdl                   logger
  uorb_tests                mavlink                   pwm
  dmesg                     sf1xx                     gps
  hrt_test                  fw_att_control            uorb
  bst                       pga460                    fmu
  mkblctrl                  controllib_test           ms5611
  cm8jl65                   bmp388                    attitude_estimator_q
  cdev_test                 tests                     ulanding_radar
  rm3100                    test_ppm                  qmc5883
  ina226                    sdp3x_airspeed            airspeed_selector
  param                     top                       mb12xx
  motor_test                dumpfile                  wqueue_test
  ex_fixedwing_control      sd_bench                  commander_tests
  mixer                     px4_simple_app            voxlpm
  camera_capture            listener                  tap_esc
  hmc5883                   px4_mavlink_debug         sercon
  local_position_estimator  mpu6000                   ll40ls
  lsm303agr                 reboot                    vmount
  rgbled_ncp5623c           load_mon                  mc_att_control
  send_event                ekf2                      mavlink_tests
  tfmini                    navigator                 irlock
  vl53lxx                   usb_connected             bottle_drop
  mc_pos_control            mappydot                  fw_pos_control_l1
  ver                       bl_update                 rover_steering_control
  vtol_att_control          ex_hwtest                 rover_pos_control
  ms5525_airspeed           i2cdetect                 uuv_example_app
  ist8310                   batt_smbus                shutdown
  config                    lps22hb                   nshterm
  rc_input                  sf0x                      dshot
  commander                 leddar_one
  teraranger                sensors
nsh>
```

Most of the time, you will probably use the nuttx shell to reboot PX4:

```bash
nsh> reboot
rebooting px4....
NuttShell (NSH)
nsh>
```

Or get and set parameters as demonstrated in the above sections.


## Next

Next page: [Other Features](/voxl-vision-px4-other-features/)