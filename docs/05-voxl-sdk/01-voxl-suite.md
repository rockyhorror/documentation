---
layout: default
title: VOXL Suite
parent: VOXL SDK
nav_order: 01
has_children: false
has_toc: false
permalink: /voxl-suite/
---

# VOXL Suite
{: .no_toc }

VOXL Suite is the collection of VOXL SDK software packages that are installed on VOXL itself, as opposed to tools and build environments that are installed on a desktop. It is consolidated into a meta-package, voxl-suite_x.x.x.ipk, that depends on all of the core ModalAI tools and services.

The voxl-suite package and its dependencies are distributed via the OPKG package manager with repositories available at [http://voxl-packages.modalai.com/](http://voxl-packages.modalai.com/). For more information see the [configuring OPKG](/configure-opkg/) page on how to configure and update packages.

voxl-suite is also bundled up with our [system image](/voxl-system-image/) releases to create what's called a "platform release" available at [downloads.modalai.com](downloads.modalai.com).

The voxl-suite meta package depends on the latest stable version of all of its constituents, therefore regular calls to `opkg update` and `opkg upgrade` will keep you up to date with bugfixes before a new voxl-suite and new platform release is published. voxl-suite releases are landmarks in an otherwise continuous software development process.

The scripts and documents describing how voxl-suite is built and bundled can be found [here](https://gitlab.com/voxl-public/utilities/voxl-suite).


# Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}



# v0.5.0

Requires VOXL system image 3.3+

```
    * NEW TOOL:    voxl-calibrate-cameras utility
    * NEW TOOL:    imu thermal calibration
    * NEW TOOL:    voxl-configure-opkg
    * NEW TOOL:    voxl-calibrate-px4-horizon tool
    * NEW TOOL:    voxl-portal web interface
    * NEW TOOL:    voxl-wait-for-fs service
    * NEW TOOL:    voxl-list-pipes
    * ADDITION:    libmodal_pipe pause-resume function
    * ADDITION:    voxl-configure extrinsics add support for Starling
    * ADDITION:    voxl-mpa-to-ROS dynamic pipe detection (no more config!!)
    * IMPROVEMENT: file system syncs more frequently
    * IMPROVEMENT: voxl-qvio-reset is more complete
    * IMPROVEMENT: voxl-configure cameras wizard tweaks
    * IMPROVEMENT: voxl-configure-mpa support new part numbers
    * IMPROVEMENT: voxl-inspect-services speedup
    * IMPROVEMENT: too many little tweaks and fixes to list
```

| Package                                                                                         | Package Version |
|---                                                                                              |---              |
| [libjpeg_turbo](https://gitlab.com/voxl-public/third-party/voxl-jpeg-turbo)                     | 9.0.4+   |
| [libmodal_exposure](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)                 | 0.0.2+   |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                         | 0.3.6+   |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)           | 2.1.1+   |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                               | 1.1.5+   |
| [libvoxl_cutils](https://gitlab.com/voxl-public/core-libs/libvoxl_cutils)                       | 0.0.2+   |
| [libvoxl_io](https://gitlab.com/voxl-public/core-libs/libvoxl_io)                               | 0.5.4+   |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager)       | 0.0.2+   |
| [mongoose](https://gitlab.com/voxl-public/third-party/voxl-mongoose)                            | 7.3.0+   |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                                  | 4.5.2-2+ |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                      | 10.0.2+  |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-calibration) | 0.1.1+   |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.7.1+   |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor)     | 0.2.0+   |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)       | 0.2.2+   |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)             | 1.1.3+   |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)             | 0.0.5+   |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)       | 0.9.1+   |
| [voxl-mavlink](https://gitlab.com/voxl-public/third-party/voxl-mavlink)                         | 0.12.0+  |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                               | 0.0.2+   |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)         | 0.3.6+   |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros)                                | 0.2.0+   |
| [voxl-portal](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-portal)               | 0.1.1+   |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server)     | 0.3.4+   |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                         | 0.2.6+   |
| [voxl-tag-detector](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tag-detector)   | 0.0.2+   |
| [voxl-tflite](https://gitlab.com/voxl-public/third-party/voxl-tflite)                           | 2.2.3+   |
| [voxl-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server) | 0.1.5+   |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                               | 0.8.4+   |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4)       | 0.9.5+   |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                                   | 0.0.3+   |



# v0.4.6

Requires System Image 3.3+

Contains the following packages:

| Package                                                                                         | Package Version |
|---                                                                                              |---              |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                               | 0.7.1+   |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)         | 0.2.6+   |
| [libmodal_exposure](https://gitlab.com/voxl-public/core-libs/libmodal_exposure)                 | 0.0.2+   |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                         | 0.3.5+   |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)           | 2.0.7+   |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                               | 1.1.5+   |
| [libvoxl_cutils](https://gitlab.com/voxl-public/core-libs/libvoxl_cutils)                       | 0.0.2+   |
| [libvoxl_io](https://gitlab.com/voxl-public/core-libs/libvoxl_io)                               | 0.5.4+   |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager)       | 0.0.2+   |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                                  | 4.5.2-1+ |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                      | 10.0.2+  |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.7.1+   |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-cpu-monitor)     | 0.1.7+   |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server)       | 0.2.0+   |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)             | 1.1.3+   |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)             | 0.0.5+   |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server)       | 0.8.1+   |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                               | 0.12.0+  |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes)                                     | 0.1.6+   |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server)     | 0.3.1+   |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                         | 0.2.3+   |
| voxl-tflite(no repo)                                                                            | 0.0.1+   |
| [voxl-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tflite-server) | 0.1.0+   |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4)       | 0.9.2+   |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                                   | 0.0.3+   |


# v0.3.3

Requires System Image 3.2+

Contains the following packages:

| Package                                                                                   | Package Version |
|---                                                                                        |---              |
| [voxl-utils](https://gitlab.com/voxl-public/utilities/voxl-utils)                         | 0.6.0+    |
| [libmodal_json](https://gitlab.com/voxl-public/core-libs/libmodal_json)                   | 0.3.4+    |
| [libmodal_pipe](https://gitlab.com/voxl-public/modal-pipe-architecture/libmodal_pipe)     | 1.7.8+    |
| [librc_math](https://gitlab.com/voxl-public/core-libs/librc_math)                         | 1.1.4+    |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/utilities/mavlink-camera-manager) | 0.0.1+    |
| [opencv](https://gitlab.com/voxl-public/core-libs/voxl-opencv)                            | 4.5.1+    |
| [openmp](https://gitlab.com/voxl-public/other/voxl-openmp)                                | 10.0.1+   |
| [voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server) | 0.5.6+ |
| [voxl-dfs-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-dfs-server) | 0.0.7+    |
| [voxl-docker-support](https://gitlab.com/voxl-public/utilities/voxl-docker-support)       | 1.1.1+ |
| [voxl-gphoto2](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-gphoto2)       | 0.0.5+ |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/ros/voxl-hal3-tof-cam-ros)         | 0.0.5+ |
| [voxl-imu-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-imu-server) | 0.7.8+ |
| [voxl-modem](https://gitlab.com/voxl-public/utilities/voxl-modem)                         | 0.11.0+ |
| [voxl-mpa-tflite-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tflite-server) | 0.0.2+ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools)   | 0.1.6+ |
| [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes)                               | 0.1.0+ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-qvio-server) | 0.2.1+ |
| [voxl-rtsp](https://gitlab.com/voxl-public/utilities/voxl-rtsp)                           | 1.0.3+ |
| [voxl-streamer](https://gitlab.com/voxl-public/utilities/voxl-streamer)                   | 0.2.1+ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-vision-px4) | 0.8.1+ |
| [voxl-vpn](https://gitlab.com/voxl-public/utilities/voxl-vpn)                             | 0.0.3+ |


# v0.2.0

Changes:

- Updated `voxl-vision-px4`, PX4 disconnect detection, better handling of 'CRC errors', UART port and baud configurable, keep publishing data if VIO is off, wizard improvements
- Updated `voxl-modem`, added LTE v2 support
- Updated `voxl-utils`, added `voxl-backup` util for camera calibration backup
- Added new `voxl-vpn` package
- Added new `voxl-time-sync` package
- Removed ffmpeg as it's normally used via Docker containers

Contains the following packages:

| Package                                                                        | Package Version |
|---                                                                             |---              |
| docker                                                                         | 1.9.0  |
| imu_app                                                                        | 0.0.6  |
| libmodal_pipe                                                                  | 1.2.2  |
| librc_math                                                                     | 1.1.2  |
| modalai-vl                                                                     | 0.1.3  |
| [libvoxl_io](https://gitlab.com/voxl-public/libvoxl_io)                        | 0.5.2  |
| [voxl-cam-manager](https://gitlab.com/voxl-public/voxl-cam-manager)            | 0.2.2  |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-docker-support)      | 1.1.1  |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros)  | 0.0.2  |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-modem)                        | 0.10.0 |
| [voxl-nodes](https://gitlab.com/voxl-public/voxl-nodes)                        | 0.0.8  |
| [voxl-rtsp](https://gitlab.com/voxl-public/voxl-rtsp)                          | 1.0.2  |
| [voxl-time-sync](https://gitlab.com/voxl-public/voxl-time-sync)                | 0.0.1  |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-utils)                        | 0.5.2  |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4)              | 0.6.8  |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-vpn)                            | 0.0.2  |
| voxl_imu                                                                       | 0.0.4  |


Tested against:

- System Image 2.5.2-1.0.1
- System Image 2.3.0-1.0.1


# v0.1.1

Contains the following packages:

| Package                                                                        | Package Version (min) |
|---                                                                             |---                    |
| docker                                                                         | 1.9.0 |
| [ffmpeg](https://gitlab.com/voxl-public/voxl-ffmpeg)                           | 4.2.2 |
| imu_app                                                                        | 0.0.6 |
| [librc_math](https://gitlab.com/voxl-public/librc_math)                        | 1.1.2 |
| [libvoxl_io](https://gitlab.com/voxl-public/libvoxl_io)                        | 0.4.1 |
| [libvoxl_pipe](https://gitlab.com/voxl-public/libvoxl_pipe)                    | 1.0.1 |
| modalai-vl                                                                     | 0.1.3 |
| [voxl-cam-manager](https://gitlab.com/voxl-public/voxl-cam-manager)            | 0.2.2 |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-docker-support)      | 1.1.1 |
| [voxl-hal3-tof-cam-ros](https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros)  | 0.0.2 |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-modem)                        | 0.9.3 |
| [voxl-nodes](https://gitlab.com/voxl-public/voxl-nodes)                        | 0.0.8 |
| [voxl-rtsp](https://gitlab.com/voxl-public/voxl-rtsp)                          | 1.0.2 |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-utils)                        | 0.5.1 |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4)              | 0.6.1 |
| voxl_imu                                                                       | 0.0.4 |


Tested against: System Image 2.3.0-1.0.1
