---
layout: default
title: OpenCV
parent: 3rd Party Libs
grand_parent: VOXL SDK
nav_order: 25
permalink: /opencv/
---

# OpenCV
{: .no_toc }

ModalAI hosts a native, VOXL-tuned version of OpenCV in 32-bit and 64-bit. The repository containing the tools required to build the OpenCV package for VOXL is available on [Gitlab](https://gitlab.com/voxl-public/core-libs/voxl-opencv).

Some good examples of using OpenCV on VOXL:

 - [voxl-logger](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-logger.cpp) and [voxl-replay](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-mpa-tools/-/blob/master/tools/voxl-replay.cpp) use OpenCV to save and load PNG files.

 - [voxl-tag-detector](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-tag-detector/-/blob/master/server/main.cpp) uses opencv to generate stereo image rectification maps, but uses a more optimized algorithm do do the actual undistortion.

 - [voxl-calibrate-camera](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-calibration) uses OpenCV to calculate camera lens parameters.



