---
layout: default
title: Mavlink
parent: 3rd Party Libs
grand_parent: VOXL SDK
nav_order: 40
permalink: /mavlink/
---

# Mavlink
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

Mavlink standard and custom messages conveniently bundled up into an ipk. No building is required, this just contains headers.

You can find the code and README on [Gitlab](https://gitlab.com/voxl-public/core-libs/voxl-mavlink)


---

