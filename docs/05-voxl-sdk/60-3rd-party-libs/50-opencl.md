---
layout: default
title: OpenCL
parent: 3rd Party Libs
grand_parent: VOXL SDK
nav_order: 50
permalink: /build-for-gpu-opencl/
---

# Build for GPU (OpenCL)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Overview

VOXL contains an embedded [Adreno 530](https://en.wikipedia.org/wiki/Adreno) GPU with 256 ALUs. This GPU is exposed through both OpenCL 1.1 and OpenGLES 3.1. The GPU can be exploited for significant algorithmic acceleration for use cases like computer vision and deep learning.


## Examples


voxl-dfs-server
voxl-tflite-server