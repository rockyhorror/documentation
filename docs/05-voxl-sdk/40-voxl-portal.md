---
layout: default
title: VOXL Portal
parent: VOXL SDK
nav_order: 40
has_children: true
has_toc: true
permalink: /voxl-portal/
---

# VOXL Portal

The voxl-portal package provides an embedded webserver on VOXL. It enables camera inspection and other debug tools all via web browser interface at your VOXL's IP address.

[Source Code](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-portal)


![voxl-portal-hires](/images/voxl-portal/voxl-portal-hires.png)
