---
layout: default
title: Flight Core Configuration
parent: Flight Core Datasheets
grand_parent: Flight Core
nav_order: 4
permalink: /flight-core-datasheets-configuration/
---

# Flight Core Configuration
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## QGroundControl

Configuration of the VOXL Flight is fully supported by QGroundControl 4.0.

## PX4 Parameter Files

Parameter files are available for reference:

- <https://gitlab.com/voxl-public/px4-parameters>

## System Defaults

The following describe the system defaults, as specified by `boards/modalai/fc-v1/init/rc.board_defaults`

### Safety Switch Disabled

The saftety switch can be 'pressed' by pulling `J13` pin 5 up to 3.3V (which is provided by J13 pin 1).  By default, we have the safety switch disabled, but it can be enabled via QGroundControl by setting `CBRK_IO_SAFETY` to 0.

### MAVLink Instance 1

- configured by default at TELEM2 (exposed at `J1`) at 921600 baud in Normal mode
- this is intended as the primary serial interface to communicate with VOXL
- this is configurable by the user, except on VOXL Flight, since it's in the PCB
