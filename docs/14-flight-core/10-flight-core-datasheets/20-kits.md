---
layout: default
title: Flight Core Kits
parent: Flight Core Datasheets
grand_parent: Flight Core
nav_order: 20
permalink: /flight-core-kits/
---

# Flight Core Kits
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Flight Core Development Kit

![fc-dk](/images/quickstart/flight-core/fc-dk.jpg)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/flight-core)

Kit contains:

- [MKIT-00011-1 - Flight Core Board Kit, R1](/flight-core-datasheet/)
- [MKIT-00037-2 - VOXL Power Module v3](/power-module-v3-datasheet/)
- [MCBL-00004](/cable-datasheets/#mcbl-00004/) - 8-channel Breakout Board and cable
- [MCBL-00005](/cable-datasheets/#mcbl-00005/) - RC Input Cable
- [MCBL-00010](/cable-datasheets/#mcbl-00010/) - 4-pin JST to USBA Cable (PX4 to QGroundControl)

## Board Only

- [MKIT-00011-1 - Flight Core Board Kit, R1](/flight-core-datasheet/)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/flight-core-pcb-only)
