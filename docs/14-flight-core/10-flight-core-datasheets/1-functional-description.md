---
layout: default
title: Flight Core Functional Description
parent: Flight Core Datasheets
grand_parent: Flight Core
nav_order: 1
permalink: /flight-core-datasheets-functional-description/
---

# Flight Core Functional Description

{: .no_toc }

## Table of contents

{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

ModalAI Flight Core is a PX4 flight controller that uses a very similar architecture to FMUv5x. It's design goal is to be software compatible with the FMUv5x architecture in a smaller Pixracer-style form factor.

## Dimensions

![flight-core-dims.png](/images/flight-core/flight-core-dims.png)
![flight_core_v1_imu_locations.png](/images/flight-core/flight_core_v1_imu_locations.png)

## Features

| Feature          | Details                                                                                                                                                         |
|:-----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Weight           | 6 g                                                                                                                                                             |
| MCU              | 216MHz, 32-bit ARM M7 [STM32F765II](https://www.st.com/en/microcontrollers-microprocessors/stm32f765ii.html)                                                    |
| Memory           | 256Kb FRAM                                                                                                                                                      |
|                  | 2Mbit Flash                                                                                                                                                     |
|                  | 512Kbit SRAM                                                                                                                                                    |
| Firmware         | [PX4](https://github.com/PX4/Firmware/tree/master/boards/modalai/fc-v1)                                                                                         |
| IMUs             | [ICM-20602](https://www.invensense.com/products/motion-tracking/6-axis/icm-20602/) (SPI1)                                                                       |
|                  | [ICM-42688-P](https://invensense.tdk.com/products/motion-tracking/6-axis/icm-42688-p/) (SPI2)                                                                   |
|                  | [BMI088](https://www.bosch-sensortec.com/bst/products/all_products/bmi088_1) (SPI6)                                                                             |
| Barometer        | [BMP388](https://www.bosch-sensortec.com/bst/products/all_products/bmp388) (I2C4)                                                                               |
| Secure Element   | [A71CH](https://www.nxp.com/products/security-and-authentication/authentication/plug-and-trust-the-fast-easy-way-to-deploy-secure-iot-connections:A71CH) (I2C4) |
| microSD Card     | [Information on supported cards](https://dev.px4.io/v1.9.0/en/log/logging.html#sd-cards)                                                                        |
| Inputs           | GPS/Mag                                                                                                                                                         |
|                  | Spektrum                                                                                                                                                        |
|                  | Telemetry                                                                                                                                                       |
|                  | CAN bus                                                                                                                                                         |
|                  | PPM                                                                                                                                                             |
| Outputs          | 6 LEDs (2xRGB)                                                                                                                                                  |
|                  | 8 PWM Channels                                                                                                                                                  |
| Extra Interfaces | 3 serial ports                                                                                                                                                  |
|                  | I2C                                                                                                                                                             |
|                  | GPIO                                                                                                                                                            |

[Top](#table-of-contents)

## Block Diagram

![fc-dk-preliminary-datasheet.png](/images/datasheet/flight-core/fc-dk-preliminary-datasheet.png)
*Figure 1*
{: style="text-align: center;"}

[Top](#table-of-contents)

## Orientation

The following is a 'top down' view of the Flight Core depicting the orientation.  Starting in PX4 v1.11, the orientation is updated to better align with the usage of the VOXL-Flight.  The following is a 'top down' view of the Flight Core depicting a `ROTATION_NONE` setup.

| PX4 Version  | Orientation to Achieve Forward | Branch | Notes |
| ---          | ---                            | ---      |
| 1.10         | `ROTATION_YAW_180`             | [relesase/1.10](https://github.com/PX4/Firmware/tree/release/1.10) | Not Recommended |
| modalai_1.10 | `ROTATION_NONE`                | [modalai_1.10](https://github.com/modalai/px4-firmware/tree/modalai-1.10) | Recommended |
| master       | `ROTATION_NONE`                | [master](https://github.com/PX4/Firmware) | In Development |

![Flight Core Orientation](/images/datasheet/flight-core/fc-orientation-144-dpi.png)

## Power Supply

### Recommended

A 5VDC supply is required to power the Flight Core.  The `FC-PM` Power Module board is recommended, which provides battery voltage and amperage monitoring through an I2C interface when connected to `J6` using the `MCBL-00003` cable.

Other connectors also allow a 5VDC supply to be connected (see [connectors page](fc-connectors.md) )

- J6 - recommended, provides I2C interace for monitoring
- J1/J4/J5

### Power Usage

**NOTE:** The following is preliminary data.

Test configuration:

- Using a 5VDC supply
- Using PX4 this [commit](https://github.com/modalai/px4-firmware/commit/f671fdb7fa88d00b0a7b4b6b922f1fe7b4706985)

| Use Case | Typical Usage (Peak) |
| --- | --- |
| Idle | 180 mA |
| Idle, with GPS (HolyBro Pixhawk 4, Second GPS)| 260 mA |
| Idle, with Spektrum DSMX | 200 mA |
| Idle, with Holybro Telemetry | 230 mA |
| Idle, with GPS/Spektrum/Telemetry | 330 mA |

[Top](#table-of-contents)

Pixhawk is a registered trademark of Lorenz Meier.
