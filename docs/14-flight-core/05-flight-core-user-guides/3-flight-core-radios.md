---
layout: default
title: Flight Core Radios
parent: Flight Core User Guides
grand_parent: Flight Core
nav_order: 2
has_children: false
permalink: /flight-core-radios/
---

# Flight Core Radios
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

This section covers different radio options for Flight Core and VOXL Flight, and subsequently, the VOXL m500 reference drone.

## Spektrum

### Spektrum Tested Receivers

Spektrum receiver connection instructions [here](https://docs.modalai.com/flight-core-connections/#spektrum-receiver-default)

Non-exclusive list:

- SPM9645 with [MCBL-00005](/cable-datasheets/#mcbl-00005)
- SPM9745 with [MCBL-00005](/cable-datasheets/#mcbl-00005)

## FrSky

FrSky receiver connection instructions [here](https://docs.modalai.com/flight-core-connections/#frsky-receiver-optional)

Video tutorial on how to pair an FrSky X8R [(Youtube)](https://www.youtube.com/watch?v=1IYg5mQdLVI)

### FrSky Tested Receivers

Non-exclusive list:

- X8R with [MCBL-00018](/cable-datasheets/#mcbl-00018)
- R-XSR with [MCBL-00021](/cable-datasheets/#mcbl-00021)

### FrSky Telemetry Output

To use FrSky Telemetry output, you use the spare telemetry input (`J5` on Flight Core, `J1010` on VOXL Flight) and configure the `TEL_FRSKY_CONFIG` setting.

For example, power the receiver with 5VDC using the spare PPM input (`J9` and`J1003` on Flight Core VOXL Flight respectively) pins 1 and 3 (5VDC and GND).

Then, route the telemetry cable to telemetry input (`J5` on Flight Core, `J1010` on VOXL Flight).

Using QGroundControl, go to `Parameters > Telemetry > TEL_FRSKY_CONFIG` and set to `TELEM1`.

## Graupner

Graupner receiver connection instructions [here](https://docs.modalai.com/flight-core-connections/#graupner-receiver-optional)

### Graupner Tested Receivers

Non-exclusive list:

- [MZ 12 Pro, Setup Guide Video](https://youtu.be/fqQ1ET4Kn9w)
- GR-16 with [MCBL-00018](/cable-datasheets/#mcbl-00018)
