---
layout: default
title: Flight Core PWM ESC Calibration
parent: Flight Core User Guides
grand_parent: Flight Core
nav_order: 97
has_children: false
permalink: /flight-core-pwm-esc-calibration/
---

# Flight Core PWM ESC Calibration
{: .no_toc }

The following describes an alternative way to calibrate PWM based ESCs without the using QGroundControl, which requires you to remove the battery upon power up of the VOXL Flight or Flight Core, which isn't an option for a non-USB powered device like ours.

**NOTE:** Alternatively, you can avoid calibration if your ESCs support DShot.  Checkout the `DSHOT_CONFIG` parameter in the PX4 settings using QGroundControl.

## px4-esc-cal-tool

Clone the following repo to proceed:

```bash
git clone https://gitlab.com/voxl-public/px4-esc-cal-tool
cd px4-esc-cal-tool
```

This tool uses python3, so depending on your environment, you may need to run `python3` or `python`.

Dependencies:

```bash
pip install pymavlink pyserial
```

or

```bash
pip3 install pymavlink pyserial
```

## WARNING

**PLEASE MAKE SURE YOU HAVE THE PROPS REMOVED**

## Procedure

### USB

Required:

- Host PC (OSX or Ubuntu)
- Python 3
- VOXL-Flight or Flight Core
- USB to JST Cable (`MCBL-00006` or `MCBL-00010` + MicroUSB cable)

Instructions:

1. Remove the PWM output cable from the board (`J1007` on VOXL Flight or `J7` on Flight Core)
2. Connect host PC to VOXL-Flight or Flight Core using USB to JST cable
3. Power vehicle with battery
4. Run the following command from the root of the `px4-esc-cal-tool` project:

```bash
# For v1.10
python px4-esc-cal-tool.py

# For v1.11+
python px4-esc-cal-tool-v1-11.py
```

or

```bash
# For v1.10
python3 px4-esc-cal-tool.py

# For v1.11+
python3 px4-esc-cal-tool-v1-11.py
```

5. Follow the instructions on screen

### UDP

Required:

- Host PC (OSX or Ubuntu)
- Python 3
- VOXL-Flight or VOXL + Flight Core, where [voxl-vision-px4](https://docs.modalai.com/voxl-vision-px4/) is configured

Instructions:

1. Remove the PWM output cable from the board (`J1007` on VOXL Flight or `J7` on Flight Core)
2. Power vehicle with battery
3. Connect host PC to VOXL-Flight or Flight Core over local network using [voxl-vision-px4](https://docs.modalai.com/voxl-vision-px4/)

```bash
python px4-esc-cal-tool.py 0.0.0.0:14550
```

or

```bash
python3 px4-esc-cal-tool.py 0.0.0.0:14550
```


5. Follow the instructions on screen
