---
layout: default
title: Miscellaneous PX4 Configuration
parent: Flight Core Quickstarts
grand_parent: Flight Core
nav_order: 6
permalink: /fc-configure-misc/
---

# Miscellaneous PX4 Configuration

## 10. ESC Calibration

If you have ESCs that support the D-Shot protocol we highly recommend enabling D-Shot mode in PX4 parameters to avoid needing to calibrate the ESCS. This can be done by setting the DSHOT_CONFIG to 600 or 1200, whichever the highest speed is that your ESC supports. Set to 0 to use normal ESC pulse signaling. The ESCs currently used on the ModalAI M500 NO NOT support D-Shot.

You can follow the standard PX4 ESC calibration instructions [here](https://docs.px4.io/master/en/advanced_config/esc_calibration.html). This MUST be performed before first flight if you aren't using DShot ESCs.

You must use a 5V Power supply connected to the Power Module to power the Flight Core and VOXL (if being used) but NOT the ESCS so you can proceed with ESC calibration.

![10-esc-cal.png](/images/qgc/10-esc-cal.png)

## 11. Battery Configuration

The PX4 battery documentation is very detailed, and perhaps a bit overwhelming:
[https://docs.px4.io/master/en/config/battery.html](https://docs.px4.io/master/en/config/battery.html). The battery parameters we suggest using with the ModalAI Flight Core are as follows:

### BAT_V_DIV & BAT_A_PER_V

These are not applicable and can be ignored since the Flight Core uses a digital voltage and current sensor instead of an ADC.

### BAT_N_CELLS

This should be set to the number of series cells in your pack. For the ModalAI M500 we suggest using 3S packs and setting this parameter to match.

### BAT_V_CHARGED BAT_V_EMPTY

We suggest setting to 4.15 and 3.4 respectively for LiPo cells. For LiHV cells these values should be increased to 4.25 and 3.5

### BAT_V_LOAD_DROP & BAT_R_INTERANAL

We suggest ignoring BAT_V_LOAD_DROP and setting the internal resistance of the battery (BAT_R_INTERNAL) directly. If you do not have a charger or meter capable of measuring the internal resistance then you can use 0.015 (15 mOhm) as a reasonable starting point for a typical 3500mAh 3S LiPo.

### BAT_CAPACITY

This is an optional setting if you have an accurate measurement of your battery's actual capacity in mAh as measured on a discharger. Don't attempt to use the advertised capacity, it will be wrong. If you are unsure, leave this set to -1. Also if you frequently restart PX4 throughout the use of a battery pack or frequently start flights with partially discharged packs we suggest leaving this parameter set to -1 as PX4 makes the assumption that the pack is fully charged when starting up if it is given a capacity.

![11-battery-config.png](/images/qgc/11-battery-config.png)

## 12. Set System ID

We highly recommend each drone have its own unique system ID. QGroundControl supports incoming telemetry from multiple drones as long as they have different system IDs. It will be very unhappy if multiple drones have the same system ID!

![12-set-system-id.png](/images/qgc/12-set-system-id.png)

## Next Steps

Now you are ready for your [first flight](/fc-first-flight/)!
