---
layout: default
title: Calibrate Sensors
parent: Flight Core Quickstarts
grand_parent: Flight Core
nav_order: 4
permalink: /px4-calibrate-sensors/
---

# Calibrate Sensors

## 5. Unplug USB and Switch to Wireless Telemetry

If you have already [installed and configured voxl-vision-px4](/voxl-vision-px4-installation/), then wireless telemetry should now be working between PX4 and QGroundControl. If not, make sure VOXL and your Ground Control Station are on the same network and the /etc/modalai/voxl-vision-px4.conf file on VOXL is configured for the right IP address of your Ground Control Station.

## 6. Enter Sensor Calibrate Section

Now click the sensor calibration section. All sensor icons should be red at this point.

![6.1-sensor-calibration.png](/images/qgc/6.1-sensor-calibration.png)

When you click on the Compass or Accelerometer calibration buttons, QGroundControl rightly asks you to confirm the board orientation. If using the PX4 master branch or the [ModalAI build 1.10](https://storage.googleapis.com/flight-core-firmware-1-10/latest/modalai_fc-v1_default.px4) then use `ROTATION_NONE`.  The default orientation in the parameters file you uploaded before takes this into account.  **NOTE:** the 1.10 release from PX4 for Flight Core is not recommended as there have been fixes including a orientation change.

During the calibration process QGroundControl will ask you to place the airframe in multiple orientations with pictures. If at any point the indicated orientation does not match how you are holding the airframe, you should recheck this orientation parameter.

![6.2-check-rotation.png](/images/qgc/6.2-check-rotation.png)

Now complete calibration for all sensors following QGroundControl's instructions.

### 6.1 Skipping Compass Calibration and Disabling GPS

The following requires the use of the VOXL companion computer.  Please ensure you have [voxl-vision-px4 installed and configured](/voxl-vision-px4-installation/) on VOXL.

**NOTE:** For PX4 1.10, the following currently requires a custom branch utilizing a modified [ECL library](https://github.com/PX4/ecl) with changes captured in [this commit](https://github.com/modalai/ecl/commit/bbfc7a877b6a344ec6b3d57d164d160e22436495).

The `modalai-1.10-ekf2-init-vio` branch is here: <https://github.com/modalai/px4-firmware/tree/modalai-1.10-ekf2-init-vio>

This should be considered for use with testing only and is not fully validated.

If not using a magnetometer and GPS, you can skip the compass calibration and disable GPS with the following parameter changes:

- `SYS_HAS_MAG` set to `Disabled`
- `GPS_1_CONFIG` set to `Disabled`

Reboot the Flight Core and VOXL.  Check that you are getting VIO data in QGroundControl's MAVLink shell running the following command:

```bash
nsh> listener vehicle_visual_odometry

TOPIC: vehicle_visual_odometry
 vehicle_odometry_s
        timestamp: 196688354  (0.028220 seconds ago)
        x: 0.0156
        y: -0.0666
        z: -0.0208
        q: [0.9990, 0.0254, -0.0094, -0.0355]
        q_offset: [0.0000, 0.0000, 0.0000, 0.0000]
        pose_covariance: [0.0000, 0.0000, -0.0000, -0.0000, -0.0000, 0.0000, 0.0000, 0.0000, 0.0000, -0.0000, -0.0000, -0.0000, -0.0000, 0.0000, 0.0000, 0.0000, 0.0000, -0.0000, 0.0000, -0.0000, -0.0000]
        vx: nan
        vy: nan
        vz: nan
        rollspeed: nan
        pitchspeed: nan
        yawspeed: nan
        velocity_covariance: [nan, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000]
        local_frame: 0
```

Verify that EKF2 has valid `local position` data.  If not, ensure you have updated ECL with changes captured in [this commit](https://github.com/modalai/ecl/commit/bbfc7a877b6a344ec6b3d57d164d160e22436495).

```bash
nsh> ekf2 status
INFO  [ekf2] local position: valid
INFO  [ekf2] global position: invalid
INFO  [ekf2] time slip: 0 us
ekf2: update: 32585 events, 5217128us elapsed, 160.11us avg, min 2us max 1498us 225.559us rms
```

## Next Steps

[Configure RC Radio and Flight Modes](/configure-rc-radio/)
