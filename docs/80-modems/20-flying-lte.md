---
layout: default
title: How to Fly using LTE with VOXL
parent: Modems
nav_order: 20
has_children: false
permalink: /voxl-lte/
---

# How to Fly using LTE with VOXL and PX4
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

Useful Resources:
- [VOXL LTE v2 Modem User Guide](/lte-v2-modem-user-guide/)
- [VOXL LTE v1 Modem User Guide](/lte-v1-modem-user-guide/)

## Getting Connected

On the VOXL, the LTE connection will need to be configured in order to connect to the network. LTE v2 Modem Instructions [here](/lte-v2-modem-manual/), LTE v1 Modem Instructions [here](/lte-v1-modem-manual/).  In order for the QGroundControl station to find the VOXL connected over LTE, the VOXL's IP address needs to be reachable. There are two methods:
1. Static IP obtained through cellular carrier
2. Use a cloud man-in-the-middle setup with VPN

To connect the QGroundControl station to the VOXL, it just needs an internet connection. This could be from your facility's Wi-Fi network. If you're going out to the field , an internet connection is needed through a cellular dongle.

## Backup Control

### RC Control for safety pilot

Often a backup R/C controller such as Spektrum or Futaba are used for safety. The range of the R/C controller is limited where the range of LTE is significantly further, as far as the network will take you. PX4's lost-link behaviour needs to be configured through QGroundControl to handle lost safety R/C.

### Joystick Control over LTE

Joystick control can be routed through QGroundControl if joysticks are connected to the same computing device that is running QGroundControl.

## Alternate Flight Controllers

Other flight controllers that use MAVLink, such as ArduPilot, should work but are not well tested by ModalAI

## Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/lj1EZ_eTGPI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
