---
layout: default
title: V1 LTE Modem and USB Add-On Board User Guide
parent: 4G LTE Modems
grand_parent: Modems
nav_order: 3
permalink: /lte-v1-modem-user-guide/
---

# LTE v1 Modem and USB Add-On Board User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL system provides the ability to easily and quickly add an LTE connection.  The following guide provides you the necessary details on doing so with an LTE v1 Modem.

[Datasheet](/lte-modem-and-usb-add-on-datasheet/)

## Requirements

### Hardware

In order to connect the VOXL to an LTE network using the v1 Modem, you will be using the following hardware:

| Part Number | Description |
| --- | --- |
| [VOXL-ACC-LTEH-NA](https://www.modalai.com/collections/voxl-add-ons/products/voxl-lte?variant=31622211207219) | Add-on board for VOXL which enables North American carrier LTE as well as adds a USB hub to integrate more peripherals CAT4 LTE Bands 2,4,5,12 |

![v1 Modem](/images/datasheet/LTE/voxl-acc-lteh-na-144.png)

Each kit conveniently comes with two LTE antennas (which are also available [here](https://www.modalai.com/products/lte-antennas-for-voxl-modem-add-ons))

### Software

The following software is required and should already be pre-installed:

- [System Image 2.5.2 +](/voxl-system-image/)
- [voxl-modem_0.10.0 +](http://voxl-packages.modalai.com/stable/)

## SIM Cards and Supported Networks

The LTE modems require a Nano SIM card, most major cellular network carriers are supported.  

For Band 3 support, the `VOXL-ACC-LTEH-DOD` modem is required and Band 3 SIM cards need to be requested through ModalAI.

## SIM Card Installation

- The SIM card needs to be installed in the orientation pictured below.

<img src="/images/datasheet/voxl-lteh/sim-card-v1-1.jpg" width="200">

- Slide the SIM card into the SIM card slot on the bottom of the LTE board.

<img src="/images/datasheet/voxl-lteh/sim-card-v1-2.jpg" width="200">

- Push the SIM card in until you feel the card click into place. 

<img src="/images/datasheet/voxl-lteh/sim-card-v1-3.jpg" width="200">

To remove the SIM card, push the card slightly further into the slot until you feel a click, the card will then be able to slide out.

## Hardware Setup

- Remove power and USB from the VOXL
- Insert your SIM card into the modem's SIM card slot
- Attach antennas to the modem
- Attach LTE modem to VOXL using VOXL's J13 connector
- **Note:** The switch near the antenna connectors should be in the **'off'** position.  When 'on', it will force the main board to into USB boot mode
- Reconnect USB and power to VOXL

## Software Setup

VOXL ships with a simple utility to assist with setting up a modem. (voxl-modem_0.10 + required)

`voxl-configure-modem` allows you to quickly and easy bring up a network connection using an LTE modem.

```bash
# On VOXL
voxl-configure-modem
```

You will first be asked which modem type you are setting up:

```bash
What type of modem are you using?

If you are unsure of which modem you have, take a look at the following datasheets:

v1 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/
v2 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/
Microhard Modem: https://docs.modalai.com/microhard-add-on-datasheet/

1) v1
2) v2
3) microhard
```

Choose `1` for v1 modem.

You will then be asked if you are attempting to connect to ModalLink: 

```bash
Are you attempting to connect to ModalLink? (not common)
1) yes
2) no 
```

If you are using a SIM from a cellular network (AT&T, Verizon, T-Mobile, etc.) select `2` for no.

You will then be asked to select the correct APN for your SIM card:

```bash
Which APN is correct for your SIM card?

AT&T - IoT device - APN: m2m.com.attz
AT&T - Laptop or Tablet - APN: broadband
AT&T - Smartphone - APN: phone
T-Mobile - APN: fast.t-mobile.com
Version - APN: vzwinternet

1) m2m.com.attz	      3) phone		    5) vzwinternet
2) broadband	      4) fast.t-mobile.com  6) Custom
```

If you do not see your APN on the list, you can choose `Custom` and enter your SIM card's APN.

You will then be asked which region your SIM card will be used in:

```bash
Select modem region:
1) Americas
2) Europe/Middle-East/Asia
```

Your VOXL is now finished configuring and will setup and start the required background services.

```bash
Setting APN: m2m.com.attz
reloading systemd services
enabling voxl-modem systemd service
starting voxl-modem systemd service
DONE configuring voxl-modem
```

After a short time your VOXL will be connected to the LTE network. You will now be able to reboot your VOXL and have it connect to the LTE network automatically.

For information on how to get connected to your Ground Control Station over LTE, checkout our page on [using a VPN with VOXL](/voxl-vpn/).
