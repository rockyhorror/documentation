---
layout: default
title: Point-to-Point Modems
parent: Modems
nav_order: 15
has_children: true
permalink: /microhard-modems/
---

# ModalAI Point-to-Point Modems
{: .no_toc }

{:toc}
Documentation for ModalAI's range of  Point-to-Point modems.  

![microhard](/images/userguides/microhard/Microhard-pMDDL2350.png)