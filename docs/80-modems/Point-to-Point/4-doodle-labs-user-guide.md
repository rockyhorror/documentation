---
layout: default
title: Doodle Labs User Guide
parent: Point-to-Point Modems
grand_parent: Modems
nav_order: 4
permalink: /doodle-labs-user-guide/
---

# Doodle Labs User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

VOXL supports the ability to quickly and easily add a Doodle Labs wireless connection to a Ground Control Station.  The following guide provides you the necessary details to do so.

[Doodle Labs Smart Radio Configuration Guide](https://doodlelabs.com/wp-content/uploads/Smart-Radio-Configuration-Guide-v0521.pdf)

[Doodle Labs Technical Library](https://doodlelabs.com/technologies/technical-library/technical-library-downloads/)

[Doodle Labs Helix Datasheet](/images/datasheet/Doodle_Labs_Smart_Radio-RM-2025-2L_Datasheet.pdf)

![doodle-labs-modem](/images/userguides/doodle/doodle-labs-radio.png)

## Requirements

### Hardware

The following hardware is required to establish a Doodle Labs network connection between VOXL and a host computer.

| Part Number | Description | Link |
| --- | --- | --- |
| MCCA-0017 | VOXL USB Expansion Board | [Purchase](https://www.modalai.com/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot?_pos=7&_sid=dac986b48&_ss=r)| 
| MCBL-00009-1 | USB Cable - Host, 4-pin JST to USB 2.0 Type A Female | [Purchase](https://www.modalai.com/products/usb-cable-4-pin-jst-to-usb-a-female?_pos=2&_sid=bfd1b9e9e&_ss=r)| 


## Hardware Setup

![doodle-labs-modem](/images/userguides/doodle/doodle-labs-setup.jpeg)

The Doodle Labs [Technical Library](https://doodlelabs.com/technologies/technical-library/) features a great video on "Smart Radio Bench Testing" which can be viewed in order to get a good idea of how to setup the ground station side of the Doodle Labs hardware.

Hardware setup on the VOXL side is very similar to that of the Ground control station side.

See the above image in order to get a good picture of the required setup for both the ground control station and VOXL sides of the network.

In order to connect the smart radio to VOXL, it is also required to have a USB to Ethernet adapter, such as the following: [Apple USB Ethernet Adapter](https://www.apple.com/shop/product/MC704LL/A/apple-usb-ethernet-adapter)


![doodle-labs-modem](/images/userguides/doodle/doodle-ethernet-test-board.jpeg)

- As per the above image, connect an ethernet cable to the `ETH0` port of the Ethernet Test Board.

- Connect the other end of the ethernet cable to the ethernet port of your USB to Ethernet adapter.

- Connect the USB male end of your USB to Ethernet adapter to the USB female end of the `MCBL-00009-1` (USB to JST adapter).

- Plug the `MCCA-0017` USB add-on board into your VOXL and connect the JST end of `MCBL-00009-1` to the USB host connector of the USB add-on board.

- Provide external power to the Power Supply on the Ethernet Test Board.



## Software Setup

In order to enable the Doodle Labs network on VOXL at boot, the following software is required:

- [System Image 2.5.2 +](/voxl-system-image/)
- [voxl-modem_0.12.0 +](http://voxl-packages.modalai.com/stable/)

`voxl-configure-modem` allows you to quickly and easily bring up a Doodle Labs modem.

```bash
# On VOXL
voxl-configure-modem
```

You will first be asked which modem type you are setting up:

```bash
What type of modem are you using?

If you are unsure of which modem you have, take a look at the following datasheets:

v1 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/
v2 LTE Modem: https://docs.modalai.com/lte-modem-and-usb-add-on-v2-datasheet/
Microhard Modem: https://docs.modalai.com/microhard-add-on-datasheet/
Doodle Labs Modem: https://docs.modalai.com/doodle-labs-user-guide/

1) v1
2) v2
3) quectel
4) microhard
5) doodle
```

Choose `5` for doodle.

You will then be asked for the IP address that you would like to be assigned to your VOXL on the Doodle Labs network.

```bash
Enter the IP address you would like to the VOXL to use on the Doodle network:
Note: The chosen IP address must be of the form: 10.223.0.XXX

Default - 10.223.0.100

1) default
2) custom 

```

Once you choose your IP address, the VOXL will configure the service files that enable Doodle Labs connection on bootup.

```bash
Making new interface file /etc/network/interfaces
reloading systemd services
enabling voxl-modem systemd service
starting voxl-modem systemd service
DONE configuring voxl-modem
``` 

## Connecting to QGroundControl

The VOXL system uses the `voxl-vision-px4` service on VOXL to establish a connection between PX4 and QGroundControl over UDP.  This is supported on VOXL Flight or when using Flight Core with VOXL (using [this procedure](/how-to-connect-voxl-to-flight-core/)).

Set the default IP address of the host PC, Ubuntu 18.04 shown below:

![doodle-labs-default-ip](/images/userguides/doodle/doodle-host-ip.png)

In this case, the IP address of the host PC is `10.223.0.150`


If you have not yet setup `voxl-vision-px4` on your VOXL, do the following:

```bash
# On Host PC
$ adb shell

# On VOXL
voxl-configure-vision-px4
```

You will be asked a variety of questions about the camera configuration (if any) and physical configuration of your drone (if any). Answer these questions with the options that best describe your setup, you can select no cameras as an option.

- When asked for the IP address for QGroundControl, you'll use `10.223.0.150` in this example

If you have already configured `voxl-vision-px4`, you can edit the configuration file as needed.  Run the following command and update the `qgc_ip` field to match the GCS computer's IP, in this example `10.223.0.150`

```bash
vi /etc/modalai/voxl-vision-px4.conf
```

If you've modified the config file, reset the `voxl-vision-px4` service and reload the configuration by running the following command:

```bash
systemctl restart voxl-vision-px4
```
