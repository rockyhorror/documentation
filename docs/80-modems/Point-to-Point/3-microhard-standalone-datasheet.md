---
layout: default
title: Microhard USB Carrier Board
parent: Point-to-Point Modems
grand_parent: Modems
nav_order: 3
permalink: /microhard-usb-carrier/
---

# Microhard USB Carrier Board
{: .no_toc }

There are two configurations for the VOXL Microhard kits.

* VOXL Add-on that plugs directly into VOXL's board to board connector
* Standalone USB Dongle

This datasheet describes the standalone USB dongle. Microhard Add-on information can be found in this [Datasheet](https://docs.modalai.com/microhard-add-on-datasheet/)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Specification

### Block Diagram

[View in fullsize](/images/datasheet/microhard/M0048-block-diagram.jpg){:target="_blank"}

![voxl-acc-mh-block-diagram.jpg](/images/datasheet/microhard/M0048-block-diagram.jpg)

### Part Numbers

| Part Number                                       | Silkscreen Marking | Description |
| ---                                               | ---                | ---         |
| [MH-SA-2](https://www.modalai.com/products/mh-sa) | M0048 REV A        | * Added proper power input connector (SM02B-SFHRS-TF) <br> * Moved to LAN7500 ethernet controller for better compatibility with Android |
| MH-SA-1                                           | M0012 REV A        | Initial Release |

### Physical Specification

| Specicifcation | Value |
| --- | --- |
| Weight | 12g |
| Dimensions | 49 x 40mm |
| Antenna Connector | ?? UFL-R-SMT-110 |
| Recommend Antennas | |
| USB2 Host Connectors | 4 Position JST GH, Vertical, BM04B-GHS-TBT |
| USB2 Host Mating Connector | JST GHR-04V-S |

### Power Consumption

| M0048 Configuration | 5V Power Consumption (mA) |
| --- | --- |
| Baseline (USB Hub, Glue Logic, All LEDs ON)          | 315 |
| Baseline plus Qty-2 USB Peripherals at Max 500mA*    | 1375 |
| Baseline + Qty-2 DS USB* + Microhard 900MHz @ 10dBm  | 2800 |
| Baseline + Qty-2 DS USB* + Microhard 900MHz @ 30dBm  | 3700 |
| Baseline + Qty-2 DS USB* + Microhard 2400MHz @ 10dBm | 2500 |
| Baseline + Qty-2 DS USB* + Microhard 2400MHz @ 30dBm | 3100 |

*For each of the two downstream peripherals removed from the hub, subtract 500mA plus ~30mA logic from the power requirement.

### Inputs

External 5VDC input required.  Included in the kit is the 5V Stand Alone Dongle Cable, v1 [MCBL-00017](/cable-datasheets/#mcbl-00017).

## Pictures

### Assembled with Cabling

![mh-sa-assembled.png](../../images/datasheet/microhard/mh-sa-assembled.png)

## Board Connections and I/O

### Top of Board

![ma-sa-top.png](../../images/datasheet/microhard/mh-sa-2-top.png)

### Bottom of Board

![ma-sa-bottom.png](../../images/datasheet/microhard/mh-sa-2-bottom.png)

### USB Connections

You can connect up to two USB-clients to the `USB2 Host` ports on the top of the board.  A [MCBL-00009](/cable-datasheets/#mcbl-00009) cable can be used, this is a 4-pin JST to USB Female Type A cable.

You can connect the carrier board to a host computer using the USB port on the bottom of the baord.  An `MCBL-00006` cable can be used to connect to a host comptuter (4-pin JST to USB) or the `MCBL-00010` stub cable, which is a 4-pin JST to microUSB female cable.

### LEDs

| Index | LED       | Description |
| ---   | ---       | --- |
| 1     | `TX`      | Flashes `GREEN` indicating wireless TX traffic  |
| 2     | `RX`      | Flashes `GREEN` indicating wireless RX traffic |
| 3     | `RSSI-3`  | Flashes `GREEN` to indicate the Received Signal Strength on the wireless link |
| 4     | `RSSI-2`  | Flashes `GREEN` to indicate the Received Signal Strength on the wireless link |
| 5     | `RSSI-1`  | Flashes `GREEN` to indicate the Received Signal Strength on the wireless link |
| 6     | `CPU`     | Solid `BLUE`indicates power to module and normal operation, flashing indicates boot or FW upgrade status |
| 7     | `LAN`     | TODO |
| 8     | `LAN/100` | TODO |
| 9     | `LAN/ACT` | TODO |
| 10    | `5V`      | Illuminates solid `GREEN` indicating 5VDC USB bus power enabled |
| 11    | `Port3`   | Illuminates solid `GREEN` indicating Port 3 USB power enabled |
| 12    | `Port4`   | Illuminates solid `GREEN` indicating Port 4 USB power enabled |

### CFG Switch

Can be used to reset all settings to default values required for a Slave module.

## Slave Configuration

To reset all settings to the default values required for a Slave module, press and hold the `CFG Button`, once the `CPU LED` begins to flash, continue to hold for *5 seconds*, then release.

The module will then reset all settings to the default values required for a Slave module
