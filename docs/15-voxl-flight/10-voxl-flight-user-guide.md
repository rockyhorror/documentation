---
layout: default
title: VOXL Flight User Guide
parent: VOXL Flight
nav_order: 10
permalink: /voxl-flight-user-guide/
---

# VOXL Flight User Guide

VOXL Flight is built around a VOXL and a Flight Core. Please refer to the [VOXL Quickstart](/quickstarts/) and the [Flight Core Quickstart](/flight-core-getting-started/) pages.