---
layout: default
title: VOXL Flight Datasheets
parent: VOXL Flight
nav_order: 70
permalink: /voxl-flight-datasheet/
has_children: true
---

# VOXL Flight Datasheets
{: .no_toc }

VOXL Flight combines VOXL and Flight Core into one package. This is one of the first computing platforms to combine the power and sophistication of Snapdragon with the flexibility and ease of use of a STM32F7. You can purchase VOXL Flight [here](https://www.modalai.com/products/voxl-flight)

VOXL Flight's specifications are almost identical to [VOXL](https://docs.modalai.com/voxl-datasheet/) and [Flight Core](https://docs.modalai.com/flight-core-datasheet-connectors/) combined. The specific differences are:
* VOXL J1 and J12 are directly connected into the Flight Core portion of the board, so are not there on VOXL-Flight.
* The IMU0 ICM20948 on VOXL is replaced on VOXL-Flight with ICM42688. IMU1 on both platforms is ICM20948.

![VOXL-Flight compared with VOXL and Flight Core](../../images/datasheet/voxl-flight/voxl-flight-cropped.png)