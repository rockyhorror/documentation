---
layout: default
title: VOXL Flight Functional Description
parent: VOXL Flight Datasheets
grand_parent: VOXL Flight
nav_order: 05
permalink: /voxl-flight-functional-description/
---

# VOXL Flight Functional Description
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

VOXL Flight combines [VOXL](/voxl-datasheet) and [Flight Core](/flight-core-datasheet) into one package. This the first open computing platform designed specifically for drones (sUAS) to combine the power and sophistication of Snapdragon with the flexibility and ease of use of a STM32F7 in a single PCB.  VOXL Flight is 100% software compatible with VOXL and Flight Core, but in a single PCB to reduce cabling, cost and increase reliability.

The VOXL advances the [Snapdragon Flight and Qualcomm Flight Pro](https://www.modalai.com/qualcomm-flight-pro) architectures for:

- PX4 Avoidance and Navigation
- ROS  / ROS2
- Open source Linux kernel, cross-compilers, PX4, ROS, OpenCV
- Docker build environment for CPU, GPU (OpenCL) and DSP (Hexagon SDK) heterogeneous computer vision and deep learning processing.
- Run Docker on VOXL target for improved portability and maintenance.
- Add cellular LTE connectivity for beyond visual line of sight (add-on).

Specs Overview:

- Snapdragon 821: Quad-core up to 2.15GHz, GPU, 2xDSP, Linux
- 216MHz, 32-bit ARM M7 [STM32F765II](https://www.st.com/en/microcontrollers-microprocessors/stm32f765ii.html), [PX4](https://github.com/PX4/Firmware)
- Video support: 3x MIPI cameras, UVC
- Machine vision support: time synchronized IMU and cameras
- Additional I/O: WiFi, SPI, I2C, UART, GPIO
- Light weight, powerful, SWAP-optimized computing:
  - Weight 24g
  - Power Adapter weight 13g
  - Cameras (6g)
  - Power consumption 3-10W

> VOXL Flight combines VOXL and Flight Core exactly. Except for mechanical specifications, the datasheets for [VOXL](https://docs.modalai.com/voxl-datasheet/) and [Flight Core](https://docs.modalai.com/flight-core-datasheet/) can be used for their respective functionalities on VOXL Flight.

## Mechanical Specs

### 3D Dimensions

[3D STEP File](https://storage.googleapis.com/modalai_public/modal_drawings/M0019_VOXL-Flight.zip)

### 2D Dimensions

![VOXL-Flight Dimensions](../../../images/datasheet/voxl-flight/voxl-flight-dimensions.png)
