---
layout: default
title: VOXL Flight IMUs
parent: VOXL Flight Datasheets
grand_parent: VOXL Flight
nav_order: 20
permalink: /voxl-flight-datasheet-imus/
---

# VOXL Flight IMUs
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Flight IMU Hardare Configuration

### Summary

| Configuration | VOXL Flight Rev A | Interface |
| --- |--- |--- |
| VOXL IMU0 | ICM-42688 | VOXL SPI10 |
| VOXL IMU1 (recommended) | ICM-20948 | VOXL SPI1 |
| Flight Core IMU1 | ICM-20602 | PX4 SPI1 |
| Flight Core IMU2 | ICM-42688 | PX4 SPI2 |
| Flight Core IMU3 | BMI-088 |  PX4 SPI6 |

<br>

### VOXL Flight IMUs Top

![VOXL-Flight IMUs Top](/images/datasheet/voxl-flight/voxl-flight-imus-top.png)

### VOXL Flight IMUs Bottom

![VOXL-Flight IMUs Bottom](/images/datasheet/voxl-flight/voxl-flight-imus-bottom.png)

## IMU Details

### VOXL Flight - VOXL IMU0

TODO

### VOXL Flight - VOXL IMU1

TODO

### VOXL Flight - FlightCore IMU1

TODO

### VOXL Flight - FlightCore IMU2

TODO

### VOXL Flight - FlightCore IMU3

TODO
