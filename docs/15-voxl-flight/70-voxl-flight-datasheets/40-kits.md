---
layout: default
title: VOXL Flight Kits
parent: VOXL Flight Datasheets
grand_parent: VOXL Flight
nav_order: 40
permalink: /voxl-flight-kits/
---

# VOXL Flight Kits
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Flight Development Kit

![voxl-flight-dk](/images/quickstart/voxl-flight/voxl-flight-dk.jpg)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/voxl-flight)

Kit contains:

- [MKIT-00020-1 - VOXL Flight computing platform and flight controller](/voxl-flight-datasheet/)
- [MKIT-00037-1 - VOXL Power Module v3](/power-module-v3-datasheet/)
- MKIT-00003 - VOXL Wi-Fi Antenna Kit
- [MCBL-00004](/cable-datasheets/#mcbl-00004/) - 8-channel Breakout Board and cable
- [MCBL-00005](/cable-datasheets/#mcbl-00005/) - RC Input Cable
- [MCBL-00010](/cable-datasheets/#mcbl-00010/) - 4-pin JST to USBA Cable (PX4 to QGroundControl)

## Board Only

- [MKIT-00020-1 - VOXL Flight computing platform and flight controller](/voxl-flight-datasheet/)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/voxl-flight?variant=31707275362355)
