---
layout: default
title: VOXL Flight
nav_order: 15
has_children: true
permalink: /voxl-flight/
---

# VOXL-Flight
{: .no_toc }

VOXL Flight is an all-in-one board solution that combines a VOXL onboard computer and a Flight Core PX4 flight controller.

Buy [Here](https://www.modalai.com/products/voxl-flight?variant=31636287094835)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/8/voxl-flight)


![voxl-flight](/images/quickstart/voxl-flight/voxlflighttop.jpg)