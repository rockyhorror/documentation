---
layout: default
title: Qualcomm Flight RB5
nav_order: 35
has_children: true
permalink: /qualcommflightrb5/
---

# Qualcomm Flight RB5
{: .no_toc }

A comprehensive drone reference design with low-power, high-performance heterogenous computing, artificial intelligence engine that is designed to deliver 15 TOPS, long-range Wi-Fi 6 and 5G (Optional) connectivity, support for 7 camera concurrency, computer vision, and vault-like security. 

Buy [Here](https://www.modalai.com/products/qualcomm-flight-rb5-5g-platform-reference-drone?variant=39363143925811)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/22/qualcomm-flight-rb5-5g-drone)

![RB5](/images/quickstart/RB5/rb5front2.png)