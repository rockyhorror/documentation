---
layout: default
title: VOXL SDK
nav_order: 05
has_children: true
permalink: /voxl-sdk/
---

# VOXL SDK
{: .no_toc }

## Overview
VOXL SDK (Software Development Kit) consists of the open source [core libraries](/core-libs/), [services](/mpa-services/), [tools](/inspect-tools/), [utilities](/sdk-utilities/), and [build environments](/build-environments/) that ModalAI provide to accelerate the use and development of VOXL compute boards and accessories.

The source code for projects within VOXL SDK can be found at [https://gitlab.com/voxl-public](https://gitlab.com/voxl-public) alongside build instructions. This section of the manual serves to provide a description of the use and functionality of the software.


[![voxl-sdk-map](/images/resources/voxl-sdk-mpa.png)](/images/resources/voxl-sdk-mpa.png)

## What is (Modal Pipe Architecture) MPA?

The vast majority of VOXL [services](/mpa-services/), [tools](/inspect-tools/), and [utilities](/sdk-utilities/) require some inter-process communication to function. For example, [voxl-qvio-server](/voxl-qvio-server/) consumes camera data from [voxl-camera-server](/voxl-camera-server/) and IMU data from [voxl-imu-server](/voxl-imu-server/) to provide Visual Inertial Odometery (VIO). We use POSIX pipes as the underlying transport mechanism for this inter-process communication due to their robustness, efficiency, and portability. This structure is called Modal Pipe Architecture, or simply MPA.

To provide standardization and ease of use, all [MPA services](/mpa-services/) use the C/C++ library [libmodal_pipe](/libmodal-pipe/) to create, publish, and subscribe to MPA data. This is not strictly necessary. Due to the nature of POSIX pipes, many things can be done in MPA simply by echo'ing and cat'ing to pipes from the command line or from other programming languages. However, [libmodal_pipe](/libmodal-pipe/) remains the tested and recommended method for interacting with Modal Pipe Architecture.

Please continue reading through the following sections to learn more:
