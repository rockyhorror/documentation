---
layout: default
title: Deprecated
nav_order: 85
has_children: true
permalink: /deprecated/
---

# Deprecated Pages
{: .no_toc }

Here is a collection of old deprecated pages. 
