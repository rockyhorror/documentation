---
layout: default
title: VOXL CAM Datasheet
parent: VOXL CAM
nav_order: 70
permalink: /voxl-cam-datasheet/
---

# VOXL-CAM v1 Datasheet
{: .no_toc }

---

## Helpful Links
{: .no_toc }
- [User Manual, v1](/voxl-cam-v1-manual/)

## Hardware Overview
![voxl-cam-labeled](/images/userguides/voxl-cam/voxl-cam-labeled.jpg)

## Dimensions
![voxl-cam](/images/userguides/voxl-cam/voxl-cam-dimensions.jpg)
[View in fullsize](/images/userguides/voxl-cam/voxl-cam-labeled.pdf){:target="_blank"}


## Specifications

| Feature               | Details                                |
|---                    |---                                     |
| Dimensions            | 100.32mm x 39.57mm x 17.47mm |
| Weight                | 57.5g |
| Compute               | [VOXL (M0006)](/voxl-datasheet/)                                |
| Flight Controller     | Integrated PX4 coming soon on DSP <br>External FlightCore via UART, not included) |
| Communications        | USB, WiFi, support for [VOXL Add-Ons](/voxl-add-ons/) including LTE and Microhard |
| Time of Flight Sensor | [PMD (ToF)](voxl-tof-sensor-datasheet/)  |
| Tracking Image Sensor | [OV7251 - M0014](/voxl-tracking-camera-datasheet/) |
| Stereo Image Sensors  | [OV7251 - M0015](/voxl-stereo-camera-datasheet/) |
| Connectors            | UART (MAVLink) [VOXL J12](/voxl-datasheet-connectors/) |
|                       | UART/I2C/GPIO [VOXL J7](/voxl-datasheet-connectors/) |
|                       | UART [VOXL J11](/voxl-datasheet-connectors/)|

