---
layout: default
title: Modems
nav_order: 80
has_children: true
permalink: /modems/
---

# Modems
{: .no_toc }

Documentation for ModalAI's range of Point-to-Point, 4G LTE, and 5G modems, including VOXL add-ons and standalone units. 

Buy [Here](https://www.modalai.com/collections/accessories)

Still have questions? Check out the forum [Here](https://forum.modalai.com)

![microhard](/images/quickstart/microhard/microhard.jpg)

