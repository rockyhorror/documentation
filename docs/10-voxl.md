---
layout: default
title: VOXL
nav_order: 10
has_children: true
permalink: /voxl/
---


# VOXL
{: .no_toc }

VOXL is ModalAI's autonomous computing platform built around the Snapdragon 821. The VOXL architecture combines a single board computer with a depth camera, flight controller and cellular modem to create fully autonomous, connected drones and robots!

Buy [here](https://www.modalai.com/voxl/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink), then follow the [VOXL Quickstart Guide](/voxl-quickstarts/) and the rest of our user manuals in the bar on the left!

<img src="/images/userguides/voxl/voxl-whiteb.jpg" alt="voxl" width="70%">


## Why VOXL?

The VOXL technology builds on the Qualcomm® Flight Pro™ architecture by adding comprehensive software development support with a build-able Linux kernel, cross-compile SDKs, LTE, time of flight cameras, [Docker images](https://gitlab.com/voxl-public/voxl-docker) for development and more.




