---
layout: default
title: Seeker Quickstart
parent: Seeker
nav_order: 5
has_children: false
permalink: /seeker-quickstart/
---

# Seeker Reference Drone Quickstart
{: .no_toc }

This guide will walk you from taking the Seeker out of the box and up into the air!

For technical details, see the [datasheet](/seeker-datasheet/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## WARNING
{: .no_toc }

*Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

---

## Overview

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- LiOn 3S battery with XT30 connector
  - 3S 3000mAh 10C Li-Ion Battery - XT30 - Requires a triangular configuration of (3) 18650 cells  (Sony VTC6 cells highly recommended)
  - Buy [Here](https://www.racedayquads.com/products/auline-11-1v-3s-3000mah-10c-li-ion-battery-xt30?currency=USD&variant=32216675549297&gclid=Cj0KCQjwyN-DBhCDARIsAFOELTn1dLrS0Sd-f5obOcmf2lDsekpYd23u0cz2M7CwYax7X1uF50BhtS8aAgbQEALw_wcB) 
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi

### What's in the Box

Included in the box are:

- Seeker Development Kit with Spektrum Reciever or FrSky
- 6" props
- Cables

![seeker-out-of-box](/images/userguides/Seeker/Seeker-out-of-box.JPG)

---