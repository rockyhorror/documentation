---
layout: default
title: Seeker Datasheet
parent: Seeker
nav_order: 10
has_children: false
permalink: /seeker-datasheet/
---

# Seeker Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL Seeker is a compact development drone. Use cases are for reconnaissance, mapping and photogrammetry, indoor asset inspection, swarming, GPS denied navigation. See below for specifications.

## High-Level Specs

![Seeker-flowchart](/images/userguides/Seeker/Seeker-Flowchart.png)
![Seeker-labeled](/images/userguides/Seeker/Seeker-Labeled.png)

| Specicifcation | VOXL® Seeker |
| --- | --- |
| Take-off Weight | ~550g |
| --- | --- |
| Operating Environment | Inside - with ModalAI VIO navigation <br> Outside - with GPS or VIO navigation |
| --- | --- |
| IP Rating | None |
| --- | --- |
| NDAA Compliant | Yes |
| --- | --- |
| Blue UAS Compliant | Yes |
| --- | --- |
| Origin of Manufacturing | USA |
| --- | --- |
| Military Only | No |
| --- | --- |
| Pre-Certified by FCC | Yes |
| --- | --- |
| Operating Voltage | 5 VDC Input <br> 3.3,5,12VDC rails available |
| --- | --- |
| Size | 270mm Air Frame <br> 6" Props |
| --- | --- |
| Fight Time | ~27 min |
| --- | --- |
| Payload Capacity (Impacts flight time) | TBD |
| --- | --- |
| Cooling | None |
| --- | --- |
| Motors | (4) 2203.5 1500KV Motors |
| --- | --- |
| Propellers | 6” x 4 propellers |
| --- | --- |
| Frame | 270mm Carbon fiber air frame manufactured from 1/8” carbon panels |
| --- | --- |
| ESCs | VOXL® ESC v2 |
| --- | --- |
| CPU | Qualcomm® Snapdragon 821 |
| --- | --- |
| Sensors | 1 ICM-42688-P IMUs <br> 2 Barometers (BMP338 and TDK ICP-10100) <br> GPS (Holybro 2nd GPS) <br> Magnetometer Connection (Dronecode compliant) <br> Spare SPI and GPIO port |
| --- | --- |
| Image Sensors | Tracking camera OV7251-A36 <br> Time of Flight Module 68FX053B <br> Front OV7251-A50 stereo cameras |
| --- | --- |
| Additional I/O | 2 GPIO <br> I2C <br> 1 SPI |
| --- | --- |
| Communications | WiFi <br> 4G LTE Sierra Modem WP7607-G_110419 (Europen and Asian Carriers) <br> 4G LTE Sierra Modem WP7610-G_1104125 (North American Carriers) |
| --- | --- |
| Remote Control | Spektrum |
| --- | --- |
| Power Module | Power Module V3 with XT30 Connectors (M0041-1-B) Rated for 3S-6S  |
| --- | --- |
| Battery | 11.1V 3S 3000mAh 10C Li-Ion Battery - XT30 <br> 3S SONYVTC6 (recommended) 18650 (triangle orientation packaging)|
| --- | --- |
| Landing Gear | Custom landing gear (front only) |
| --- | --- |
| Operating Temperature | +5*C to +40*C |
| --- | --- |
| Operating System | Linux Yocto Jethro with 3.18 Kernel |
| --- | --- |
| Open Applications | Docker <br> ROS <br> PX4 Software |
| --- | --- |
| ModalAI Software | VOXL®-Suite 0.4.6 |
| --- | --- |
| Developer Connectivity | QGroundControl over WiFi <br> adb - USB |
| --- | --- |
