---
layout: default
title: Seeker User Guide
parent: Seeker
nav_order: 5
has_children: false
permalink: /seeker-user-guide/
---

# Seeker Reference Drone User Guide
{: .no_toc }

This guide will walk you from taking the Seeker out of the box and up into the air!

For technical details, see the [datasheet](/seeker-datasheet/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## WARNING
{: .no_toc }

*Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

---

## Overview

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter 
  - e.g. SPMR6655, SPM8000 or SPMR8000
  - Any Spektrum transmitter with DSMX/DSM2 compatibility will likely work
  - Buy [Here](https://www.modalai.com/products/m500-spektrum-dx6e-pairing-and-installation?_pos=2&_sid=8af1d1b9b&_ss=r) 
- LiOn 3S battery with XT30 connector
  - 3S 3000mAh 10C Li-Ion Battery - XT30 - Requires a triangular configuration of (3) 18650 cells  (Sony VTC6 cells highly recommended)
  - Buy [Here](https://www.racedayquads.com/products/auline-11-1v-3s-3000mah-10c-li-ion-battery-xt30?currency=USD&variant=32216675549297&gclid=Cj0KCQjwyN-DBhCDARIsAFOELTn1dLrS0Sd-f5obOcmf2lDsekpYd23u0cz2M7CwYax7X1uF50BhtS8aAgbQEALw_wcB) 
- Host computer with:
  - QGroundControl 3.5.6+
  - Ubuntu 18.04+
  - Wi-Fi

### What's in the Box

Included in the box are:

- Seeker Development Kit with Spektrum Reciever or FrSky
- 6" props
- Cables

![seeker-out-of-box](/images/userguides/Seeker/Seeker-out-of-box.JPG)

---

### Vehicle Components

The following images give an overview of the vehicle components.

![vehicle-overview-back-zoom-144-1024.jpg](/images/userguides/Seeker/Seeker-bottom.JPG) 

![vehicle-overview-back-zoom-144-1024.jpg](/images/userguides/Seeker/Seeker-top.JPG)

---
## Setup 
## VOXL-CAM 
The Seeker Development Kit comes equipped with **VOXL CAM** which is a state-of-the art, fully integrated robot perception system. It is built around our incredibly popular VOXL platform and includes a pre-mounted configuration for VIO tracking, stereo and time of flight image sensors.

### Powering up Seeker Development Drone

This can be done using a power supply set to “12.6 volts” or a 3 cell Lithium Ion battery with an XT30 connector.

To install a 3 cell 18650 Li-Ion battery, (Options shown above), open the battery cage as shown and insert the battery pack. The battery cage snaps into place and takes a bit of force to break free.

### Connect to VOXL-CAMS Wi-Fi Access Point
Out of the box, the vehicle will power up as a WiFi Access Point. Under available WiFi connections you should see something like-`VOXL-XX:XX:XX:XX:XX` (where XX... is the MAC address).  Connect to it using password `1234567890`.

![voxl-ssh.png](/images/userguides/m500/voxl-ssh.png).

*Note: you can connect VOXL-CAM to your wireless network using the `voxl-wifi station <SSID> <Password>` command to have allow your host computer to have internet while also connected to VOXL-CAM.  You will need to locate the IP address, which is easily done with `adb` and the USB connection.*

<br>

## Viewing Localization Data from Tracking Sensor

The Visual Intertial Odometry (VIO) feature uses the tracking image sensor with fisheye lens and an onboard IMU to provide localization data.  At a high level, this provides you a realtime "X, Y, Z" in space.

To use this feature, simply run the following command:

```bash
voxl-inspect-qvio
```

The output displayed updates as you move the VOXL-CAM around in space:


![VOXL-CAM-QVIO](/images/userguides/voxl-cam/voxl-cam-qvio-output.png)

You can move VOXL-CAM around and see the data update.

Hit `CTRL+c` to stop the program


## Viewing Point Cloud (and Stereo Tracking Images)
(only needed if you mess with things).04 or newer
- [Setup ROS on PC](https://docs.modalai.com/setup-ros-on-host-pc/)
- [Setup ROS on VOXL](https://docs.modalai.com/setup-ros-on-voxl/) (already done on shipped VOXL-CAMs)
- RViz (`apt-get install rviz`)

### Configuration

Please ensure you've setup ROS on PC

The unit's software is shipped already configured and ready to use.

  *Note: If needed, you can follow the [Software Setup](#software-setup) section below to fully reconfigure the VOXL-CAM.*

### SSH into VOXL-CAM

If not already done, SSH into VOXL-CAM and change the shell to bash:

```bash
ssh root@192.168.8.1
~# bash
yocto:~#
```

Setup the environment (this assumes VOXL-CAM is in SoftAP with IP Address of 192.168.8.1 which is the default):

```bash
yocto:~# source /opt/ros/indigo/setup.bash
yocto:~# source ~/my_ros_env.sh 
```

Run the following command to run the MPA ROS Node:

```bash
yocto:~# roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
```
The exepected output is shown:

```bash
yocto:~# roslaunch voxl_mpa_to_ros voxl_mpa_to_ros.launch
... logging to /home/root/.ros/log/790d372a-a45e-11eb-ade8-ec5c68cd1ad7/roslaunch-apq8096-4239.log
Checking log directory for disk usage. This may take awhile.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://192.168.1.188:36869/

SUMMARY
========

PARAMETERS
 * /mpa/voxl_mpa_to_ros_node/image0_pipe: tracking
 * /mpa/voxl_mpa_to_ros_node/image0_publish: True
 * /mpa/voxl_mpa_to_ros_node/image1_pipe: hires_preview
 * /mpa/voxl_mpa_to_ros_node/image1_publish: True
 * /mpa/voxl_mpa_to_ros_node/image2_pipe: qvio_overlay
 * /mpa/voxl_mpa_to_ros_node/image2_publish: True
 * /mpa/voxl_mpa_to_ros_node/image3_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image3_publish: False
 * /mpa/voxl_mpa_to_ros_node/image4_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image4_publish: False
 * /mpa/voxl_mpa_to_ros_node/image5_pipe: 
 * /mpa/voxl_mpa_to_ros_node/image5_publish: False
 * /mpa/voxl_mpa_to_ros_node/imu0_pipe: imu0
 * /mpa/voxl_mpa_to_ros_node/imu0_publish: True
 * /mpa/voxl_mpa_to_ros_node/imu1_pipe: imu1
 * /mpa/voxl_mpa_to_ros_node/imu1_publish: True
 * /mpa/voxl_mpa_to_ros_node/stereo_pipe: stereo
 * /mpa/voxl_mpa_to_ros_node/stereo_publish: True
 * /mpa/voxl_mpa_to_ros_node/tof_cutoff: 100
 * /mpa/voxl_mpa_to_ros_node/tof_pipe: tof
 * /mpa/voxl_mpa_to_ros_node/tof_publish: True
 * /mpa/voxl_mpa_to_ros_node/vio0_pipe: qvio
 * /mpa/voxl_mpa_to_ros_node/vio0_publish: True
 * /mpa/voxl_mpa_to_ros_node/vio1_pipe: 
 * /mpa/voxl_mpa_to_ros_node/vio1_publish: False
 * /rosdistro: indigo
 * /rosversion: 1.11.21

NODES
  /mpa/
    voxl_mpa_to_ros_node (voxl_mpa_to_ros/voxl_mpa_to_ros_node)

ROS_MASTER_URI=http://192.168.1.188:11311/

core service [/rosout] found
process[mpa/voxl_mpa_to_ros_node-1]: started with pid [4257]
Param: "image3_publish" set to false, not publishing associated interface
Param: "image4_publish" set to false, not publishing associated interface
Param: "image5_publish" set to false, not publishing associated interface
Param: "vio1_publish" set to false, not publishing associated interface


MPA to ROS app is now running

Starting Manager Thread with 8 interfaces

Found pipe for interface: tracking, now advertising
Did not find pipe for interface: hires_preview,
    interface will be idle until its pipe appears
Did not find pipe for interface: qvio_overlay,
    interface will be idle until its pipe appears
Found pipe for interface: stereo, now advertising
Found pipe for interface: tof, now advertising
Found pipe for interface: imu0, now advertising
Found pipe for interface: imu1, now advertising
Found pipe for interface: qvio, now advertising
```

### Launch RVIZ on Desktop

On the host computer, run RVIZ

```bash
rviz
```

To view the Point Cloud from ToF sensor:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “PointCloud2”
3. Change Display Name to “tof”
4. In the left column under "tof" tab select type in Image Topic as /tof/point_cloud
5. If needed, set the "Global Options" "Fixed Frame" to "map"
6. If needed, Under the "tof" node, set Style to "Points"

To View the Tracking image sensors:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “Image”
3. Change Display Name to “tracking” and click "OK"
4. In the left column, expand the “tracking” node and select type in Image Topic as /tracking/image_raw

To View the Stereo image sensors:

1. On the leftmost column click on the “Add” button
2. In the pop-up options click on “Image”
3. Change Display Name to “stereo_left”
4. In the left column under “stereo_left” tab select type in Image Topic as /stereo/left/image_raw
5. On the leftmost column click on the “Add” button
6. In the pop-up options click on “Image”
7. Change Display Name to “stereo_right”
8. In the left column under “stereo_right” tab select type in Image Topic as /stereo/right/image_raw

![voxl cam RVIZ](/images/userguides/voxl-cam/voxl-cam-rviz.png)

---

## Software Setup 

Out of the box, the VOXL-CAM will already be pre-configured to be able to visualize and publish via ROS. In the situation where the device needs to be configured manually, the steps below demonstrate how to do so.

### Setup ADB

On the host computer install the Android debug Bridge (ABD):
```
me@mylaptop:~$ sudo apt install android-tools-adb android-tools-fastboot
```
Make sure the VOXL-CAM is connected via Micro-USB to the host PC. If ADB is set up properly you can check ADB detects your device using ```adb devices```. If a device shows up then you have configured it properly.
```
me@mylaptop:~$ adb devices
List of devices attached
73a05d48	device
```

### Installing Required Packages

The latest VOXL Platform Release 3.2.0-0.3.4-b or newer is all that is needed, and can be downloaded from [here](https://developer.modalai.com/asset/download/69).

This is already installed on units shipped, but if needed, you can follow instructions [here](/flash-system-image/) to reflash.

Begin by entering the VOXL-CAM's bash shell from the host computer using `adb shell` and starting `bash`.

```bash
me@mylaptop:~$ adb shell
/ # bash
yocto:/#
```

Setup the environment (this assumes VOXL-CAM is in SoftAP with IP Address of 192.168.8.1 which is the default):

### Configure Cameras

```bash
yocto:~# voxl-configure-cameras
```
This should allow you to select different camera configurations. After running the command the following prompt will appear, you'll select option 7.

```bash
Which camera configuration are you using?
0 None
1 Tracking + Stereo
2 Tracking Only
3 Hires + Stereo + Tracking (default)
4 Hires + Tracking
5 TOF + Tracking
6 Hires + TOF + Tracking
7 TOF + Stereo + Tracking
8 Hires Only
9 TOF Only

```
Again, we have camera configuration 7. Enter 7 into the command line and press enter.

Update the environment for ROS usage:

```bash
yocto:~# exec bash
yocto:~# source /opt/ros/indigo/setup.bash
yocto:~# source ~/my_ros_env.sh 
```

### Enable Core Services

Enable the required services:

```
yocto:~# systemctl enable voxl-camera-server
yocto:~# systemctl enable voxl-qvio-server
yocto:~# systemctl enable voxl-imu-server
```

Start the required services:

```
yocto:~# systemctl start voxl-camera-server
yocto:~# systemctl start voxl-qvio-server
yocto:~# systemctl start voxl-imu-server
```

*Note: You can use `voxl-inspect-services` from the `mpa-tools` library to see a list of available services.*

You can run any of these servers in an ssh or adb window by typing their executable name, e.g.:

```bash
yocto:~# voxl-camera-server
```

Manually running these will require an open shell window, but will often have the ability to more easily see logged data from the servers.
