---
layout: default
title: Image Sensors
parent: Other Products
nav_order: 10
has_children: true
permalink: /image-sensors/
---

# Image Sensors
{: .no_toc }

Documentation for ModalAI's range of tracking, Hi-Res, Time of Flight, and Stereo image sensors. 

![microhard](/images/userguides/microhard/imagesensors.png)

{:toc}

## Extending MIPI Image Sensor Cable Lenghts

It is strongly recommended to not connect multiple flex cables back-to-back to increase length beyond what ModalAI has shipped as a valid and supported configuration. The risk here includes:
* Incorrect connector orientation risk resulting in sensor or Voxl failures (including power to ground shorts)
* Adding mating cycles to connectors that have limited life span
* Reducing reliability due to increased interconnect points
* Increasing the length or creating a configuration beyond the data link limits ModalAI has already proven and supports 

If your application needs an extended length for your image sensor, please [contact ModalAI](https://modalai.com/contact) and we can explore a custom flex/cable hardware and software solution that will work for you.
