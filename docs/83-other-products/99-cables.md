---
layout: default
title: Cables
parent: Other Products
nav_order: 99
has_children: false
permalink: /cable-datasheets/
---

# Cables
{: .no_toc }

ModalAI has various cables used by its systems.  Below captures the cables by part number and provides pinouts and other information.

## Table of contents
{: .no_toc .text-delta }

| Cable                                        | Description                                           |
|---                                           |---                                                    |
| [FC-CBL-R1](/cable-datasheets/#fc-cbl-r1)    | Flight Core Cable Kit                                 |
| [MCBL-00001](/cable-datasheets/#mcbl-00001)  | VOXL Flight and VOXL to Power Module Cable            |
| [MCBL-00003](/cable-datasheets/#mcbl-00003)  | Flight Core to Power Module Cable                     |
| [MCBL-00004](/cable-datasheets/#mcbl-00004)  | PWM Output Cable                                      |
| [MCBL-00005](/cable-datasheets/#mcbl-00005)  | RC Input Cable (Spektrum)                             |
| [MCBL-00006](/cable-datasheets/#mcbl-00006)  | 4-pin JST GH to USBA Cable                            |
| [MCBL-00007](/cable-datasheets/#mcbl-00007)  | VOXL to Flight Core/VOXL ESC Serial Cable             |
| [MCBL-00008](/cable-datasheets/#mcbl-00008)  | VOXL to Flight Controller TELEM port                  |
| [MCBL-00009](/cable-datasheets/#mcbl-00009)  | 4-pin JST GH to USBA Female Cable                     |
| [MCBL-00010](/cable-datasheets/#mcbl-00010)  | 4-pin JST GH to micro USB Female Cable                |
| [MCBL-00011](/cable-datasheets/#mcbl-00011)  | VOXL-PM-Y for Flight Deck R0/R1 Cable                 |
| [MCBL-00013](/cable-datasheets/#mcbl-00013)  | VOXL Flight to VOXL ESC Cable (cross over)            |
| [MCBL-00014](/cable-datasheets/#mcbl-00014)  | Flight Core to VOXL ESC Cable (cross over)            |
| [MCBL-00015](/cable-datasheets/#mcbl-00015)  | 4pin-JST-GH-to-4pin-JST-GH USB cable                  |
| [MCBL-00016](/cable-datasheets/#mcbl-00016)  | 6pin-JST-GH-to-pigtail break out cable                |
| [MCBL-00017](/cable-datasheets/#mcbl-00017)  | 5V Stand Alone Dongle Cable, v1                       |
| [MCBL-00018](/cable-datasheets/#mcbl-00018)  | RC Input Cable, Servo (S.Bus, FrSky)                  |
| [MCBL-00019](/cable-datasheets/#mcbl-00019)  | USB3 Host Mode, USB3.0 A Female Plug to Micro-B plug  |
| [MCBL-00020](/cable-datasheets/#mcbl-00020)  | (WIP) USB Cable, 4-pin Molex Picoblade to 4-pin JST-GH, ArduCam (internal) |
| [MCBL-00021](/cable-datasheets/#mcbl-00021)  | (WIP) RC Input Cable, FrSky R-XSR to Flight Core / VOXL Flight |
| [MCBL-00022](/cable-datasheets/#mcbl-00022)  | (WIP) USB3 10-pin JST cable |
| [MCBL-00022](/cable-datasheets/#mcbl-00023)  | (WIP) M0051 Consolidated Pigtail Cable (internal) |
| [MCBL-00024](/cable-datasheets/#mcbl-00024)  | (WIP) Power Module to Stand Alone Modem Cable |
| [MCBL-00025](/cable-datasheets/#mcbl-00025)  | (WIP) LTE v2 to Flight Core Telemetry |
| [MCBL-00026](/cable-datasheets/#mcbl-00026)  | (WIP) Microhard v2 to Flight Core Telemetry |
| [MCBL-00027](/cable-datasheets/#mcbl-00027)  | (WIP) Flight Core v2 to Power Module Cable |

| Accesories                                   | Description                                           |
|---                                           |---                                                    |
| [MCCA-M0022](/cable-datasheets/#mcca-m0022)  | PWM Breakout PCB                                      |

## FC-CBL-R1

![FC-CBL-R1](/images/datasheet/cables/FC-CBL-R1-1.jpg)

Description:

- Flight Core Cable Kit

Contains:

- PWM Break Out Board ([MCCA-M0022](/cable-datasheets/#mcca-m0022))
- [MCBL-00004](/cable-datasheets/#mcbl-00004/) - PWM breakout cable
- [MCBL-00005](/cable-datasheets/#mcbl-00005/) - RC Input Cable
- [MCBL-00006](/cable-datasheets/#mcbl-00006/) - 4-pin JST to USBA Cable (PX4 to QGroundControl)
- [MCBL-00007](/cable-datasheets/#mcbl-00007/) - VOXL to Flight Core Serial Cable (provides MAVLink connection over UART between VOXL and Flight Core)

Buy:

- [Available on the ModalAI shop](https://www.modalai.com/products/flight-core-cable-kit)

## MCBL-00001

![MCBL-00001](/images/datasheet/cables/MCBL-00001.jpg)

Description:

- VOXL Flight and VOXL to Power Module Cable

Where used:

- [VOXL Flight J1013](/voxl-flight-datasheet-connectors/#j1013---5v-dc-power-input-i2c3-to-power-cable-apm/) to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)
- [VOXL J10](/voxl-datasheet-connectors/#j1-5v-dc-in-i2c-to-power-cable-apm/) to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)

Also available here [here](http://www.robotis.us/robot-cable-4p-140mm-10pcs/)

Details:

| Component   | Details |
|---          |--- |
| Connector A | Molex, 0050375043 |
| Connector B | Molex, 0050375043 |
| Length      |  |
| Insulator   |  |
| Color       |  |
| Gauge       |  |

Pinout:

| A   |          | B  |             |
|---  |---       |--- |---          |
| 1   | 5V DC    | 1  | 5V DC       |
| 2   | GND      | 2  | GND         |
| 3   | I2C3_SCL | 3  | PM_SCL      |
| 4   | I2C3_SDA | 4  | PM_SDA      |

## MCBL-00003

![MCBL-00003](/images/datasheet/cables/MCBL-00003.jpg)

Description:

- Flight Core to Power Module Cable

Where Used:

- [Flight Core J6](/flight-core-datasheet-connectors/#j6---voxl-power-management-input--expansion/)

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-06V-S |
| Connector B | Molex, 0050375043 |
| Length      | 120mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 5V DC        | 1  | 5V DC       |
| 2   | -            | -  |             |
| 3   | -            | -  |             |
| 4   | EXP_I2C_SCL  | 3  | PM_SCL      |
| 5   | EXP_I2C_SDA  | 4  | PM_SDA      |
| 6   | GND          | 2  | GND         |

## MCBL-00004

![MCBL-00004](/images/datasheet/cables/MCBL-00004.jpg)

Description:

- PWM Output Cable

Where Used:

- [VOXL Flight J1007](/voxl-flight-datasheet-connectors/#j1007---8-channel-pwmdshot-output-connector) or [Flight Core J7](/flight-core-datasheet-connectors/#j7---8-channel-pwm-output-connector) to PWM Break Out Board ([MCCA-M0022](/cable-datasheets/#mcca-M0022))

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-10V-S |
| Connector B | JST, GHR-10V-S |
| Length      | 120mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 5V DC        | 1  | 5V DC       |
| 2   | PWM_CH0      | 2  | PWM_CH0     |
| 3   | PWM_CH1      | 3  | PWM_CH1     |
| 4   | PWM_CH2      | 4  | PWM_CH2     |
| 5   | PWM_CH3      | 5  | PWM_CH3     |
| 6   | PWM_CH4      | 6  | PWM_CH4     |
| 7   | PWM_CH5      | 7  | PWM_CH5     |
| 8   | PWM_CH6      | 8  | PWM_CH6     |
| 9   | PWM_CH7      | 9  | PWM_CH7     |
| 10  | GND          | 10 | GND         |

## MCBL-00005

![MCBL-00005](/images/datasheet/cables/MCBL-00005.jpg)

Description:

- RC Input Cable (Spektrum)

Where Used:

- [VOXL Flight J1004](/voxl-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector/) or [Flight Core J12](/flight-core-datasheet-connectors/#j12---rc-input--usart6-connector) to RC Receiver (e.g. Spektrum)

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | JST, ZHR-3 |
| Length      | 150mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |              |  B |             |
|---  |---           |--- |---          |
| 1   | 3.3VDC       | 1  | 3.3VDC       |
| 2   | USART6_TX    |-  | NC     |
| 3   | SPEKTRUM RX (3.3V), SBus RX (3.3V), USART6_RX      | 3  | SPEKTRUM TX (3.3V), SBus TX (3.3V)     |
| 4   | GND      | 2  | GND    |

## MCBL-00006

![MCBL-00006](/images/datasheet/cables/MCBL-00006.jpg)

Description:

- 4-pin JST to USBA Cable
  - Flight Core (PX4) to QGroundControl (MAVLink)
  - Microhard Standalone Modem to host computer

Where Used:

- [VOXL Flight J1006](/voxl-flight-datasheet-connectors/#j1006---usb-connector/) or [Flight Core J3](/flight-core-datasheet-connectors/#j3---usb-connector/) to Host Computer with QGroundControl
- [Microhard USB Carrier Board USB2 Host port](/microhard-usb-carrier/) to Host Computer

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | USB Type A Male |
| Length      | 1m |
| Sheilded    | Yes |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | USB      | 1  | VCC    |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| 4   | GND      | 4  | GND    |

## MCBL-00007

![MCBL-00007](/images/datasheet/cables/MCBL-00007.jpg)

Description:

- VOXL to Flight Core Serial Cable (provides MAVLink connection over UART between VOXL and Flight Core)
- VOXL to VOXL ESC Cable (straight through)

Where Used:

- [Flight Core J1](/flight-core-datasheet-connectors/#j1---voxl-communications-interface-connector/) to [VOXL J12](/voxl-datasheet-connectors/#j12-blsp5-off-board-uart-esc/)
- [VOXL J12](/voxl-datasheet-connectors/#j12-blsp5-off-board-uart-esc/) to VOXL ESC (J9 on VOXL ESC v2 or J2 on VOXL ESC v3)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (Flight Core) |
| Connector B | DF13-6S-1.25C (VOXL) |
| Length      | 90mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | -        | 1  |        |
| 2   | RX       | 2  | TX     |
| 3   | TX       | 3  | RX     |
| 4   | -        | 4  | -      |
| 5   | GND      | 4  | GND    |
| 6   | -        | 6  | -      |

## MCBL-00008

![MCBL-00008](/images/datasheet/cables/MCBL-00008.jpg)

Description:

- VOXL to Flight Controller TELEM port (Dronecode Compliant)

Where Used:

- [VOXL J12](/voxl-datasheet-connectors/#j12-blsp5-off-board-uart-esc/) to Flight Controller TELEM, such as [Flight Core J5](/flight-core-datasheet-connectors/#j5---telemetry-connector/) or other Dronecode Compliant TELEM ports

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL) |
| Connector B | JST, GHR-06V-S (Flight Core) |
| Length      | 90mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | -        | -  |        |
| 2   | RX       | 2  | TX     |
| 3   | TX       | 3  | RX     |
| 4   | -        | -  | -      |
| 5   | GND      | 6  | GND    |
| 6   | -        |    | -      |

Buy:

- [Available on the ModalAI shop](https://www.modalai.com/products/voxl-to-flight-controller-telemetry-cable)

## MCBL-00009

![MCBL-00009](/images/datasheet/cables/MCBL-00009.jpg)

Description:

- 4-pin JST to USBA Female Cable

Where Used:

- [LTE Modem and USB Hub Addon J16 and J17](/lte-modem-and-usb-add-on-datasheet/) to USB client device
- [USB Expander and Debug Board](/usb-expander-and-debug-datasheet/) to USB client device
- [Microhard and USB Hub Add-on USB J16 and J17](/microhard-add-on-datasheet/) to USB client device

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S) |
| Connector B | USB 2.0 Type A Female |
| Length      | ~60mm |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | USB      | 1  | VCC    |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| 4   | GND      | 4  | GND    |

Buy:

- [Available on the ModalAI shop](https://www.modalai.com/collections/accessories/products/usb-cable-4-pin-jst-to-usb-a-female)

## MCBL-00010

![MCBL-00010](/images/datasheet/cables/MCBL-00010.jpg)

Description:

- 4-pin JST to micro USB Female Cable
  - Flight Core (PX4) to micro USB cable to QGroundControl (MAVLink)
  - Microhard Standalone Modem to micro USB cable to host computer

Where Used:

- [VOXL Flight J1006](/voxl-flight-datasheet-connectors/#j1006---usb-connector/) or [Flight Core J3](/flight-core-datasheet-connectors/#j3---usb-connector/) to micro USB cabl to Host Computer with QGroundControl
- [Microhard USB Carrier Board USB2 Host port](/microhard-usb-carrier/) to micro USB cable to Host Computer

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | Micro USB 2.0 Female |
| Length      | ~60mm |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | USB      | 1  | VCC    |
| 2   | DATA_M   | 2  | D-     |
| 3   | DATA_P   | 3  | D+     |
| -   |          | 4  | ID     |
| 4   | GND      | 5  | GND    |

Buy:

- [Available on the ModalAI shop](https://www.modalai.com/collections/accessories/products/usb-cable-4-pin-jst-to-micro-usb-female)

## MCBL-00011

![MCBL-00011](/images/datasheet/cables/MCBL-00011.jpg)

Description:

- VOXL-PM-Y for Flight Deck R0/R1 Cable

Where Used:

- VOXL-m500-R1
- [VOXL J1](/voxl-datasheet-connectors/#j1-5v-dc-in-i2c-to-power-cable-apm/) and [Flight Core J6](/flight-core-datasheet-connectors/#j6---voxl-power-management-input--expansion/)to [VOXL PM v2 or VOXL PM v3](/power-module-datasheets/)

Details:

| Component     | Details |
|---            |--- |
| Connector A   | Molex, 0050375043 (VOXL) |
| Connector B   | Molex, 0050375043 |
| Connector C   | JST, GHR-06V-S (Flight Core)|
| Length        | 120mm |
| Insulator     | PVC (flexible) |
| Color         | Black |
| Gauge         | 22/26 AWG |

Pinout:

| A   | VOXL     |  B | VOXL PM |  C | Flight Core |
|---  |---       |--- |---      |--- |---          |
| 1   | 5V DC    | 1  | 5V DC   | 1  | 5V DC       |
| 2   | GND      | 2  | GND     | 6  | GND         |
| -   |          | 3  | SCL     | 4  | EXP_I2C_SCL |
| -   |          | 4  | SDA     | 5  | EXP_I2C_SDA |

## MCBL-00013

![MCBL-00013](/images/datasheet/cables/MCBL-00013.jpg)

Description:

- VOXL Flight to VOXL ESC Cable (cross over)

Where Used:

- [VOXL Flight J1002](/voxl-flight-datasheet-connectors/#j1002---uart-esc-uart2telem3-interface-connector/) to VOXL ESC (J9 on VOXL ESC v2 or J2 on VOXL ESC v3)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-6S-1.25C (VOXL Flight) |
| Connector B | DF13-6S-1.25C (ESC) |
| Length      | 100mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   | VOXL Flight | B  | VOXL ESC |
|---  |---          |--- |---       |
| 1   | -           | -  |                |
| 2   | RX (3.3V)   | 3  | TX (5V, supports 3.3V) |
| 3   | TX (3.3V)   | 2  | RX (5V, supports 3.3V) |
| 4   | -           | -  | -        |
| 5   | GND         | 5  | GND      |
| 6   | -           | -  | -        |

## MCBL-00014

![MCBL-00014](/images/datasheet/cables/MCBL-00014.jpg)

Description:

- Flight Core to VOXL ESC Cable (cross over)

Where Used:

- Flight Core J4 to VOXL ESC (J9 on VOXL ESC v1 or J2 on VOXL ESC v2)

Details:

| Component   | Details |
|---          |--- |
| Connector A | DF13-8S-1.25C (Flight Core) |
| Connector B | DF13-6S-1.25C (ESC) |
| Length      | 80mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   | Flight Core | B  | VOXL ESC |
|---  |---          |--- |---       |
| 1   | -           | -  |                |
| 2   | RX (3.3V)   | 3  | TX (5V, supports 3.3V) |
| 3   | TX (3.3V)   | 2  | RX (5V, supports 3.3V) |
| 4   | -           | -  | -        |
| 5   | GND         | 5  | GND      |
| 6   | -           | -  | -        |
| 7   | -           |
| 8   | -           |

## MCBL-00015

![MCBL-00015](/images/datasheet/cables/MCBL-00015.jpg)

Description:

- 4pin-JST-to-4pin-JST USB cable

Where Used:

- VOXL USB Add-On boards to Stand Alone Modem Boards

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S) |
| Connector B | JST, GHR-04V-S) |
| Length      | 100 mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | V_BUS    | 1  | V_BUS  |
| 2   | DATA_M   | 2  | DATA_M |
| 3   | DATA_P   | 3  | DATA_P |
| 4   | GND      | 4  | GND    |

## MCBL-00016

![MCBL-00016](/images/datasheet/cables/MCBL-00016.jpg)

Description:

- 6pin-JST-to-pigtail break out cable

Where Used:

- M0048 ICM42688 IMU Breakout Board

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-06V-S) |
| Connector B | None |
| Length      | 100 mm |
| Insulator   | PVC (flexible) |
| Color       | Black |
| Gauge       | 26 AWG |

Pinout:

| A   |          |    |        |
|---  |---       |--- |---     |
| 1   |          | 1  | (pigtail) |
| 2   |          | 2  | (pigtail) |
| 3   |          | 3  | (pigtail) |
| 4   |          | 4  | (pigtail) |
| 5   |          | 4  | (pigtail) |
| 6   |          | 4  | (pigtail) |

## MCBL-00017

Description:

- Power Supply (not supplied) to Stand Alone Modem Dongle

Where Used:

- [LTEH-SA-7607-1 J3](/lte-modem-v2-dongle-datasheet/#j3---5vdc-power-in)
- [LTEH-SA-7610-1 J3](/lte-modem-v2-dongle-datasheet/#j3---5vdc-power-in)
- [MA-SA-2 J3](/microhard-usb-carrier/)

Details:

| Component   | Details |
|---          |--- |
| Connector A | [SFHR-02V-R](https://www.digikey.com/en/products/detail/jst-sales-america-inc/SFHR-02V-R/2328483) |
| Connector B | None |
| Length      | 30 cm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black |
| Gauge       | 22 AWG |
| Pre-crimped | [ALEALEA22K102](https://www.digikey.com/en/products/detail/jst-sales-america-inc/ALEALEA22K102/9923160) |

Pinout:

| A   | Wire        | B  |Modem |
|---  |---          |--- |---       |
| 1   | RED         | 1  | VDCIN_5V |
| 2   | BLACK       | 2  | GND      |

## MCBL-00018

![MCBL-00018](/images/datasheet/cables/MCBL-00018.jpg)

Description:

- RC Input (S.Bus, FrSky)

Where Used:

- [VOXL Flight J1004](/voxl-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector/) or [Flight Core J12](/flight-core-datasheet-connectors/#j12---rc-input--usart6-connector) to RC Receiver (e.g. Spektrum) and [VOXL Flight J1003](/voxl-flight-datasheet-connectors/#j1003---ppm-rc-in) or [Flight Core J9](/flight-core-datasheet-connectors/#j9---ppm-rc-in)

| Component   | Details |
|---          |--- |
| Connector A | GHR-03V-S |
| Connector B | GHR-04V-S |
| Connector C | M20-1060300, CONN RCPT HSG 3POS 2.54MM (or similar) |
| Length      | 15 cm |
| Insulator   | PVC (flexible) |
| Color       | Red/Black/Orange |
| Gauge       | 26 AWG |

Pinout:

| A   | Power       | C  | Servo    | B  | Signal   |
|---  |---          |--- |---       |--- |---       |
| 1   | RED (5V)    | 2  | 5V DC    |    |          |
| 2   |             | 3  | S.Bus    | 3  | S.Bus    |
| 3   | BLACK (GND) | 1  | GND      |    |          |
|     |             |    |          |    |          |

## MCBL-00019

Description:

- USB3 Host Mode, USB3.0 Female A Plug to Micro-B plug

Where Used:

- [VOXL and VOXL Flight J8](/voxl-flight-datasheet-connectors/#j8---usb-30-otg)

![MCBL-00019](/images/datasheet/cables/MCBL-00019.png)

## MCBL-00020

Description:

- USB Cable, 4 pin Molex Picoblade to 4 -pin JST-GH

Where Used:

- VOXL Add-ons with USB hub to ArduCam USB Camera

Details:

| Component   | Details |
|---          |--- |
| Connector A | JST, GHR-04V-S |
| Connector B | Molex, 51021-0400 |
| Length      | ? mm |
| Insulator   | ? |
| Color       | ? |
| Gauge       | ? AWG |

Pinout:

| A   |          |  B |        |
|---  |---       |--- |---     |
| 1   | V_BUS    | 1  | V_BUS  |
| 2   | DATA_M   | 2  | DATA_M |
| 3   | DATA_P   | 3  | DATA_P |
| 4   | GND      | 4  | GND    |

## MCCA-M0022

Description:

- PWM Breakout PCB

Where Used:

Pinout:

See [schematic](https://storage.googleapis.com/modalai_public/modal_drawings/MCCA-00022_REVA.pdf)
