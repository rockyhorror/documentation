---
layout: default
title: VOXL Time of Flight Sensor Datasheet
parent: Image Sensors
grand_parent: Other Products
nav_order: 20
has_children: false
permalink: /voxl-tof-sensor-datasheet/
---

# VOXL Time of Flight (ToF) Sensor Datasheet
{: .no_toc }

## Specification

The PMD Time of Flight sensor produces high-fidelity depth mapping indoors up to 6m. On the VOXL platform this sensor is mutually exclusive to the stereo cameras, meaning the stereo cameras need to be replaced with the TOF Add-on.

### Requirements

Requires VOXL System Image 2.3 or greater.

### Details

| Specicifcation | Value |
| --- | --- |
| Technology | [PMD](https://www.pmdtec.com/) |
| Rate | 5 - 45FPS in configurable option modes for distance / accuracy / framerate |
| Exposure Time | 4.8 ms typ. @ 45 fps / 30 ms typ. @ 5 fps |
| Resolution  | 224 x 171 (38k) px |
| FOV (H x V)  | 117° x 117° (near field 100° x 80°) |
| Range | 4 - 6m |
| Depth Resolution | <= 1% of distance (0.5 – 4m @ 5fps) <= 2% of distance (0.1 – 1m @ 45fps) |
| Time Sync | No physical pin, but the frame timestamp is measured with 50ns precision on a single clock. All of the sensors on the VOXL platform are timestamped for computer vision. |
| Power Consumption | <2W |
| Weight | 3g  |
| Dimensions | 24mm x 10.6mm |
| Eye Safe | Yes |

## 2D/3D Drawings

* 2D Dimensions: 24mm x 10.6mm
* 3D Model (STEP): [MCAM-00005-TOF-A65.STEP](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.STEP)
* 3D Model (SolidWorks 2020): [MCAM-00005-TOF-A65.SLDPRT](https://storage.googleapis.com/modalai_public/modal_drawings/MCAM-00005-TOF-A65.SLDPRT)

## Pin Out

![TOF_Pin1s.JPG](/images/datasheet/image-sensor/TOF_Pin1s.JPG)

![TOF_Pin_to_VOXL.JPG](/images/datasheet/image-sensor/TOF_Pin_to_VOXL.JPG)

![TOF_Location.JPG](/images/datasheet/image-sensor/TOF_Location.JPG)

## Example Code

The best approach to access TOF data on VOXL is to write an MPA client that listens to the voxl-camera-server pipe data. An example of that is [here](https://gitlab.com/voxl-public/ros/voxl_mpa_to_ros/-/blob/master/src/interfaces/tof_interface.cpp).

### Lower-level Examples

[HAL3 TOF Point Cloud Publishing to MPA Pipe in voxl-camera-server](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-camera-server/-/blob/master/src/api_interface/hal3/hal3_camera_mgr_tof.cpp)

[libCamera ROS HAL3 TOF Example](https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros)
