---
layout: default
title: VOXL Stereo Sensor Datasheet
parent: Image Sensors
grand_parent: Other Products
nav_order: 15
has_children: false
permalink: /voxl-stereo-camera-datasheet/
---

# VOXL Stereo Sensor Datasheet
{: .no_toc }

## Specification

| Specicifcation | Value |
| --- | --- |
| Sensor | OV7251 [Datasheet](https://www.ovt.com/download/sensorpdf/146/OmniVision_OV7251.pdf) |
| Shutter | Global, Hardware-Synchronized |
| Resolution | 1280x480 (640x480 * 2) |
| Framerate | up to 60Hz |
| Data formats | YUV only |
| Lens Size | 1/7.5" |
| Focusing Range | 5cm~infinity |
| Focal Length | 1.77mm |
| F Number | 2.5 |
| Fov(DxHxV) | 85° x 68° x 56° |
| TV Distortion | < -3.5% |
| Weight | 4g |

## 2D / 3D Drawings

3D model can be found as a component in the VOXL Flight Deck STEP file [found here](https://developer.modalai.com/asset/eula-download/55)

[2D Drawing](https://storage.googleapis.com/modalai_public/modal_drawings/M0015_2D_11-02-21.pdf)

## Pinouts

The following diagram shows the connectors on the [MFPC-M0010A](https://www.modalai.com/products/voxl-replacement-stereo-flex-cable) stereo flex cable. The M0015 image sensor module connects to these for example.

![MFPC-M0010A-pinout.png](/images/datasheet/image-sensor/MFPC-M0010A-pinout.png)

![Stereo_TFlex_Pin_Location.JPG](/images/datasheet/image-sensor/Stereo_TFlex_Pin_Location.JPG)

![Stereo_TFlex_Complete.JPG](/images/datasheet/image-sensor/Stereo_TFlex_Complete.JPG)

![Stereo_TFlex_Location.JPG](/images/datasheet/image-sensor/Stereo_TFlex_Location.JPG)
