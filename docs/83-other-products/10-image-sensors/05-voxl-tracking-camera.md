---
layout: default
title: VOXL Tracking Sensor Datasheet
parent: Image Sensors
grand_parent: Other Products
nav_order: 5
has_children: false
permalink: /voxl-tracking-camera-datasheet/
---

# VOXL Tracking Sensor Datasheet
{: .no_toc }

## Specification

| Specicifcation | Value |
| --- | --- |
| Sensor | OV7251 [Datasheet](https://www.ovt.com/download/sensorpdf/146/OmniVision_OV7251.pdf) |
| Shutter | Global |
| Resolution | 640x480 |
| Framerate | 30,60,90Hz implemented on VOXL, sensor suports up to 120Hz |
| Data formats | B&W 8 and 10-bit |
| Lens Size | 1/3.06" |
| Focusing Range | 5cm~infinity |
| Focal Length | 0.83mm |
| F Number | 2.0 |
| Fov(DxHxV) | 166° x 133° x 100° |
| TV Distortion | -20.77% |


## Tracking Module Technical Drawings

### Tracking Module 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014%20Tracking%20Camera.STEP)

### Tracking Module 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0014_2D_11-02-21.pdf)

## Pin Out

![Tracking_Pin1.JPG](/images/datasheet/image-sensor/Tracking_Pin1.JPG)

![Tracking_Orientation.JPG](/images/datasheet/image-sensor/Tracking_Orientation.JPG)
