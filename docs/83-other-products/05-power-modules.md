---
layout: default
title: Power Modules
parent: Other Products
nav_order: 5
has_children: true
permalink: /power-modules/
---

# Power Modules 
{: .no_toc }

ModalAI offers high performance 5V power adapters for drones (sUAS, UAVs) and other robotic use cases. These power modules support voltage and current monitoring for estimating battery consumption and remaining charge.

The v3 power monitor supports 2S-6S batteries, but does not support hot swapping.

The v2 power monitor supports 2S-5S batteries and hot-swapping batteries with a wall power supply.

<img src="/images/datasheet/hdmi-usb-accessories/pow-mod.jpg" alt="hdmi-expansion" width="60%">


{:toc}
