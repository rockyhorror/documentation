---
layout: default
title:  HDMI Input Accessory Datasheet
parent: HDMI and USB accessories
grand_parent: Other Products
nav_order: 35
has_children: false
permalink: /hdmi-input-accessory-datasheet/
---

# HDMI Input Accessory Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

Provides HDMI input into VOXL or VOXL Flight.

[User Manual](/hdmi-input-accessory-manual/)

## Board Connections

SYSTEM

![FC-DK-R1](/images/datasheet/voxl-acc-hdmi/voxl-acc-hdmi-system.png)

TOP

![voxl-acc-hdmi-top](/images/userguides/hdmi-input/hdmi-input-top.png)

BOTTOM

![voxl-acc-hdmi-bottom](/images/userguides/hdmi-input/hdmi-input-bottom.png)

## Specifications

### System Specification

- HDMI input connector for camera, MIPI connector output for VOXL/VOXL Flight
- Provides 1080p videa to VOXL/VOXL Flight at variable frame rates up to 60FPS in a named pipe
- Requires ModalAI System Image 2.6 or newer
- For B102 specificiations, refer to [this link](https://auvidea.eu/download/manual/B10x_technical_reference_1.4.pdf)

### Physical Specification

System:

| Specicifcation     | Value |
| ---                | --- |
| Size               | TODO |
| Weight             | TODO |

Auvidea PCB:

| Specicifcation     | Value |
| ---                | --- |
| Size               | 27 x 42 mm |
| Weight             | TODO |
| Mounting           | 3x M2.5 mounting holes (do not use M3 screws) |
| Mount Hole Spacing | 22mm (h) x 21mm (v) |
