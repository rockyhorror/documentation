---
layout: default
title: Seeker
nav_order: 45
has_children: true
permalink: /seeker/
---

# Seeker
The world's first micro-development drone with SWAP-optimized sensors and payloads optimized for indoor and outdoor navigation. 

{: .no_toc }
Buy [Here](https://www.modalai.com/products/seeker)

Still have questions? Check out the forum [Here](https://forum.modalai.com/)

![Seeker](/images/userguides/Seeker/Seeker-whitebackg.png)