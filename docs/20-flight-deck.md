---
layout: default
title: VOXL Flight Deck
nav_order: 20
has_children: true
permalink: /flight-deck/
---

# Voxl Flight Deck
{: .no_toc }

A fully assembled and calibrated flight kit. Pre-configured for GPS-denied navigation using Visual Inertial Odometry (VIO).

Buy [Here](https://www.modalai.com/products/voxl-flight-deck-r1?variant=31790279196723)

Still have questions? Check out the forum [Here](https://forum.modalai.com/category/7/voxl-flight-deck)


![voxl-flight-deck](/images/quickstart/flight-deck/flight-deck.jpg)


- [VOXL Flight Deck Support Forum](https://forum.modalai.com/category/7/voxl-flight-deck)

